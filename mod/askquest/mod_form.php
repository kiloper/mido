<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Resource configuration form
 *
 * @package    mod
 * @subpackage resource
 * @copyright  2009 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/course/moodleform_mod.php');
//require_once($CFG->libdir.'/filelib.php');

class mod_askquest_mod_form extends moodleform_mod {
    function definition() {
        global $CFG, $DB;
        $mform =& $this->_form;

        $config = get_config('resource');

		$mform->addElement('header', 'general', get_string('general', 'form'));
        
		$mform->addElement('text', 'name', 'Название', array('size'=>'48'));
		$mform->addRule('name', null, 'required', null, 'client');
		$mform->setType('name', PARAM_TEXT);
		$mform->setDefault('name', 'Задать вопрос');
		
		//$this->add_intro_editor($config->requiremodintro);
		$mform->addElement('textarea', 'descrip', 'Описание', 'rows="10" cols="10" style="width: 366px"');
		$mform->setDefault('descrip', 'Вопрос преподавателю');
        $mform->setType('name', PARAM_RAW);
		//$mform->addRule('descrip', null, 'required', null, 'client');
        
		$this->standard_grading_coursemodule_elements();
        //-------------------------------------------------------
        $this->standard_coursemodule_elements();
        $mform->setDefault('completion', 0);
        //-------------------------------------------------------
        $this->add_action_buttons();

        //-------------------------------------------------------
        
    }
	
	/*function data_preprocessing(&$default_values) {
        if ($this->current->instance and !$this->current->tobemigrated) {
            $draftitemid = file_get_submitted_draft_itemid('files');
            file_prepare_draft_area($draftitemid, $this->context->id, 'mod_askquest', 'content', 0, array('subdirs'=>true));
            $default_values['files'] = $draftitemid;
        }
	}*/

   

}
