<?php
require_once('../../config.php');
require_once($CFG->libdir . '/messagelib.php');
require_once($CFG->dirroot . '/local/edu/programs/lib.php');
require_once($CFG->dirroot . '/local/edu/orgs/syslib.php');
require_once($CFG->dirroot . '/local/edu/tickets/tickets_class.php');
$text		= optional_param('text', '', PARAM_CLEAN);
$course		= optional_param('course', 0, PARAM_INT);
$to		= optional_param('to', 0, PARAM_INT);
$teacherto		= optional_param('teacher', 0, PARAM_INT);
$context = context_course::instance($course, MUST_EXIST);
require_capability('mod/askquest:send', $context);
$tickets = new Tickets();
function getTeachersZ($courseid)
{
    global $CFG, $DB;
    $mas = array();
    require_once($CFG->libdir. '/coursecatlib.php');
    $c = $DB->get_record('course', array("id" => $courseid));
    $course = new course_in_list($c);
    $html = '';
    if ($course->has_course_contacts()) {
        foreach ($course->get_course_contacts() as $userid => $coursecontact) {
            $mas[$userid] = $DB->get_record('user', array('id' => $userid));
        }

    }
    return $mas;
}
$C = $DB->get_record('course', array('id' => $course));

$d = new stdClass();

$program = '';
if($myprogram = getMyProgram($C->id, $USER->id))
    $program = " (Программа - ".$myprogram->name.", дисциплина - {$C->fullname})";

$tickettext = $text;
$text = "<h4>$program</h4><b>Вопрос: </b>".$text."<p>".html_writer::link(new moodle_url('/?to=tickets&ticketid=tdi'), "Перейти к вопросу...")."</p>";

$d->subject           = "Вопрос по дисциплине - ".$C->fullname;
$d->userfrom        =  $USER->id;
$d->fullmessage       = $text;
$d->fullmessageformat = FORMAT_HTML;
$d->fullmessagehtml   = $text;
$d->smallmessage      = strip_tags(str_replace("</p>", "\n", $text));
$d->notification      = 0;
$d->name            = "askquestion_send";
$d->component       = 'mod_askquest';
$d->contexturlname  = "Перейти к сообщениям";
$d->contexturl      = new moodle_url('/message/index.php?id='.$USER->id);

//$CFG->wwwroot."/course/view.php?id=".$course;
//$supportuser = generate_email_supportuser();
//$from = $DB->get_record('user', array('id' => $USER->id));

// maz edit ticket
$ticket = new stdClass();
$ticket->userid = $USER->id;
$ticket->courseid = $course;
$maz_temp = orgs::getNowMyOrg($USER->id, $course);
$ticket->ordid = $maz_temp->id;
$ticket->timecreated = time();
$ticket->timeanswer = time();
$ticket->pid = $myprogram->id;
/*function AddTicketMSG($id,$msg){
    global $DB,$USER;
    $ins = new stdClass();
    $ins->ticketid = $id;
    $ins->userid = $USER->id;
    $ins->timeanswer = time();
    $ins->msg = $msg;
    $DB->insert_record('edu_ticket_msg',$ins);
    
}*/
// -- maz end

if($to == 0)
{
    if($teacher  = getTeachersZ($course))
    {
        if(isset($teacher[$teacherto])) {
            $d->userto = $teacherto;
            //message_post_message($USER, $teacher[$teacherto], $text, FORMAT_HTML);
            //if($USER->id == 6 || 1){
                $ticket->isteacher = 1;
                $ticket->recid = $teacherto;
                //$ticketid = $DB->insert_record('edu_ticket',$ticket);
                $ticketid = Tickets::getTicketOrNo($ticket->pid,$USER->id,$ticket->recid,$ticket->courseid,$ticket);
                if($ticketid) {
                   // AddTicketMSG($ticketid, $tickettext);
                   $insert= Tickets::sendMsgFromStudent($ticketid,$tickettext);
                    $text = str_replace("tdi", $ticketid, $text);
                    email_to_user($teacher, core_user::get_noreply_user(), $d->subject, $text, $text);

                }
            //}
        }

    }
} else
{
    $org = orgs::getNowMyOrg($USER->id, $course);
    $admins = array();

    if($org)
    {
        $admins = orgs::getManagers($org->id);
    }
    if(empty($admins)) $admins = $DB->get_records("user", array('id' => 37));
    //$ticketid = $DB->insert_record('edu_ticket',$ticket);
    //$ticketid = $tickets::getTicketOrNo($ticket->pid,$USER->id,$ticket->recid,$ticket->courseid,$ticket);
    $ticketid = $tickets::insertTicket($ticket);
    if($ticketid) {
        //AddTicketMSG($ticketid, $tickettext);
        $insert= Tickets::sendMsgFromStudent($ticketid,$tickettext);
        $text = str_replace("tdi", $ticketid, $text);
    }
    if($admins)
    {
        foreach($admins as $a)
        {
            $d->userto          = $a->id;
            email_to_user($a, core_user::get_noreply_user(), $d->subject, $text, $text);
            //message_post_message($USER, $a, $text, FORMAT_HTML);
        }
    }
    //if($USER->id == 6 || 1){


    //}
}


?>