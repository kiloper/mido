<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Page module version information
 *
 * @package    mod
 * @subpackage page
 * @copyright  2009 Petr Skoda (http://skodak.org)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once($CFG->libdir.'/completionlib.php');

$id = optional_param('id', 0, PARAM_INT); // Course Module ID

    if (!$cm = get_coursemodule_from_id('askquest', $id)) {
        print_error('invalidcoursemodule');
    }
    $askquest = $DB->get_record('askquest', array('id'=>$cm->instance), '*', MUST_EXIST);


$course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);

require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/askquest:view', $context);

$PAGE->set_url('/mod/askquest/view.php', array('id' => $cm->id));

$PAGE->set_title($course->shortname.': '.$askquest->name);
$PAGE->set_heading($course->fullname);

echo $OUTPUT->header();
echo $OUTPUT->heading(format_string($askquest->name), 2);

echo $askquest->id . "<br>";
echo $askquest->course . "<br>";
echo $askquest->name . "<br>";
echo $askquest->descrip . "<br>";
//echo $askquest->email . "<br>";

echo $OUTPUT->footer();
