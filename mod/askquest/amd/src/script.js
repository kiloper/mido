define(['jquery', 'theme_boost/modal'], function ($) {


    return {
        init: function (courseid, sesskey) {
            $(document).ready(function () {
                var userid = 0;
                $("#askquest").modal({
                    show: false
                });
                $("#askquest .btn-primary").click(function () {
                    var text = $("#askquest textarea").val();
                    if(text)
                    {
                        Y.io('/mod/askquest/ajax.php?userid='+userid+'&courseid='+courseid + '&sesskey=' + sesskey + '&text=' + text, {
                            method: 'POST',
                            on: {
                                success: function (tid, response) {
                                    alert("Сообщение отправлено");
                                    $("#askquest textarea").val('');
                                }
                            }
                        });
                    }
                })
                $('select[name="askquest-userid"]').change(function () {
                    userid = $(this).val();
                    $(".askquest-msg").hide();
                    $(".askquest-msg[alt='"+userid+"']").show();

                })
            })
        }

    };
});