<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod
 * @subpackage resource
 * @copyright  2009 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

/**
 * List of features supported in Resource module
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed True if module supports feature, false if not, null if doesn't know
 */
function askquest_supports($feature) {
    switch($feature) {
        //case FEATURE_MOD_ARCHETYPE:           return MOD_ARCHETYPE_RESOURCE;
        case FEATURE_GROUPS:                  return false;
        case FEATURE_GROUPINGS:               return false;
        case FEATURE_GROUPMEMBERSONLY:        return false;
        case FEATURE_MOD_INTRO:               return false;
        case FEATURE_COMPLETION_TRACKS_VIEWS: return false;
        case FEATURE_GRADE_HAS_GRADE:         return false;
        case FEATURE_GRADE_OUTCOMES:          return false;
        case FEATURE_BACKUP_MOODLE2:          return true;
        case FEATURE_SHOW_DESCRIPTION:        return false;
		//case FEATURE_NO_VIEW_LINK:            return true;

        default: return null;
    }
}


function askquest_add_instance($data, $mform) {
    global $CFG, $DB;
	$data->id = $DB->insert_record('askquest', $data);
	return $data->id;
	
}
function getTeachersm($courseid)
{
    global $CFG;
    require_once($CFG->dirroot. '/local/edu/locallib.php');
    $edu = new Edu();
    if($teachers = $edu->getMyTeachers($courseid))
    {
        $mas = array();
        foreach($teachers as $t) $mas[$t->id] = fullname($t);
        return $mas;
    }
    return array();
}


function askquest_get_coursemodule_info($coursemodule) {
    global $DB, $COURSE, $USER, $CFG, $OUTPUT, $EDU, $PAGE;



	$askquest = $DB->get_record('askquest', array('id' => $coursemodule->instance));
	

    $info = new cached_cm_info();
    $info->content = '<div id="inset_help">
							<div id="dialog" courseid="'.$askquest->course.'">
								<h1 class="dialog_title">'.$askquest->name.'<h1>
								<p>'.$askquest->descrip.'</p>
								<textarea rows="10" id="textarea_help"></textarea>
							</div>
							<a href="">'.$askquest->name.'</a>
						</div>';
    $info->name  = $askquest->name;

    $content = $askquest->descrip;
    $content = str_replace("text", html_writer::tag('textarea', '', ['class' => 'ta form-control']), $content);
    $teacher = $EDU->getTeachers($COURSE->id);
    $t = reset($teacher);
    if($p = $EDU->getProgram())
    {
        if($p->managerid)
        {
            $who = [$p->managerid => "менеджеру"];
            $sel = $p->managerid;
            $perepiska = '';
            if($t)
            {
                $who[$t->id] = "Преподавателю - ".fullname($t);
                $sel = $t->id;
                $perepiska .= html_writer::link("/message/index.php?id=".$t->id, "Перейти к переписке с преподавателем...", ['class' => 'askquest-msg', 'alt' => $t->id]);
            }
            $perepiska .= html_writer::link("/message/index.php?id=".$p->managerid, "Перейти к переписке с менеджером...", ['class' => 'askquest-msg'.($t ? ' hidden' : ''), 'alt' => $p->managerid]);
            $select = html_writer::select($who, 'askquest-userid', $sel, []);
            $content = str_replace("select", $select, $content);
            $content = str_replace("messages", $perepiska, $content);
        }
    }
    if($teacher)
    {
        $content = str_replace("teacher", fullname($t), $content);
        $content = str_replace("messages", html_writer::link("/message/index.php?id=".$t->id, "Перейти к переписке с преподавателем..."), $content);
    }

    $modal = new \theme_edu\modal($askquest->name, html_writer::div($content, 'askquest-content'), 'askquest');
    $modal->setOk("Отправить");
    $info->content = $modal->linkTag('a', $askquest->name, ['class' => 'askquest-link']).$modal->render();
    return $info;
}



function askquest_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
	$filename = $args[1];
	if ($context->contextlevel != CONTEXT_MODULE) {
        return false; 
    }
	if ($filearea !== 'content') {
        // intro is handled automatically in pluginfile.php
        return false;
    }
	require_login($course, true, $cm);
	$fs = get_file_storage();
    $file = $fs->get_file($context->id, 'mod_askquest', $filearea, 0, '/', $filename);
	if (!$file) {
        return false; // The file does not exist.
    }
    send_stored_file($file, null, 0, $forcedownload, $options);
}

function askquest_update_instance($data, $mform) {
    global $CFG, $DB;
    require_once("$CFG->libdir/resourcelib.php");
	$data->id = $data->instance;
    $DB->update_record('askquest', $data);
	
	return true;
}


function askquest_delete_instance($id) {
    global $DB;

    if (!$resource = $DB->get_record('askquest', array('id'=>$id))) {
        return false;
    }


    $DB->delete_records('askquest', array('id'=>$resource->id));

    return true;
}