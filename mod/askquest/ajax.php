<?php
define('AJAX_SCRIPT', true);
require_once('../../config.php');

$courseid = required_param('courseid', PARAM_INT);
$text = required_param('text', PARAM_TEXT);
$userid = optional_param('userid', 0, PARAM_INT);

require_once($CFG->libdir . '/externallib.php');
global $EDU;

$teacher = $EDU->getTeachers($courseid);
$t = reset($teacher);
$userid = $userid ? $userid : (int)$t->id;
define('PREFERRED_RENDERER_TARGET', RENDERER_TARGET_GENERAL);
$settings = external_settings::get_instance();
$settings->set_file('pluginfile.php');
$settings->set_fileurl(true);
$settings->set_filter(true);
$settings->set_raw(false);

$response = ['messages' => [['touserid' => $userid, 'text' => $text]]];
$response = external_api::call_external_function('core_message_send_instant_messages', $response, true);
var_dump($response);
?>