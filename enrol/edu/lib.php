<?php


defined('MOODLE_INTERNAL') || die();


class enrol_edu_plugin extends enrol_plugin {
   
    public function allow_unenrol_user(stdClass $instance, stdClass $ue) {
        

        return false;
    }



    /**
     * Gets an array of the user enrolment actions.
     *
     * @param course_enrolment_manager $manager
     * @param stdClass $ue A user enrolment object
     * @return array An array of user_enrolment_actions
     */
    public function get_user_enrolment_actions(course_enrolment_manager $manager, $ue) {
        $actions = array();
        return $actions;
    }




    public function sync_user_enrolments($user) {
        global $CFG, $DB;
        $edu = new local_edu\edu($user->id);
        $instances = $mycourses = [];
        if($programs = $edu->programs)
        {
            foreach ($programs as $p)
            {
                if($courses = $edu->getCourses($p->id))
                {
                    list($colors, $comments, $info, $completed, $access) = $edu->getInfoByCourses($p->id);
                    $i = 0;
                    foreach ($courses as $c)
                    {
                        if($access[$i] && ($course = $DB->get_record('course', array('id' => $c->id))))
                        {
                            $mycourses[] = $c->id;
                            $contexts[$course->id] = context_course::instance($course->id, MUST_EXIST);
                            if ($instance = $DB->get_record('enrol', array('courseid'=>$course->id, 'enrol'=>'edu'), '*', IGNORE_MULTIPLE)) {
                                $instances[$course->id] = $instance;
                                continue;
                            }

                            $enrolid = $this->add_instance($course);
                            $instances[$course->id] = $DB->get_record('enrol', array('id'=>$enrolid));

                        }
                        $i++;
                    }
                }
                if($forum = \local_edu\forum::getForumForProgram($p->id))
                {
                    $mycourses[] = $forum->id;
                    if ($instance = $DB->get_record('enrol', array('courseid'=>$forum->id, 'enrol'=>'edu'), '*', IGNORE_MULTIPLE)) {
                        $instances[$forum->id] = $instance;
                        continue;
                    }

                    $enrolid = $this->add_instance($forum);
                    $instances[$forum->id] = $DB->get_record('enrol', array('id'=>$enrolid));
                }
            }
        }
        foreach($instances as $courseid => $i)
        {
            if(!$DB->get_record('user_enrolments', array('enrolid'=>$i->id, 'userid'=>$user->id))) {
                $this->enrol_user($i, $user->id, 5, time(), 0, ENROL_USER_ACTIVE);
            }
        }
        if ($instances = $DB->get_records('enrol', array('enrol'=>'edu'))) {
            foreach($instances as $i)
            {
                if(!in_array($i->courseid, $mycourses))
                {
                    $this->unenrol_user($i, $user->id);
                }
            }
        }


    }

    
}
