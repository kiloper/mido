define(['jquery', 'jqueryui', 'local_edu/jstree'], function($, jui, t) {
    var pageurl = '';
    return {
        
        params:{
            jsTreeData: []
        },


        startJSTreeLib: function (data) {
            var app = this;
            app.params.jsTreeData = data;
        },
        
        init: function (url) {

            var app = this;
            pageurl = url;
            //var jsTreeData = [];
            var jsTreeSelected = [];
            var isjsTreeselected = [];
            var bookStart = 0;
            var bookStep = 12;
            var canBookNext = true;
            var lastNameBook = '';
            var currentScroll = 0;
            var lastScroll = 0;
            var checkType = $(".search-fields .type").val();
            var checkDidcipline = 0;
            var date1 = 0;
            var date2 = 0;

            var lastLocation = location.pathname + location.search + location.hash;


            function initSelectJSTreeLib(yu, data) {
                $(".jstree-container-info span").text(data.length);
                $("input[name='tags']").val(data.join(','));
                isjsTreeselected = data;
            }

            /*function startJSTreeLib(yu, data) {
                jsTreeData = data;
            }*/

            function startLibrarySearch(emptyList) {

                // Собираем все поля ввода со значениями
                if (emptyList) {
                    bookStart = 0;
                }
                var fields = [];
                $('.search-fields input[type="text"], .search-fields select').each(function (index, item) {
                    if ($(item).val()) {
                        var field = {
                            name: $(item).attr("name"),
                            value: $(item).val()
                        };
                        fields.push(field);
                    }
                });
                var fieldsStr = JSON.stringify(fields);
                console.log(fieldsStr);

                // Собираем все отмеченные чекбоксы
                var tags = jsTreeSelected;
                var tagsStr = JSON.stringify(tags);

                // ======= AJAX ============
                var loading = false;
                if (loading == false) {
                    loading = true;
                    if (emptyList) $(".booklist").empty();
                    $(".lib-loading").css('display', 'block');
                    $(".lib-loading-webinary").css('display', 'block');
                    $(".lib-showmore").hide();
                    $.ajax({
                        url: '/local/edu/lib/lib-ajax.php',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            type: $(".search-fields .type").val(),
                            action: "search",
                            fields: fieldsStr,
                            tags: tagsStr,
                            start: bookStart,
                            step: bookStep,
                            check: checkDidcipline,
                            date1: date1,
                            date2: date2
                        },
                        success: function (data) {
                            //console.log(data);
                            $(".lib-loading").hide();
                            $(".lib-loading-webinary").hide();
                            loading = false;
                            if (data) {
                                var colcount = data.length;
                                if (emptyList) $(".booklist").empty();
                                $(".booklist").append(data);
                                canBookNext = true;
                                if (checkType == 'webinary' && colcount < 6500)
                                    $(".lib-showmore").hide();
                                else
                                    $(".lib-showmore").show();
                            } else if (emptyList) {
                                $(".lib-showmore").hide();
                                $(".booklist").empty();
                                $(".booklist").append("По данному запросу ничего не найдено");
                            } else {
                                canBookNext = false;
                            }

                        }
                    });
                }
                // =========================

            }

            function getBookContent(id) {
                $(".lib-loading").css('display', 'block');
                $(".booklist").hide();
                $(".lib-showmore").hide();
                lastCanBookNext = canBookNext;
                var $namer = $("h4", $(".bookcontent").parent());
                canBookNext = false;
                lastScroll = currentScroll;
                $.ajax({
                    url: '/local/edu/lib/lib-ajax.php',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        action: "get_content",
                        id: id,
                        uc: $("#tabUc").val()
                    },
                    success: function (data) {
                        if (data) {
                            $(".lib-loading").hide();
                            $(".bookcontent").html(data.content);
                            $namer.text(data.name);
                        } else {
                            $(".lib-loading").hide();
                            $(".booklist").show();
                            canBookNext = lastCanBookNext;
                            $(window).scrollTop(lastScroll);
                        }
                        history.pushState(null, null, pageurl + '&id=' + id);
                    }
                });
            }


            function stopPlayVideo() {
                $('body').on('hidden.bs.modal', '.modal', function (e) {
                    $('video').trigger('pause');
                })
            }

            function processsingDatesDiscipline() {
                $(".lib-discipline-check").change(function () {
                    // alert(this.checked);
                    if (this.checked) {
                        date1 = Date.parse($("#lib-date1").val());
                        date1 = date1 / 1000;
                        date2 = Date.parse($("#lib-date2").val());
                        date2 = date2 / 1000;
                        checkDidcipline = 1;
                    }
                    else {
                        date1 = 0;
                        date2 = 0;
                        checkDidcipline = 0;
                    }
                });
            }


            function catchValInput() {
                if (checkType == 'discipline') {
                    $("#lib-title").change(function () {
                        startLibrarySearch(true);
                    });

                    $("#title_letter_books").change(function () {
                        startLibrarySearch(true);
                    });

                    $(".lib-discipline-check").change(function () {
                        startLibrarySearch(true);
                    });

                }
                else {
                    $(".search-fields input, .search-fields select").change(function () {
                        startLibrarySearch(true);
                    })
                }
            }

            $(document).ready(function () {

                /* $(".lib-discipline-check").change(function() {
                    // alert(this.checked);
                     if(this.checked) {
                         date1 =  Date.parse($("#lib-date1").val());
                         date1 = date1/1000;
                         date2 =  Date.parse($("#lib-date2").val());
                         date2 = date2/1000;
                         checkDidcipline = 1;
                     }
                     else {
                         date1 = 0;
                         date2 = 0;
                         checkDidcipline = 0;
                     }
                 });*/
                processsingDatesDiscipline();
                stopPlayVideo();
                var doExcel = false;
                $(".library .excel-library").click(function () {
                    if (doExcel) return false;
                    doExcel = true;
                    var name = $('span', this).text();
                    var a = this;
                    $('span', a).text('Подождите...');
                    $.ajax({
                        url: '/local/edu/lib/lib-ajax.php',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            action: 'excel',
                            check: checkDidcipline,
                            date1: date1,
                            date2: date2
                        },
                        success: function (data) {
                            $('span', a).text(name);
                            doExcel = false;
                            window.location.href = data;
                        }
                    });
                    return false;
                })


                $(this).on('mouseenter', '.lib_like:not(.like-voting) img', function () {
                    var alt = $(this).attr('alt');
                    $(".lib_like img[like-hover*='" + alt + "']").addClass('like-hover');
                });
                $(this).on('mouseleave', '.lib_like:not(.like-voting) img', function () {
                    var alt = $(this).attr('alt');
                    $(".lib_like img[like-hover*='" + alt + "']").removeClass('like-hover');
                });
                $(this).on('click', '.lib_like:not(.like-voting) img', function () {
                    var mark = $(this).attr('alt');
                    var webid = $(this).attr('webid');
                    if (mark)
                        $.ajax({
                            url: "/local/edu/lib/lib-ajax.php?action=setLikeWebinar",
                            data: {
                                mark: mark,
                                webid: webid
                            }, success: function (data) {
                                $(".lib-likes-container").html(data);
                            }
                        });
                });

                $(this).on('click', '#lib-webinary-view', function () {
                    var webid = $(this).attr('data-id');
                    $.ajax({url: "/local/edu/lib/lib-ajax.php?action=viewWebinar" + "&webid=" + webid});
                });


                if ($('.library').get().length) {
                    var $namer = $("h4", $(".bookcontent").parent());
                    lastNameBook = $namer.text();
                    $('.library').on('click', '.bookback', function () {
                        var col = $('.lib-block-full').length;
                        $(".booklist").show();
                        $(".bookcontent").empty();
                        if (checkType == 'webinary' && col >= 12) $(".lib-showmore").show();
                        $("h4", $(".bookcontent").parent()).text("Список вебинаров");

                        if (typeof lastCanBookNext !== 'undefined') {
                            canBookNext = lastCanBookNext;
                        }

                        $(window).scrollTop(lastScroll);
                        history.pushState(null, null, lastLocation);
                    });
                    $('.library').on('click', '.lib-block-name', function () {
                        var id = $(this).attr('data-itemid');
                        getBookContent(id);
                    })
                    if (checkType != 'webinary') {
                        $(window).scroll(function () {
                            var height = $(document).height();
                            var wheight = $(window).height();
                            //console.log(height - wheight - $(window).scrollTop());
                            currentScroll = $(window).scrollTop();
                            if (height - wheight - $(window).scrollTop() < 100) {
                                if (canBookNext) {
                                    canBookNext = false;
                                    bookStart += bookStep;
                                    startLibrarySearch(false);
                                }
                            }
                        });
                    }
                    else {
                        $(".lib-showmore").click(function () {
                            if (canBookNext) {
                                canBookNext = false;
                                $(".lib-showmore").hide();
                                bookStart += bookStep;
                                startLibrarySearch(false);
                            }
                        });
                    }

                }
                $(this).on('click', '.confirm-delete', function () {
                    if (confirm("Подтвердите удаление...")) {
                        var href = $(this).attr('href');
                        window.location.href = href;
                    }
                    return false;
                })
                $('.jstree-container').jstree({
                    "checkbox": {
                        "keep_selected_style": false
                    },
                    "plugins": ["checkbox"], 'core': {
                        'data': app.params.jsTreeData
                    }
                }).on("changed.jstree", function (e, data) {
                    jsTreeSelected = data.selected;
                    startLibrarySearch(true);
                });

                $('.jstree-container-edit').jstree({
                    "checkbox": {
                        "keep_selected_style": false
                    },
                    "plugins": ["checkbox"], 'core': {
                        'data': app.params.jsTreeData
                    }
                }).on("changed.jstree", function (e, data) {
                    jsTreeSelected = data.selected;
                    $(".jstree-container-info span").text(jsTreeSelected.length);
                    $("input[name='tags']").val(jsTreeSelected.join(','));
                });


                $("#editJsTree").click(function () {
                    window.location.href = pageurl + '&action=edit&id=' + jsTreeSelected;
                });

                $('.jstree-container-noncheck').jstree({
                    "checkbox": {
                        "keep_selected_style": false
                    },
                    'core': {
                        'data': app.params.jsTreeData
                    }
                }).on("changed.jstree", function (e, data) {
                    jsTreeSelected = data.selected;
                    $("#editJsTree").prop('disabled', false);
                });

                $(".lib-reset").click(function () {
                    $('.search-fields input[type="text"]').val("");
                    $('.jstree-container').jstree(true).deselect_all();
                    jsTreeSelected = [];
                    checkDidcipline = 0;
                    $(".lib-discipline-check").removeAttr("checked");
                    date1 = 0;
                    date2 = 0;
                    startLibrarySearch(true);
                });

                $(".lib-find").click(function () {
                    $(".lib-showmore").hide();
                    startLibrarySearch(true);
                });

                catchValInput();
                /*$(".search-fields input, .search-fields select").change(function () {

                    if(checkType == 'discipline') {
                        var sender = false;

                        $("#lib-date1").change(function () {
                            alert (9);
                            sender = true;

                        });

                        $("#lib-date2").change(function () {
                            sender = true;
                        });

                        if(!sender)
                        {
                            alert(sender);
                            startLibrarySearch(true);
                        }
                        else {
                            startLibrarySearch(true,true);
                        }


                    }
                    else {
                        startLibrarySearch(true);
                    }

                });*/


                $("#alphabet input").keyup(function (e) {
                    if (e.keyCode == 13) {
                        var container = $("#alphabet");
                        $("input", container).hide();
                        var letter = $("input", container).val();
                        if (!letter) letter = 'Все';
                        $('span', container).show().text(letter);
                    }
                    var letter = $(this).val();
                    if (!/[a-z|а-я|0-9|A-Z|А-Я]/.test(letter))
                        $(this).val('');


                });

                $('#alphabet').click(function () {
                    $('input', this).show().focus().select();
                    $('span', this).hide();
                });
                $('#alphabet input').focusout(function () {
                    var container = $("#alphabet");
                    $("input", container).hide();
                    var letter = $("input", container).val();
                    if (!letter) letter = 'Все';
                    $('span', container).show().text(letter);
                });

                $(this).click(function (e) {
                    var container = $("#alphabet");
                    if (container.has(e.target).length === 0) {
                        $("input", container).hide();
                        var letter = $("input", container).val();
                        if (!letter) letter = 'Все';
                        $('span', container).show().text(letter);
                    }
                });

                $('.letter').click(function () {
                    $('#letter-alphabet').show();
                    $('.letter').hide();
                });


            });

            $(function () {
                var cache = {};
                $("#groupSearch").autocomplete({
                    minLength: 3,
                    delay: 500,
                    source: function (request, response) {
                        var term = request.term;
                        if (term in cache) {
                            response(cache[term]);
                            return;
                        }

                        $.getJSON("/local/edu/lib/lib-ajax.php?action=getGroups&json=1", request, function (data, status, xhr) {
                            cache[term] = data;
                            response(data);
                        });
                    }
                });

            });

            $(function () {
                var cache = {};
                $(".thmemeSearch").autocomplete({
                    minLength: 5,
                    delay: 500,
                    /* select: function (event, ui) {
                         $(".thmemeSearch").val(ui.item.value);
                         $("#thmemeSearchID").val(ui.item.id);
                     },*/
                    source: function (request, response) {
                        var term = request.term;
                        if (term in cache) {
                            response(cache[term]);
                            return;
                        }
                        $.getJSON("/local/edu/lib/lib-ajax.php?action=getThemes&json=1", request, function (data, status, xhr) {
                            cache[term] = data;
                            response(data);
                        });
                    }
                });

            });
        }
    }

});