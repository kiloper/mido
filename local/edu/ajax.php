<?php

require_once("../../config.php");
require_once($CFG->libdir . '/externallib.php');
$action = required_param('action', PARAM_TEXT);

switch ($action) {
    case 'ident':
        $read = optional_param('read', false, PARAM_BOOL);
        $userid = required_param('userid', PARAM_INT);
        $field = optional_param('field', '', PARAM_TEXT);
        $comment = optional_param('comment', '', PARAM_TEXT);
        $edu = new \local_edu\edu($USER->id);
        $ident = $DB->get_record('edu_ident', ['userid' => $userid], '*', MUST_EXIST);
        if ($edu->isProgramsForManager()) {
            if ($field == 'cancel' || $field == 'ok') {
                $ident->status = $field == 'cancel' ? -1 : 1;
                $ident->adminid = $USER->id;
                $ident->timestatus = time();
                $ident->comment = $comment;
                $DB->update_record('edu_ident', $ident);
                if ($comment) {
                    define('AJAX_SCRIPT', true);
                    define('PREFERRED_RENDERER_TARGET', RENDERER_TARGET_GENERAL);
                    $settings = external_settings::get_instance();
                    $settings->set_file('pluginfile.php');
                    $settings->set_fileurl(true);
                    $settings->set_filter(true);
                    $settings->set_raw(false);
                    $response = ['messages' => [['touserid' => $userid, 'text' => $comment]]];
                    $response = external_api::call_external_function('core_message_send_instant_messages', $response, true);
                }
            } else {
                $fs = get_file_storage();
                $files = $fs->get_area_files(1, 'local_edu', 'ident', $ident->id);
                $url = '';
                foreach ($files as $file) {
                    if ($file->get_filename() == '.') continue;
                    if ($file->get_filename() == '..') continue;
                    $isimage = $file->is_valid_image();
                    $url = file_encode_url("$CFG->wwwroot/pluginfile.php",
                        '/' . $file->get_contextid() . '/' . $file->get_component() . '/' .
                        $file->get_filearea() . '/' . $file->get_itemid() . $file->get_filepath() . $file->get_filename(), !$isimage);


                }
                if($read)
                {
                    echo html_writer::tag('form', html_writer::div(
                        html_writer::div('<img src="' . $url . '" width="100%"/>', 'col-6') .
                        html_writer::div('<h4>Комментарий</h4>'.$ident->comment, 'col-6')

                        , 'row'));
                } else {
                    echo html_writer::tag('form', html_writer::div(
                        html_writer::div('<img src="' . $url . '" width="100%"/>', 'col-6') .
                        html_writer::div('<textarea style="width: 100%; height: 150px;" name="comment" placeholder="* Ваш комментарий..."></textarea>
                   <p>* Комментарий будет отправлен в личные сообщения ' . html_writer::link('/message/index.php?id=' . $userid, fullname(core_user::get_user($userid))) . '</p>', 'col-6')

                        , 'row'));
                }
            }

        }

        break;
}

?>