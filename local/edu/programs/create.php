<?php
// This file is part of Moodle - http:/
require_once('../../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot . '/local/edu/programs/form.php');
//require_once($CFG->dirroot.'/local/edu/programs/lib.php');
$id = optional_param("id", 0, PARAM_INT);
require_capability('local/edu:manage', context_system::instance());
admin_externalpage_setup('programs_create');
$systemcontext = context_system::instance();

if ($program = $DB->get_record('edu_programs', array('id' => $id))) {
    $fileoptions = array(
        'subdirs' => false,
        'maxbytes' => 0,
        'maxfiles' => -1,
        'changeformat' => 0,
        'context' => context_system::instance(),
        'noclean' => 1,
        'trusttext' => 0);
    $form = new program_form(null, array('program' => $program));
    $draftitemid = file_get_submitted_draft_itemid('files');
    file_prepare_draft_area($draftitemid, 1, 'local_edu', 'programs', $id, $fileoptions);
    $program->files = $draftitemid;
    /*$fileoptions = array('subdirs' => 0, 'maxbytes' => 1024 * 20 * 1024, 'maxfiles' => 1);
    $draftitemid = file_get_submitted_draft_itemid('img');
    file_prepare_draft_area($draftitemid, context_system::instance()->id, 'local_edu', 'img', $program->id, $fileoptions);
    $program->img = $draftitemid;
    $program->footer = ['text' => $program->footer, 'format' => FORMAT_HTML];*/
    $form->set_data($program);
} else
    $form = new program_form();
if ($data = $form->get_data()) {
    $data->duration = isset($data->duration) ? $data->duration : 0;
    //$data->footer = $data->footer['text'];
    if (empty($data->id)) {
        if ($id = $DB->insert_record('edu_programs', $data)) {
            $data->id = $id;
        }
    } else {
        $DB->update_record('edu_programs', $data);

    }
    if ($data->files) {
        file_save_draft_area_files($data->files, 1, 'local_edu', 'programs', $data->id, array('subdirs' => false));
    }
    if($data->id)
    {
        \local_edu\forum::forumForProgram($data);
    }
    /*if ($data->img) {
        $fileoptions = array('subdirs' => 0, 'maxbytes' => 1024 * 20 * 1024, 'maxfiles' => 1);
        file_save_draft_area_files($data->img, $systemcontext->id, 'local_edu', 'img', $data->id, $fileoptions);
    }*/
    redirect($CFG->wwwroot . '/local/edu/programs/index.php');

}

if ($form->is_cancelled()) {
    redirect($CFG->wwwroot . '/local/edu/programs/index.php');
}

echo $OUTPUT->header();
echo $OUTPUT->heading("Создание программы");

$form->display();

echo $OUTPUT->footer();


?>