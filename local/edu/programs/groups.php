<?php
// This file is part of Moodle - http:/
require_once('../../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot . '/local/edu/programs/form.php');
require_once($CFG->dirroot.'/local/edu/programs/locallib.php');
//require_once($CFG->dirroot.'/local/edu/programs/lib.php');
$id = optional_param("id", null, PARAM_INT);
$users = optional_param("users", null, PARAM_INT);
$delete = optional_param("delete", false, PARAM_BOOL);
require_capability('local/edu:manage', context_system::instance());
admin_externalpage_setup('programs_groups');
$systemcontext = context_system::instance();

echo $OUTPUT->header();
echo $OUTPUT->heading("Группы программы");
if($users)
{
    $options = ['pid' => $users];
    $potentialuserselector = new edu_groups_potential_participant('addselect', $options);
    $currentuserselector = new edu_groups_current_participant('removeselect', $options);
    if (optional_param('add', false, PARAM_BOOL) && confirm_sesskey()) {
        $userstoassign = $potentialuserselector->get_selected_users();
        if (!empty($userstoassign)) {
            foreach($userstoassign as $adduser) {
                $ob = new stdClass();
                $ob->groupid = $users;
                $ob->userid = $adduser->id;
                $DB->insert_record('edu_groups_members', $ob);
            }
            $potentialuserselector->invalidate_selected_users();
            $currentuserselector->invalidate_selected_users();
        }
        //\local_edu\group::sync($users);
    }

    if (optional_param('remove', false, PARAM_BOOL) && confirm_sesskey()) {
        $userstounassign = $currentuserselector->get_selected_users();
        if (!empty($userstounassign)) {
            foreach($userstounassign as $removeuser) {
                $DB->delete_records('edu_groups_members', array('groupid' => $users, 'userid' => $removeuser->id));
            }
            $potentialuserselector->invalidate_selected_users();
            $currentuserselector->invalidate_selected_users();
        }
        //\local_edu\group::sync($users);
    }


    echo '<form id="assignform" method="post" action="'.new moodle_url($PAGE->url, ['users' => $users]).'"><div>
            <input type="hidden" name="sesskey" value="'.sesskey().'" />

            <table summary="" class="roleassigntable generaltable generalbox boxaligncenter" cellspacing="0">
                <tr>
                    <td id="existingcell">
                        <p><label for="removeselect">'.print_string('enrolledusers', 'enrol').'</label></p>
                        ';
    $currentuserselector->display();
                    echo '</td>
                    <td id="buttonscell">
                        <div id="addcontrols">
                            <input name="add" id="add" type="submit" value="'.$OUTPUT->larrow().'&nbsp;'.get_string('add').'" title="'.print_string('add').'" class="btn btn-secondary" /><br />
                        </div>

                        <div id="removecontrols">
                            <input name="remove" class="btn btn-secondary" id="remove" type="submit" value="'.get_string('remove').'&nbsp;'.$OUTPUT->rarrow().'" title="'.print_string('remove').'" />
                        </div>
                    </td>
                    <td id="potentialcell">
                        <p><label for="addselect">'.print_string('enrolcandidates', 'enrol').'</label></p>
                        ';
                    $potentialuserselector->display();
                    echo '</td>
                </tr>
            </table>
        </div></form>';
}elseif($id !== null)
{
    if($delete)
    {
        $DB->delete_records("edu_groups", ['id' => $id]);
        $DB->delete_records("edu_groups_members", ['groupid' => $id]);
        //$DB->delete_records("edu_groups_subs", ['groupid' => $id]);
        redirect($PAGE->url);
    }
    $form = new edu_group_form();
    if($id)
        $form->set_data($DB->get_record('edu_groups', ['id' => $id]));
    if($data = $form->get_data())
    {
        if(empty($data->id))
        {
            $DB->insert_record('edu_groups', $data);
        } else
        {
            $DB->update_record('edu_groups', $data);
        }
        redirect($PAGE->url);
    }
    $form->display();

} else {


    $groups = $DB->get_records('edu_groups', [], 'name');
    $table = new html_table();
    $table->head = ["Название группы", "Начало обучения", 'Программа',  'Кол-во участников', ''];
    $table->data = [];
    foreach ($groups as $g) {
        $delimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => 'Удалить программу', 'class' => 'iconsmall', 'title' => 'Удалить программу'));
        $user = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/user'), 'alt' => 'Подписать', 'class' => 'iconsmall', 'title' => 'Подписать пользователей в группу'));
        $edit = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => 'Редактировать', 'class' => 'iconsmall', 'title' => 'Редактировать группу'));
        $edit = html_writer::link($CFG->wwwroot . '/local/edu/programs/groups.php?id=' . $g->id, $edit);
        $delete = $OUTPUT->action_link($CFG->wwwroot . '/local/edu/programs/groups.php?id=' . $g->id . "&delete=1", $delimg, new confirm_action("Удалить группу?", null));
        $data = [];
        $data[] = $g->name;
        $data[] = date("d.m.Y", $g->timestart);
        $programs = $DB->get_records_sql("select p.*, g.timestart from {edu_programs_subs} as s, {edu_programs} as p, {edu_groups} as g
        where g.id = ? and p.id = s.pid and s.groupid = g.id order by g.timestart DESC", [$g->id]);
        $phtml = [];
        foreach ($programs as $p)
        {
            $phtml[] = html_writer::tag('span', $p->name." до ". date('d.m.Y', $p->timestart + $p->duration * 3600 * 24));
        }
        $phtml = implode(',</br>', $phtml);
        $data[] = $phtml;
        $data[] = html_writer::link($CFG->wwwroot . '/local/edu/programs/groups.php?users=' . $g->id, $DB->count_records('edu_groups_members', ['groupid' => $g->id]));
        $data[] = $edit . ' ' . $delete;
        $table->data[] = $data;
    }
    echo $OUTPUT->single_button($CFG->wwwroot . '/local/edu/programs/groups.php?id=0', 'Создать новую группу');
    echo html_writer::table($table);
//$form->display();
}
echo $OUTPUT->footer();


?>