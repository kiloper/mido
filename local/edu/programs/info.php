<?php
require_once('../../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot . '/local/edu/programs/form.php');
$systemcontext = context_system::instance();

admin_externalpage_setup('programs_list');

$pid = required_param("pid", PARAM_INT);
$id = optional_param("id", null, PARAM_INT);
$delete = optional_param("delete", 0, PARAM_INT);
$program = $DB->get_record('edu_programs', array('id' => $pid));
$news = $DB->get_records_select('edu_programs_info', "pid = ? or pid = 0", [$pid]);
echo $OUTPUT->header();
echo $OUTPUT->heading("Новости программы. {$program->name}");
if($delete)
{
    $DB->delete_records('edu_programs_info', ['id' => $delete]);
    redirect(new moodle_url('/local/edu/programs/info.php', array('pid' => $pid)));
}
if($id !== null)
{
    $editoroptions = array(
        'subdirs' => 1,
        'maxbytes' => 0,
        'maxfiles' => -1,
        'changeformat' => 0,
        'context' => $systemcontext,
        'noclean' => 1,
        'trusttext' => 0);
    $form = new edu_info_form();
    if($data = $form->get_data())
    {
        $pid = $data->pid;
        $data->pid = $data->pid1;
        $data->content = $data->describe['text'];
        if($data->id)
            $DB->update_record('edu_programs_info', $data);
        else
            $data->id = $DB->insert_record('edu_programs_info', $data);

        if (!empty($data->describe['itemid'])) {
            $draftitemid = $data->describe['itemid'];
            $data->content = file_save_draft_area_files($draftitemid, $systemcontext->id, 'local_edu', 'info', $data->id, $editoroptions, $data->content);
        }
        $DB->update_record('edu_programs_info', $data);
        redirect(new moodle_url('/local/edu/programs/info.php', array('pid' => $pid)));
    }
    if($id)
    {
        $n = $DB->get_record('edu_programs_info', ['id' => $id]);
        $draftitemid = file_get_submitted_draft_itemid('describe');
        $mas = array();
        $mas['format'] = 1;
        $mas['text'] = file_prepare_draft_area($draftitemid, context_system::instance()->id, 'local_edu', 'info', $id, $editoroptions, $n->content);
        $mas['itemid'] = $draftitemid;
        $n->describe = $mas;
        $n->pid1 = $n->pid;
        $n->pid = $pid;
        $form->set_data($n);
    }
    if($form->is_cancelled())
        redirect(new moodle_url('/local/edu/programs/info.php', array('pid' => $pid)));
    $form->display();
} else {

    $actionurl = new moodle_url('/local/edu/programs/info.php', array('pid' => $pid, 'id' => 0));
    echo $OUTPUT->single_button($actionurl, "Создать новость", 'post');

    $table = new html_table();
    $table->head = array("Название", "Тип", "Управление");
    $num = array();


    $table->data = array();
    if ($news)
        foreach ($news as $tag) {
            $data = array($tag->name);
            $delimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'title' => 'Удалить новость', 'class' => 'iconsmall'));
            $delete = $OUTPUT->action_link($CFG->wwwroot . '/local/edu/programs/info.php?pid=' . $pid . '&delete=' . $tag->id, $delimg, new confirm_action("Удалить новость?", null));
            $data[] = $tag->pid ? "Новость программы" : "Общая новость";
            $data[] = html_writer::link(new moodle_url('/local/edu/programs/info.php', array('id' => $tag->id, 'pid' => $pid)), html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'title' => 'Редактировать', 'class' => 'iconsmall'))) . $delete;;
            $table->data[] = $data;
        }


    echo html_writer::table($table);
}
echo $OUTPUT->footer();

?>