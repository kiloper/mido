<?php
require_once("$CFG->libdir/formslib.php");
class program_list extends moodleform {

    public function definition() {
        global $DB;
        $defaultifertext = '';
        $editoroptionsifer = array(
            'subdirs'=>1,
            'maxbytes'=>0,
            'maxfiles'=>0,
            'changeformat'=>0,
            'context'=>context_system::instance(),
            'noclean'=>1,
            'trusttext'=>0);
        $mform = $this->_form; // Don't forget the underscore!
        $program = $this->_customdata['program'];
        $mform->addElement('hidden', 'id', $program->id);
        $mform->setType('id', PARAM_INT);


        $courses = $DB->get_records_sql('select c.*, cat.name from {course} as c, {course_categories} as cat  where c.id > 1 and c.category = cat.id order by cat.name, c.fullname');
        $i = 0;
        $cs = array();
        $groups = array();
        $groups1 = array();
        $cs['Выберите курс...'] = ['Выберите курс...'];
        foreach($courses as $c) {
            $n = shorten_text($c->name, 60);
            if(!isset($cs[$n])) $cs[$n] = [];
            $cs[$n][$c->id] = shorten_text($c->fullname, 60);
        }
        for($i = 1; $i < 41; $i++)$groups[$i] = $i;
        for($i = 1; $i < 41; $i++)$groups1[$i] = $i;
        $i = 0;

       if($pcourses = $DB->get_records_sql('select * from {edu_programs_courses} where pid = '.$program->id.' order by id'))
        {
            foreach($pcourses as $pc)
            {
                $mform->addElement('header', 'program'.$i, 'Дисциплина №'.($i+1));
                $mform->addElement('selectgroups', 'courseid'.$i, 'Выберите курс', $cs);
                $mform->setDefault('courseid'.$i, $pc->courseid);
                $mform->addRule('courseid'.$i, null, 'required');

                $mform->addElement('text', 'name'.$i, 'Название');
                $mform->setType('name'.$i, PARAM_TEXT);
                $mform->setDefault('name'.$i, $pc->name);

                $mform->addElement('text', 'hours'.$i, 'Количество часов');
                $mform->setType('hours'.$i, PARAM_INT);
                $mform->setDefault('hours'.$i, $pc->hours);

                $mform->addElement('text', 'open_course'.$i, 'Настройка открытия дисциплины через заданное кол-во дней');
                $mform->setType('open_course'.$i, PARAM_INT);
                $mform->setDefault('open_course'.$i, $pc->open_course);

                $mform->addElement('text', 'close_course'.$i, 'Настройка закрытия дисциплины через заданное кол-во дней');
                $mform->setType('close_course'.$i, PARAM_INT);
                $mform->setDefault('close_course'.$i, $pc->close_course);

                $mform->addElement('text', 'relativecourses'.$i, 'Курс откроется если пройдены дисциплины (номера дисциплин)');
                $mform->setType('relativecourses'.$i, PARAM_TEXT);
                $mform->setDefault('relativecourses'.$i, $pc->relativecourses);

                $mform->addElement('text', 'days'.$i, 'Продолжительность обучения (дн)');
                $mform->setType('days'.$i, PARAM_INT);
                $mform->setDefault('days'.$i, $pc->days);

                $mform->addElement('text', 'control'.$i, 'Форма аттестации');
                $mform->setType('control'.$i, PARAM_TEXT);
                $mform->setDefault('control'.$i, $pc->control);

                $mform->addElement('selectyesno', 'ident'.$i, 'Требовать идентификацию личности');
                $mform->setDefault('ident'.$i, $pc->ident);


                $i++;

            }
        }
        $mform->addElement('header', 'program'.$i, 'Дисциплина №'.($i+1));
        $mform->addElement('selectgroups', 'courseid'.$i, 'Выберите курс', $cs);
        $mform->addRule('courseid'.$i, null, 'required');

        $mform->addElement('text', 'name'.$i, 'Название курса');
        $mform->setType('name'.$i, PARAM_TEXT);

        $mform->addElement('text', 'hours'.$i, 'Количество часов');
        $mform->setType('hours'.$i, PARAM_INT);

        $mform->addElement('text', 'open_course'.$i, 'Настройка открытия дисциплины через заданное кол-во дней');
        $mform->setType('open_course'.$i, PARAM_INT);

        $mform->addElement('text', 'close_course'.$i, 'Настройка закрытия дисциплины через заданное кол-во дней');
        $mform->setType('close_course'.$i, PARAM_INT);

        $mform->addElement('text', 'relativecourses'.$i, 'Курс откроется если пройдены дисциплины (номера дисциплин)');
        $mform->setType('relativecourses'.$i, PARAM_TEXT);

        $mform->addElement('text', 'days'.$i, 'Продолжительность обучения (дн)');
        $mform->setType('days'.$i, PARAM_INT);
        $mform->setDefault('days'.$i, 0);

        $mform->addElement('text', 'control'.$i, 'Форма аттестации');
        $mform->setType('control'.$i, PARAM_TEXT);

        $mform->addElement('selectyesno', 'ident'.$i, 'Требовать идентификацию личности');

            $this->add_action_buttons();
    }

    function validation($data, $files) {
        global $DB;
        $error = parent::validation($data, $files);
        return $error;
    }
}

?>