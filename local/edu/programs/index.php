<?php
// This file is part of Moodle - http:/
require_once('../../../config.php');
require_once($CFG->libdir . '/adminlib.php');
//require_once($CFG->dirroot.'/local/edu/programs/lib.php');

$id = optional_param("id", 0, PARAM_INT);
$delete = optional_param("delete", 0, PARAM_INT);
$hide = optional_param("hide", 0, PARAM_INT);
$visible = optional_param("visible", 0, PARAM_INT);

require_capability('local/edu:manage', context_system::instance());
admin_externalpage_setup('programs_list');
//$isadmin = has_capability('local/edu:admin', context_system::instance());
echo $OUTPUT->header();
echo $OUTPUT->heading("Управление программами");

if ($hide) {
    $DB->set_field('edu_programs', 'visible', 0, ['id' => $hide]);
}
if ($visible) {
    $DB->set_field('edu_programs', 'visible', 1, ['id' => $visible]);
}
if ($delete) {
    if ($program = $DB->get_record('edu_programs', array('id' => $id))) {
        $DB->delete_records('edu_programs', array('id' => $id));
        $DB->delete_records('edu_programs_courses', array('pid' => $id));
        $DB->delete_records('edu_programs_subs', array('pid' => $id));
    }
}

$programs = $DB->get_records_sql("select * from {edu_programs} order by name");
if (empty($programs)) notice("Нет программ", $CFG->wwwroot . '/local/edu/programs/');

$table = new html_table();

$table->head = ["№", "Название программы", "Длительность обучения (дн.)"];
            $table->head[] = 'Описание';
            $table->head[] = 'Менеджер';
            $table->head[] = 'Управление';

    $table->data = [];
    $i = 0;
    foreach ($programs as $p) {
        $i++;
        $delimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => 'Удалить программу', 'class' => 'iconsmall', 'title' => 'Удалить программу'));
        $user = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/user'), 'alt' => 'Подписать', 'class' => 'iconsmall', 'title' => 'Подписать пользователей на программу'));
        $user = html_writer::link($CFG->wwwroot . '/local/edu/programs/manage.php?id=' . $p->id, $user);
        $edit = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => 'Редактировать', 'class' => 'iconsmall', 'title' => 'Редактировать программу'));
        $edit = html_writer::link($CFG->wwwroot . '/local/edu/programs/create.php?id=' . $p->id, $edit);
        $delete = $OUTPUT->action_link($CFG->wwwroot . '/local/edu/programs/index.php?id=' . $p->id . "&delete=1", $delimg, new confirm_action("Удалить программу?", null));
        $info = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/info'), 'alt' => 'Информация', 'class' => 'iconsmall', 'title' => 'Новости'));
        $info = html_writer::link($CFG->wwwroot . '/local/edu/programs/info.php?pid=' . $p->id, $info);
        $report = '';//html_writer::link($CFG->wwwroot . '/local/edu/programs/report.php?id=' . $p->id, html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/report'), 'title' => 'Отчет по программе', 'class' => 'iconsmall')));
        $report1 = '';//html_writer::link($CFG->wwwroot . '/local/edu/programs/report1.php?id=' . $p->id, html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/report'), 'title' => 'Отчет по уведомлениям', 'class' => 'iconsmall')));

        $hide = '';//html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/hide'), 'alt' => 'Скрыть программу', 'class' => 'iconsmall', 'title' => 'Скрыть программу'));
        $vis = '';//html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/show'), 'alt' => 'Показать программу', 'class' => 'iconsmall', 'title' => 'Показать программу'));

        $visible = '';

        if ($p->visible)
            $visible = html_writer::link("/local/edu/programs/?hide=" . $p->id, $hide);
        else
            $visible = html_writer::link("/local/edu/programs/?visible=" . $p->id, $vis);

        $a = html_writer::span(html_writer::link($CFG->wwwroot . '/local/edu/programs/list.php?id=' . $p->id, $p->name), $p->visible ? "" : "unactive");
        /*if(!$isadmin)
            $a = html_writer::span(html_writer::link($CFG->wwwroot . '/local/edu/programs/report.php?id=' . $p->id, $p->name), $p->visible ? "" : "unactive");*/
        $table->data[$i - 1] = array($i, $a . " ", $p->duration, $p->description);
            $table->data[$i - 1][] = $p->managerid ? html_writer::link(new moodle_url('/user/profile.php?id='.$p->managerid), fullname(core_user::get_user($p->managerid))) : '-';
            $table->data[$i - 1][] = $user . "&nbsp&nbsp" . $edit . "&nbsp&nbsp" . $delete . "&nbsp&nbsp" . $info . "&nbsp&nbsp" . $visible;
    }

echo html_writer::table($table);

echo $OUTPUT->footer();


?>