<?php
require_once('../../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot . '/local/edu/programs/formlist.php');
//require_once($CFG->dirroot.'/local/edu/locallib.php');
$id = required_param("id", PARAM_INT);

require_capability('local/edu:manage', context_system::instance());
admin_externalpage_setup('programs_list');

$program = $DB->get_record('edu_programs', array('id' => $id));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/admin/tool/reliz/multiple/jquery.multiple.select.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/admin/tool/reliz/multiple/code.js'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/admin/tool/reliz/multiple/multiple-select.css'));
echo $OUTPUT->header();
echo $OUTPUT->heading("Редактирование курсов программы - " . $program->name);

$form = new program_list(null, array('program' => $program));

if ($form->is_cancelled()) {
    redirect($CFG->wwwroot . '/local/edu/programs/index.php');
}

$program = $DB->get_record('edu_programs', array('id' => $id));
if ($data = $form->get_data()) {
    $i = 0;
    $str = 'courseid' . $i;
    $DB->delete_records('edu_programs_courses', array('pid' => $data->id));
    while (isset($data->$str)) {
        if ($data->$str) {
            $name = 'name' . $i;
            $open_course = 'open_course' . $i;
            $close_course = 'close_course' . $i;
            $ident = 'ident' . $i;
            $sampleid = 'sampleid' . $i;
            $days = 'days' . $i;
            $ob = new stdClass();
            $ob->pid = $data->id;
            $ob->courseid = $data->$str;
            $ob->name = $data->$name;
            $ob->hours = $data->{'hours'.$i};
            $ob->relativecourses = $data->{'relativecourses'.$i};
            $ob->control = $data->{'control'.$i};
            $ob->open_course = isset($data->$open_course) ? $data->$open_course : 0;
            $ob->close_course = isset($data->$close_course) ? $data->$close_course : 0;
            $ob->ident = isset($data->$ident) ? $data->$ident : 0;
            $ob->days = isset($data->$days) ? $data->$days : 0;
            $ob->sampleid = 0;//isset($data->$sampleid) ? $data->$sampleid : 0;
            $DB->insert_record('edu_programs_courses', $ob);


        }
        $i++;
        $str = 'courseid' . $i;
    }
    redirect($CFG->wwwroot . '/local/edu/programs/list.php?id=' . $id);

}
$form->display();
echo $OUTPUT->footer();


?>