<?php
require_once("$CFG->libdir/formslib.php");
class edu_group_form extends moodleform {
    public function definition() {
        global $DB;
        $mform = $this->_form; // Don't forget the underscore!
        $mform->addElement('header', 'program', "Группа");

        $mform->addElement('hidden', 'id', null);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('text', 'name', 'Название группы');
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required');

        $mform->addElement('date_time_selector', 'timestart', 'Начало обучения');
        $mform->setType('timestart', PARAM_INT);
        $mform->addRule('timestart', null, 'required');

        $this->add_action_buttons(true, "Сохранить");
    }
    //Custom validation should be added here
    function validation($data, $files) {
        global $DB;
        $error = parent::validation($data, $files);
        if(empty($data['id']))
            if($DB->record_exists('edu_programs', array('name' => $data['name'])))
                $error['name'] = 'В одном уч. центре не может быть программ с одним названием';

        return $error;
    }
}
class program_form extends moodleform {

    public function definition() {
        global $DB;
        $mform = $this->_form; // Don't forget the underscore!
        $mform->addElement('header', 'program', "Программа");

        $mform->addElement('hidden', 'id', null);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('text', 'name', 'Название программы');
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required');

        $mform->addElement('text', 'duration', 'Длительность обучения (дн.)');
        $mform->setType('duration', PARAM_INT);
        $mform->addRule('duration', null, 'required');

        $mform->addElement('textarea', 'description', "Описание программы");
        $mform->setType('description', PARAM_TEXT);

        $users = [0 => "Выберите менеджера..."] + $DB->get_records_sql_menu("SELECT u.id, concat(u.lastname, ' ', u.firstname) as fio
                             FROM {role_assignments} cra JOIN {user} u ON cra.userid = u.id
                            WHERE cra.roleid = 1 AND cra.contextid = 1 AND u.deleted = 0 order by fio");

        $mform->addElement('select', 'managerid', "Менеджер программы", $users);
        $mform->setType('managerid', PARAM_INT);



        $filemanager_options['accepted_types'] = array('*');
        $filemanager_options['subdirs'] = false;
        $mform->addElement('filemanager', 'files', "Файлы программы", null, $filemanager_options);


        $mform->addElement('textarea', 'filedesc', "Описание файлов (с новой строки описание файла последовательно)", ['style' => 'width: 100%; height: 150px;']);
        $mform->setType('filedesc', PARAM_TEXT);

        //$mform->addElement('filepicker', 'img', "Картинка программы", null, array('maxbytes' => 1024 * 20 * 1024, 'accepted_types' => array('image')));

        //$mform->addElement('editor', 'footer', "Footer");
        //$mform->setType('footer', PARAM_RAW);

        /*$mform->addElement('text', 'sortorder', "Сортировка");
        $mform->setType('sortorder', PARAM_INT);
        $mform->setDefault('sortorder', 0);*/

        $this->add_action_buttons();
    }
    //Custom validation should be added here
    function validation($data, $files) {
        global $DB;
        $error = parent::validation($data, $files);
        if(empty($data['id']))
            if($DB->record_exists('edu_programs', array('name' => $data['name'])))
                $error['name'] = 'В одном уч. центре не может быть программ с одним названием';

        return $error;
    }
}

class edu_sample_form extends moodleform {


    public function definition() {
        global $DB;
        $mform = $this->_form; // Don't forget the underscore!
        $mform->addElement('header', 'serach', "Шаблон");

        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_INT);
        $mform->addElement('text', 'name', 'Название');
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required');

        $mform->addElement('text', 'days', 'За сколько дней до завершения курса отправлять?');
        $mform->setType('days', PARAM_INT);
        $mform->addRule('days', null, 'required');




        $editoroptions = array(
            'subdirs'=>1,
            'maxbytes'=>0,
            'maxfiles'=>0,
            'changeformat'=>0,
            'context'=>context_system::instance(),
            'noclean'=>1,
            'trusttext'=>0);
        $mform->addElement('text', 'subject', "Тема новости", null, $editoroptions);
        $mform->addElement('editor', 'describe', "Текст новости", null, $editoroptions);
        $mform->setType('describe', PARAM_RAW);
        $mform->setType('subject', PARAM_TEXT);
        $mform->addRule('subject', null, 'required', null, 'client');
        $mform->addRule('describe', null, 'required', null, 'client');

        $this->add_action_buttons(true, "Сохранить");

    }



}


class edu_info_form extends moodleform
{


    public function definition()
    {
        global $DB;
        $mform = $this->_form; // Don't forget the underscore!
        $mform->addElement('header', 'serach', "Новость");

        $mform->addElement('hidden', 'pid');
        $mform->addElement('hidden', 'id');
        $mform->setType('pid', PARAM_INT);
        $mform->setType('id', PARAM_INT);


        $mform->addElement('text', 'name', 'Название');
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required');

        $opt = [0 => 'Общая новость', 1 => 'Новость программы'];

        $mform->addElement('select', 'pid1', "Тип новости", $opt);

        $editoroptions = array(
            'subdirs' => 1,
            'maxbytes' => 0,
            'maxfiles' => -1,
            'changeformat' => 0,
            'context' => context_system::instance(),
            'noclean' => 1,
            'trusttext' => 0);
        $mform->addElement('editor', 'describe', "Информация", null, $editoroptions);
        $mform->setType('describe', PARAM_RAW);
        $mform->addRule('describe', null, 'required', null, 'client');


        if ($id = optional_param("id", 0, PARAM_INT)) {
                $this->add_action_buttons(true, "Сохранить");
        } else
            $this->add_action_buttons(true, "Создать");
    }

    //Custom validation should be added here
    function validation($data, $files)
    {
        global $DB;
        $error = parent::validation($data, $files);
        /*if(empty($data['id']))
        if($DB->get_field('edu_orgs', 'id', array('name' => trim($data['name'])))) $error['name'] = 'Организация с таким именем уже есть';*/
        return $error;
    }


}

?>