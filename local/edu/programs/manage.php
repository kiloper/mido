<?php
require('../../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot.'/local/edu/programs/locallib.php');

$pid = required_param('id', PARAM_INT);
$program = $DB->get_record('edu_programs', array('id' => $pid));
admin_externalpage_setup('programs_list');

$myday   = optional_param('myday', 0, PARAM_INT);
$mymonth   = optional_param('mymonth', 0, PARAM_INT);
$myyear   = optional_param('myyear', 0, PARAM_INT);
$myhour   = optional_param('myhour', 0, PARAM_INT);
$myminute   = optional_param('myminute', 0, PARAM_INT);

$timer = strtotime("$myday.$mymonth.$myyear $myhour:$myminute");
$timer = $timer ? $timer : time();

echo $OUTPUT->header();
echo $OUTPUT->heading("Запись пользователей на программу ($program->name)");

$PAGE->set_url('/local/edu/programs/manage.php', array('id'=>$pid));
$PAGE->set_title("Запись пользователей на программу");
$PAGE->set_heading("Запись пользователей на программу");

$options = ['pid' => $pid];

$potentialuserselector = new enrol_manual_potential_participant('addselect', $options);
$currentuserselector = new enrol_manual_current_participant('removeselect', $options);


if (optional_param('add', false, PARAM_BOOL) && confirm_sesskey()) {
    $userstoassign = $potentialuserselector->get_selected_users();
    if (!empty($userstoassign)) {
        foreach($userstoassign as $adduser) {

            $ob = new stdClass();
            $ob->pid = $pid;
            $ob->groupid = $adduser->id;
            $DB->insert_record('edu_programs_subs', $ob);
        }
        $potentialuserselector->invalidate_selected_users();
        $currentuserselector->invalidate_selected_users();
    }

}

if (optional_param('remove', false, PARAM_BOOL) && confirm_sesskey()) {
    $userstounassign = $currentuserselector->get_selected_users();
    if (!empty($userstounassign)) {
        foreach($userstounassign as $removeuser) {
            $DB->delete_records('edu_programs_subs', array('pid' => $pid, 'groupid' => $removeuser->id));
        }
        $potentialuserselector->invalidate_selected_users();
        $currentuserselector->invalidate_selected_users();
    }
}

if($program->duration)
    echo html_writer::div("Продолжительнось обучения на данной программе {$program->duration} дн. с даты подписки. После студенты отпишутся автоматически.", "alert alert-info");
?>

    <form id="assignform" method="post" action="<?php echo $PAGE->url ?>"><div>
            <input type="hidden" name="sesskey" value="<?php echo sesskey() ?>" />

            <table summary="" class="roleassigntable generaltable generalbox boxaligncenter" cellspacing="0">
                <tr>
                    <td id="existingcell">
                        <p><label for="removeselect">Записанные группы</label></p>
                        <?php $currentuserselector->display() ?>
                    </td>
                    <td id="buttonscell">
                        <div id="addcontrols">
                            <input name="add" id="add" type="submit" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" class="btn btn-secondary" /><br />


                        </div>

                        <div id="removecontrols">
                            <input name="remove" class="btn btn-secondary" id="remove" type="submit" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" />
                        </div>
                    </td>
                    <td id="potentialcell">
                        <p><label for="addselect">Незаписанные группы</label></p>
                        <?php $potentialuserselector->display() ?>
                    </td>
                </tr>
            </table>
        </div></form>

<?php

echo $OUTPUT->footer();
