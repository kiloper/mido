<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Auxiliary manual user enrolment lib, the main purpose is to lower memory requirements...
 *
 * @package    enrol_manual
 * @copyright  2010 Petr Skoda {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/user/selector/lib.php');
require_once($CFG->dirroot . '/enrol/locallib.php');





/**
 * Enrol candidates.
 */
class edu_groups_potential_participant extends user_selector_base {
    protected $pid;

    public function __construct($name, $options) {
        $this->pid  = $options['pid'];
        parent::__construct($name, $options);
    }

    /**
     * Candidate users
     * @param string $search
     * @return array
     */
    public function find_users($search) {
        global $DB;
        // By default wherecondition retrieves all users except the deleted, not confirmed and guest.
        list($wherecondition, $params) = $this->search_sql($search, 'u');
        $params['pid'] = $this->pid;

        $fields      = 'SELECT ' . $this->required_fields_sql('u');
        $countfields = 'SELECT COUNT(1)';

        $sql = " FROM {user} u
            LEFT JOIN {edu_groups_members} ue ON (ue.userid = u.id AND ue.groupid = :pid)
                WHERE $wherecondition
                      AND ue.id IS NULL";

        list($sort, $sortparams) = users_order_by_sql('u', $search, $this->accesscontext);
        $order = ' ORDER BY ' . $sort;

        if (!$this->is_validating()) {
            $potentialmemberscount = $DB->count_records_sql($countfields . $sql, $params);
            if ($potentialmemberscount > $this->maxusersperpage) {
                return $this->too_many_results($search, $potentialmemberscount);
            }
        }

        $availableusers = $DB->get_records_sql($fields . $sql . $order, array_merge($params, $sortparams));

        if (empty($availableusers)) {
            return array();
        }


        if ($search) {
            $groupname = get_string('enrolcandidatesmatching', 'enrol', $search);
        } else {
            $groupname = get_string('enrolcandidates', 'enrol');
        }

        return array($groupname => $availableusers);
    }

    protected function get_options() {
        $options = parent::get_options();
        $options['pid'] = $this->pid;
        $options['file']    = 'local/edu/programs/locallib.php';
        return $options;
    }
}
class enrol_manual_potential_participant extends user_selector_base {
    protected $pid;

    public function __construct($name, $options) {
        $this->pid  = $options['pid'];
        parent::__construct($name, $options);
    }
    public function output_user($user) {
        $out = $user->name;
        return $out;
    }

    /**
     * Candidate users
     * @param string $search
     * @return array
     */
    public function find_users($search) {
        global $DB;
        // By default wherecondition retrieves all users except the deleted, not confirmed and guest.
        list($wherecondition, $params) = $this->search_sql($search, 'u');
        $params['pid'] = $this->pid;

        $fields      = 'SELECT g.*';
        $countfields = 'SELECT COUNT(1)';

        $sql = " FROM {edu_groups} g
            LEFT JOIN {edu_programs_subs} s ON (s.groupid = g.id AND s.pid = :pid)
                WHERE s.id IS NULL";

        $order = ' ORDER BY g.name';

        if (!$this->is_validating()) {

        }

        $availableusers = $DB->get_records_sql($fields . $sql . $order, array_merge($params));

        if (empty($availableusers)) {
            return array();
        }




        return array('Группы' => $availableusers);
    }

    protected function get_options() {
        $options = parent::get_options();
        $options['pid'] = $this->pid;
        $options['file']    = 'local/edu/programs/locallib.php';
        return $options;
    }
}


class edu_groups_current_participant extends user_selector_base {
    protected $pid;

    public function __construct($name, $options) {
        $this->pid  = $options['pid'];
        parent::__construct($name, $options);
    }

    public function find_users($search) {
        global $DB;
        // By default wherecondition retrieves all users except the deleted, not confirmed and guest.
        list($wherecondition, $params) = $this->search_sql($search, 'u');
        $params['pid'] = $this->pid;

        $fields      = 'SELECT ' . $this->required_fields_sql('u');
        $countfields = 'SELECT COUNT(1)';

        $sql = " FROM {user} u
                 JOIN {edu_groups_members} ue ON (ue.userid = u.id AND ue.groupid = :pid)
                WHERE $wherecondition";

        list($sort, $sortparams) = users_order_by_sql('u', $search, $this->accesscontext);
        $order = ' ORDER BY lastname,' . $sort;

        if (!$this->is_validating()) {
            $potentialmemberscount = $DB->count_records_sql($countfields . $sql, $params);
            if ($potentialmemberscount > $this->maxusersperpage) {
                return $this->too_many_results($search, $potentialmemberscount);
            }
        }

        $availableusers = $DB->get_records_sql($fields . $sql . $order, array_merge($params, $sortparams));

        if (empty($availableusers)) {
            return array();
        }


        if ($search) {
            $groupname = get_string('enrolledusersmatching', 'enrol', $search);
        } else {
            $groupname = get_string('enrolledusers', 'enrol');
        }

        return array($groupname => $availableusers);
    }

    protected function get_options() {
        $options = parent::get_options();
        $options['pid'] = $this->pid;
        $options['file']    = 'local/edu/programs/locallib.php';
        return $options;
    }
}
class enrol_manual_current_participant extends user_selector_base {
    protected $pid;

    public function __construct($name, $options) {
        $this->pid  = $options['pid'];
        parent::__construct($name, $options);
    }
    public function output_user($user) {
        $out = $user->name;
        return $out;
    }

    /**
     * Candidate users
     * @param string $search
     * @return array
     */
    public function find_users($search) {
        global $DB;
        // By default wherecondition retrieves all users except the deleted, not confirmed and guest.
        list($wherecondition, $params) = $this->search_sql($search, 'u');
        $params['pid'] = $this->pid;

        $fields      = 'SELECT g.*';
        $countfields = 'SELECT COUNT(1)';

        $sql = " FROM {edu_groups} g
                 JOIN {edu_programs_subs} s ON (s.groupid = g.id AND s.pid = :pid)";

        $order = ' ORDER BY name';

        if (!$this->is_validating()) {
            $potentialmemberscount = $DB->count_records_sql($countfields . $sql, $params);
        }

        $availableusers = $DB->get_records_sql($fields . $sql . $order, array_merge($params));

        if (empty($availableusers)) {
            return array();
        }




        return array('Группы' => $availableusers);
    }

    protected function get_options() {
        $options = parent::get_options();
        $options['pid'] = $this->pid;
        $options['file']    = 'local/edu/programs/locallib.php';
        return $options;
    }
}


?>


