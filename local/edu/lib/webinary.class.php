<?php

namespace edu;

global $CFG;

use theme_edu\modal;

require_once("$CFG->libdir/formslib.php");


class webinary
{
    private static $LIB_WEBINARY = "edu_lib_webinary";
    private static $LIB_TAGS = "edu_lib_tags";
    public static $admin = true;
    public static $uc = 0;
    public static $tabname = 'libtab';
    public static $tabvalue = '1';
    private static $FIELDS = array(
        'id' => false,
        'title' => false,
        'author' => "Автор/Ведущий",
        'description' => 'Описание вебинара',
        'duration' => 'Продолжительность вебинара (ЧЧ:ММ:СС)',
        'sector' => "Отрасль",
        'groupid' => false,
        'dateview' => "Дата проведения вебинара",
        'tags' => false,
        'course' => "Курс",
        'video' => false,
        'poster' => false,
        'rating' => false,
        'dateinsert' => false,
        'presentation' => "Презентация",
        'otherfiles' => false
    );

    /**
     * Создание и редактирование записи о вебинаре. Выводит форму для работы с вебинаром
     * @param int $id - ID книги, если 0 то создание нового вебинара
     */
    static function delete($id)
    {
        global $DB, $PAGE;
        $DB->delete_records(self::$LIB_WEBINARY, array("id" => $id));
        $DB->delete_records('edu_lib_webinary_groups_items', array('webid' => $id));
        $DB->delete_records('edu_lib_webinary_recomended_sorted', array('webid' => $id));
        $fs = get_file_storage();
        $fs->delete_area_files(1, 'local_edu', 'poster', $id);
        $fs->delete_area_files(1, 'local_edu', 'video', $id);
        $fs->delete_area_files(1, 'local_edu', 'presentation', $id);
        $fs->delete_area_files(1, 'local_edu', 'otherfiles', $id);
        redirect(new \moodle_url($PAGE->url));
    }

    public static function edit($id = 0)
    {
        global $DB, $PAGE;
        $context = \context_system::instance();
        $form = new webinary_form($PAGE->url, $id);
        if ($id) {
            $data = $DB->get_record(self::$LIB_WEBINARY, array("id" => $id));
            $fileoptions = array(
                'subdirs' => 1,
                'maxbytes' => 0,
                'maxfiles' => -1,
                'changeformat' => 0,
                'context' => $context,
                'noclean' => 1,
                'trusttext' => 0
            );
            $draftitemid = file_get_submitted_draft_itemid('poster');
            file_prepare_draft_area($draftitemid, $context->id, 'local_edu', 'libposter', $id, $fileoptions);
            $data->poster = $draftitemid;

            $draftitemid = file_get_submitted_draft_itemid('file');
            file_prepare_draft_area($draftitemid, $context->id, 'local_edu', 'libfile', $id, $fileoptions);
            $data->file = $draftitemid;
            $form->set_data($data);
        }

        if ($data = $form->get_data()) {
            $data->groupid = 0;
            if ($data->qroupname) {
                //$data->groupid = self::getGroupWebinar($data->qroupname);
            }
            $tags = optional_param('tags', '', PARAM_TEXT);
            $data->tags = '';
            if ($tags) $data->tags = ",$tags,";
            //определяем поле Author и userid

            $data->userid = implode(',', $data->userid);
            //$user = \core_user::get_user($data->userid);
            //$data->author = fullname($user);
            $data->author = '';
            if ($data->id == 0) {
                //Добавляем новую запись
                $data->dateinsert = time();
                if ($libid = $DB->insert_record(self::$LIB_WEBINARY, $data)) {
                    if ($data->qroupname) {
                        foreach ($data->qroupname as $gr) {
                            $ob = new \stdClass();
                            $ob->webid = $libid;
                            $ob->groupid = $gr;
                            $DB->insert_record('edu_lib_webinary_groups_items', $ob);
                        }
                    }
                    if ($data->poster) {
                        file_save_draft_area_files($data->poster, $context->id, 'local_edu', 'libposter', $libid, array('subdirs' => false));
                    }
                    if ($data->file) {
                        file_save_draft_area_files($data->file, $context->id, 'local_edu', 'libfile', $libid, array('subdirs' => false));
                    }

                }
            } else {


                $DB->update_record(self::$LIB_WEBINARY, $data);

                if ($data->poster) {
                    file_save_draft_area_files($data->poster, $context->id, 'local_edu', 'libposter', $data->id, array('subdirs' => false));
                }
                if ($data->file) {
                    file_save_draft_area_files($data->file, $context->id, 'local_edu', 'libfile', $data->id, array('subdirs' => false));
                }


            }
            redirect(new \moodle_url($PAGE->url, ['action' => '']));
        }
        return $form->render();
    }

    public static function getGroupWebinar($name)
    {
        global $DB;
        $groupid = $DB->get_field_sql("select id from {edu_lib_webinary_groups} where name like '%$name'");
        if (empty($groupid)) {
            $ob = new \stdClass();
            $ob->name = $name;
            $groupid = $DB->insert_record('edu_lib_webinary_groups', $ob);
        }
        return $groupid;
    }


    public static function getUserRating($webid)
    {
        global $DB, $USER;
        return $DB->get_field('edu_lib_webinary_liker', 'mark', array('webid' => $webid, 'userid' => $USER->id));
    }


    public static function getRaitingWebinar($webid)
    {
        global $DB;
        $ball = $DB->get_field_sql("select round(rating,1) from {edu_lib_webinary} where id = $webid");
        return $ball ? $ball : 0;
    }

    /**
     * Вывод книг в виже списка
     * @return String $html - HTML список книг
     */
    public static function printRating($item, $asajax = false)
    {
        $isvoting = (int)self::getUserRating($item->id);
        $like = \html_writer::div(\html_writer::div(\html_writer::img(("/local/edu/lib/img/like_lib_lomonosov.png"), 1, ['class' => $isvoting > 0 ? 'voting' : '', 'like-hover' => '1,2,3,4,5', 'webid' => $item->id])) .
            \html_writer::div(\html_writer::img(("/local/edu/lib/img/like_lib_lomonosov.png"), 2, ['class' => $isvoting > 1 ? 'voting' : '', 'like-hover' => '2,3,4,5', 'webid' => $item->id])) .
            \html_writer::div(\html_writer::img(("/local/edu/lib/img/like_lib_lomonosov.png"), 3, ['class' => $isvoting > 2 ? 'voting' : '', 'like-hover' => '3,4,5', 'webid' => $item->id])) .
            \html_writer::div(\html_writer::img(("/local/edu/lib/img/like_lib_lomonosov.png"), 4, ['class' => $isvoting > 3 ? 'voting' : '', 'like-hover' => '4,5', 'webid' => $item->id])) .
            \html_writer::div(\html_writer::img(("/local/edu/lib/img/like_lib_lomonosov.png"), 5, ['class' => $isvoting > 4 ? 'voting' : '', 'like-hover' => '5', 'webid' => $item->id])), 'lib_like ' . ($isvoting ? 'like-voting' : ''));

        $text = \html_writer::div($isvoting ? "Вы уже проголосовали" : 'Оцените вебинар', 'text_description');
        $raiting = \html_writer::div('Рейтинг -' . ' ' . self::getRaitingWebinar($item->id), 'text_raiting_webinar');
        $html = $raiting . $like . $text;
        if ((!$asajax)) $html = \html_writer::div($html, 'lib-likes-container');
        return $html;
    }

    public static function getRecommendWebinar($item, $groupid = [], $editForm = false, $idgroup = 0)
    {
        global $DB;
        $html = '';
        $liker = '';
        if (!empty($groupid)) {
            $groupid = implode(',', $groupid);
            $liker = " AND i.groupid in ($groupid)";
        } else {
            return $html;
        }
        //$rec = $DB->get_records_sql("select * from {edu_lib_webinary} where groupid > 0 and groupid = $item->groupid and id <> $item->id");
        /* $rec = $DB->get_records_sql("SELECT lib.* FROM {edu_lib_webinary} as lib,
                                         {edu_lib_webinary_groups_items} as i
                                          WHERE lib.id <> $item->id AND
                                          lib.id = i.webid $liker");*/

        $groupid = explode(',', $groupid);
        $liker = '';
        foreach ($groupid as $g) {
            $liker = " AND i.groupid in ($g)";
            if ($idgroup) $liker = " AND i.groupid in ($idgroup)";
            $rec = $DB->get_records_sql("SELECT lib.*,s.webid2,s.sorted,i.groupid as idgroup FROM {edu_lib_webinary} as lib 
                                   left join {edu_lib_webinary_recomended_sorted} as s on (s.webid = $item->id and s.webid2 = lib.id),
                                        {edu_lib_webinary_groups_items} as i 
                                         WHERE /*lib.id <> $item->id 
                                         AND*/
                                         lib.id = i.webid $liker
                                         ORDER BY s.sorted ASC");
            if ($editForm) return $rec;
            if (!empty($rec)) {
                $html .= '<br><br>';
                $html .= \html_writer::div('Данный вебинар входит в серию. Рекомендуется просматривать вебинары в данной последовательности:', 'recomend_webinars') . '<br>';
                $html .= self::getList($rec);
            }
        }
        return $html;


        //if($getContent) return $rec;
        /* if (!empty($rec)) {
             $html .= '<br><br>';
             $html .= \html_writer::div('Данный вебинар входит в серию. Рекомендуется просматривать вебинары в данной последовательности:', 'recomend_webinars') . '<br>';
             $html .= self::getList($rec);
             return $html;
         } else {
             return $html;
         }*/
    }


    public static function getUserBrowser()
    {
        $user_agent = $_SERVER["HTTP_USER_AGENT"];
        if (strpos($user_agent, "Firefox") !== false)
            $browser = "Firefox";
        elseif (strpos($user_agent, "Opera") !== false)
            $browser = "Opera";
        elseif (strpos($user_agent, "Chrome") !== false)
            $browser = "Chrome";
        elseif (strpos($user_agent, "MSIE") !== false)
            $browser = "Internet Explorer";
        elseif (strpos($user_agent, "Safari") !== false)
            $browser = "Safari";
        else $browser = "Неизвестный";

        return $browser;
    }

    public static function getContent($id)
    {
        global $DB, $OUTPUT, $PAGE;
        $ob = new \stdClass();
        $ob->content = '';
        $ob->name = '';
        $fs = get_file_storage();
        if ($item = $DB->get_record(self::$LIB_WEBINARY, array("id" => $id))) {
            $editimg = \html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/settings'), 'title' => 'Редактировать', 'class' => ''));
            $deleteimg = \html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'title' => 'Удалить', 'class' => ''));
            $edit = " " . \html_writer::link(new \moodle_url('/local/edu/lib/index.php', array('id' => $item->id, 'action' => 'edit', 'libtab' => 1)), $editimg) .
                " " . \html_writer::link(new \moodle_url('/local/edu/lib/index.php', array('id' => $item->id, 'action' => 'delete', 'libtab' => 1)), $deleteimg, array('class' => 'confirm-delete'));
            if (!self::$admin) $edit = '';
            $files = $fs->get_area_files(1, 'local_edu', 'libposter', $item->id);
            $url = '';
            foreach ($files as $file) {
                $name = $file->get_filename();
                if ($name == '.') continue;
                $url = \moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename());
                break;
            }
            $files = $fs->get_area_files(1, 'local_edu', 'libfile', $item->id);
            $down = '';
            foreach ($files as $file) {
                $name = $file->get_filename();
                if ($name == '.') continue;
                $down = \moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename());
                break;
            }
            $files = $fs->get_area_files(1, 'local_edu', 'presentation', $item->id);
            $presenturl = '';
            $presentname = '';
            foreach ($files as $file) {
                $presentname = $file->get_filename();
                if ($presentname == '.') continue;
                $presenturl = \moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename(), true);
                break;
            }

            $files = $DB->get_records_sql("select *  from {files} where contextid = 1 and component = 'local_edu'
                              and filearea = 'otherfiles' and itemid = $item->id and filename <> '.'");
            $otherfiles = [];
            if (!empty($files)) {
                foreach ($files as $file) {
                    $otherfiles[$file->filename] = \moodle_url::make_pluginfile_url(1, 'local_edu', 'otherfiles', $item->id, "/", $file->filename, true);
                }
            }


            $modal_html = '';
            if (empty($item->subtype)) {
                $modal_html = \html_writer::link($down, 'Скачать', ['class' => 'btn btn-success']);
            } else {
                $modal_content = get_config('local_edu', 'videocode');
                if ($item->subtype == 2)
                    $modal_content = get_config('local_edu', 'youtubecode');
                $modal_content = str_replace('<id>', $item->urlvideo, $modal_content);
                $modal = new modal($item->title, $modal_content, 'webinar' . $id);
                $modal->setWidth('1000px');
                $modal->setOk(false);
                $modal_html = $modal->linkTag('a', 'Посмотреть', ['class' => 'btn btn-success']);
                $modal_html .= "<br/>" . $modal->render();
            }
            $img = \html_writer::div(
                \html_writer::tag('p', \html_writer::tag("button", "Вернуться к списку", array("class" => "bookback btn-success", "id" => "lib-back btn-success"))) .
                //\html_writer::tag('p', \html_writer::span('Вернуться к списку', 'bookback')) .
                \html_writer::div(\html_writer::img($url, $item->title), 'img') . "<br/>" .
                $modal_html .
                self::printRating($item), 'col-sm-3 bookimage');


            $content = $edit;
            $fields = array(
                'id' => false,
                'title' => false,
                'author' => false,
                'description' => 'Описание вебинара',
                'duration' => 'Продолжительность вебинара',
                'sector' => "Отрасль",
                'groupid' => false,
                'dateview' => false,
                'tags' => false,
                'course' => "Курс",
                'video' => false,
                'urlvideo' => false,
                'poster' => false,
                'userid' => "Авторы/Ведущиие",
                'rating' => false,
                'dateinsert' => false,
                'presentation' => "Презентация",
                'otherfiles' => self::checkOtherFiles($otherfiles)

            );
            $itemmas = (array)$item;
            foreach ($itemmas as $k => $v) {
                $name = isset($fields[$k]) ? $fields[$k] : $k;
                if (empty($name)) continue;
                if (empty($v)) continue;
                $content .= \html_writer::tag('dt', $name);
                if ($k == 'presentation') $v = \html_writer::link($presenturl, $presentname);

                if ($k == 'otherfiles') {
                    $v = '';
                    if (!empty($otherfiles)) {
                        foreach ($otherfiles as $fname => $furl) {
                            $v .= \html_writer::link($furl, $fname) . '<br>';
                        }
                    }

                }
                if ($k == 'duration') {
                    $all = $v / 3600;
                    $hours = '0';
                    $min = '0';
                    $seconds = '0';
                    $hours .= (floor($v / 3600));
                    $ostatokHours = $all - (floor($v / 3600));
                    $minutes = floor($ostatokHours * 60);
                    $ostatokMin = ($ostatokHours * 60) - $minutes;
                    $seconds = floor($ostatokMin * 60);

                    if ($minutes > 0 && $minutes < 10) {
                        $m = '0' . $minutes;
                        $minutes = $m;
                    } elseif ($minutes == 0) {
                        $minutes = '00';
                    }
                    if ($seconds > 0 && $seconds < 10) {
                        $s = '0' . $seconds;
                        $seconds = $s;
                    } elseif ($seconds == 0) {
                        $seconds = '00';
                    }

                    $v = $hours . ':' . $minutes . ':' . $seconds;

                }
                if ($k == 'userid') {
                    $users = explode(',', $item->userid);
                    $text = array();
                    foreach ($users as $uid) {
                        $text[] = \html_writer::link('/user/profile.php?id=' . $uid, fullname(\core_user::get_user($uid)));
                    }
                    $content .= \html_writer::tag('dd', implode(', ', $text));
                } else
                    $content .= \html_writer::tag('dd', $v);
            }

            $content .= \html_writer::tag('dt', "Категории");
            $tags = [];
            $mas = $DB->get_field('edu_lib_webinary', 'tags', array('id' => $item->id));
            if ($mas = explode(',', $mas)) {
                foreach ($mas as $m)
                    if ($m) $tags[] = $m;
            }
            $html = "";
            foreach ($tags as $tid) {
                $html .= \html_writer::div(tag::get_fullpath_tagname($tid));
            }
            $content .= \html_writer::tag('dd', $html);

            $content = \html_writer::div(\html_writer::tag('dl', $content), 'col-sm-9');
            $content = \html_writer::div($img . $content, 'row');

            $year = date('Y', time());
            $date = strtotime($year . " -1 year");
            $lastyear = date('Y', $date);
            $copyright = \html_writer::div('© ' . $lastyear . '-' . $year . '. Электронная библиотека. Использование, воспроизведение и распространение данного объекта интеллектуальной собственности (видеоматериала) без согласия правообладателя преследуется по закону', 'span12 copyright-webinars');
            $groupid = [];
            $groupid = $DB->get_records_sql_menu("select id,groupid from {edu_lib_webinary_groups_items} where webid = $item->id");
            $ob->content = $content . \html_writer::div($copyright . \html_writer::div(self::getRecommendWebinar($item, $groupid), 'span12'), 'row-fluid');
            $ob->name = $item->title;
            return json_encode($ob);
        }
        return json_encode(false);
    }

    public static function getList($lib)
    {
        global $CFG, $OUTPUT, $DB;
        require_once($CFG->libdir . "/filelib.php");
        $fs = get_file_storage();
        $html = '';
        $editimg = \html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/settings'), 'title' => 'Редактировать', 'class' => ''));
        $deleteimg = \html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'title' => 'Удалить', 'class' => ''));
        foreach ($lib as $item) {
            $edit = " " . \html_writer::link(new \moodle_url('/', array('id' => $item->id, 'action' => 'edit', 'tab' => 1, 'uc' => self::$uc)), $editimg) .
                " " . \html_writer::link(new \moodle_url('/', array('id' => $item->id, 'action' => 'delete', 'tab' => 1, 'uc' => self::$uc)), $deleteimg, array('class' => 'confirm-delete'));
            if (!self::$admin) $edit = '';
            $files = $fs->get_area_files(1, 'local_edu', 'libposter', $item->id);
            $url = '';
            foreach ($files as $file) {
                $name = $file->get_filename();
                if ($name == '.') continue;
                $url = \moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename());
                break;
            }
            $files = $fs->get_area_files(1, 'local_edu', 'video', $item->id);
            $down = '';
            foreach ($files as $file) {
                $name = $file->get_filename();
                if ($name == '.') continue;
                $down = \moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename());
                break;
            }
            //
            $files = $fs->get_area_files(1, 'local_edu', 'presentation', $item->id);
            $presenturl = '';
            $presentname = '';
            foreach ($files as $file) {
                $presentname = $file->get_filename();
                if ($presentname == '.') continue;
                $presenturl = \moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename(), true);
                break;
            }
            $files = $fs->get_area_files(1, 'local_edu', 'otherfiles', $item->id);
            //
            $img = \html_writer::div(\html_writer::div(\html_writer::img($url, $item->title), 'img') .
                \html_writer::link($down, "Просмотреть", array('class' => 'btn btn-info down'))
                , 'span3 bookimage');
            $content = '';
            $fields = array(
                'id' => false,
                'title' => false,
                'author' => false,
                'description' => 'Описание вебинара',
                'duration' => 'Продолжительность вебинара',
                'sector' => "Отрасль",
                'groupid' => false,
                'dateview' => "Дата проведения вебинара",
                'tags' => false,
                'course' => "Курс",
                'video' => false,
                'poster' => false,
                'rating' => false,
                'userid' => "Автор/Ведущий",
                'dateinsert' => false,
                'presentation' => "Презентация",
                'otherfiles' => false
            );
            $itemmas = (array)$item;
            foreach ($itemmas as $k => $v) {
                $name = isset($fields[$k]) ? $fields[$k] : $k;
                if (empty($name)) continue;
                if (empty($v)) continue;
                $content .= \html_writer::tag('dt', $name);
                if ($k == 'presentation') $v = \html_writer::link($presenturl, $presentname);
                // $content .= \html_writer::tag('dd', $v);
                if ($k == 'userid')
                    $content .= \html_writer::tag('dd', \html_writer::link('/user/profile.php?id=' . $item->userid, $v));
                else
                    $content .= \html_writer::tag('dd', $v);
            }

            $content = \html_writer::div(\html_writer::tag('dl', $content), 'span9');


            $block = \html_writer::div(
                \html_writer::div($item->title, 'lib-block-name', array('data-itemid' => $item->id)),
                'lib-block lib-block-full', array('style' => "background-image:url($url);")
            );

            $html .= \html_writer::div($block .
                \html_writer::div(shorten_text($item->title, 65), 'lib-block-name', array('data-itemid' => $item->id)),
                'lib-block', array('style' => "background-image:url($url);")
            );
            //$html .= \html_writer::div(\EduSlider::render(\html_writer::span($item->title) . $edit, \html_writer::div($img . $content, 'row-fluid')), 'eduslider');

        }
        return $html;
    }

    public static function view()
    {
        return self::getList(self::getSearchData());
    }

    /**
     * Производит поиск книг по полям и тегам
     * @param Array $fields - Массив объектов (пар: название поля - значение)
     * @param Array $tags - Массив тегов
     * @return Array $data - массив найденных книг
     */
    public static function getSearchData(Array $fields = [], Array $tags = [], $start = 0, $step = 12)
    {
        global $DB, $CFG;
//        require_once($CFG->dirroot . '/local/edu/locallib.php');
        /*$sql = '';
        if (!empty($programs)) {
            $tagssql = array();
            $tagsnosql = array();
            foreach ($edu->programs as $p) {
                if ($p->tags != '') {
                    $mas = explode(',', $p->tags);
                    foreach ($mas as $m)
                        $Tag[] = $m;
                }
            }
            if (!empty($Tag)) {
                $sql = self::getSQLTag($fields, $tags, $Tag);

            } else {
                $sql = self::getSQL($fields, $tags);
            }

        } else {*/
        $sql = self::getSQL($fields, $tags);

        $data = $DB->get_records_sql($sql, array(), $start, $step);
        return $data;
    }

    static function getWhere($fields, $tags)
    {
        $where = ['1'];
        if ($fields) {
            foreach ($fields as $field) {
                if ($field->name == 'title_letter') {
                    $name = 't.title';
                    $where[] = " and LOWER($name) LIKE '$field->value%'";
                } elseif ($field->name == 'author') {
                    $where[] = " and u.id = t.userid and (LOWER(u.firstname) like '$field->value%' or LOWER(u.firstname) like '%$field->value%' or LOWER(u.firstname) like '%$field->value' 
                    or LOWER(u.lastname) like '$field->value%' or LOWER(u.lastname) like '%$field->value%' or LOWER(u.lastname) like '%$field->value')";
                } elseif ($field->name == 'dateview') {
                    $where[] = '';
                } elseif ($field->name == 'userid') {
                    if ($field->value != 0)
                        $where[] = " and t.userid = $field->value";
                    else $where[] = '';
                } else
                    $where[] = " and LOWER(t.$field->name) LIKE '%$field->value%'";
            }
        }

        if ($tags) {
            $where[] = " and ";
            $tagssql = array();
            foreach ($tags as $tag) {
                $tagssql[] = " t.tags LIKE '%,$tag,%'";
            }
            if ($tagssql) {
                $where[] = "(" . implode(" or ", $tagssql) . ")";

            }
        }
        return $where;
    }


    public static function getSQLTag(Array $fields, Array $tags, $Tag)
    {
        $sql = "select t.* from (select * from (SELECT * FROM {edu_lib_webinary} ";
        $sql .= " WHERE ";
        $whereno = ' ';
        $tagssql = array();
        $tagsnosql = array();
        foreach ($Tag as $t) {
            $tagssql[] = " tags LIKE '%,$t,%' ";
            $tagsnosql[] = " tags NOT LIKE '%,$t,%' ";
        }
        if ($tagssql)
            $sql .= ' ' . implode(" or ", $tagssql);

        if ($tagsnosql)
            $whereno .= ' ' . implode(" and ", $tagsnosql);
        $sql .= " order by title ";
        $sql .= " ) as a";
        $sql .= " union ";
        $sql .= " select * from (select * from {edu_lib_webinary} where $whereno order by title) as b";
        $sql .= " ) as t";
        if ($fields) {
            $sql .= self::addTableForSearchAuthor($fields);
        }


        return $sql . " where " . implode('  ', self::getWhere($fields, $tags));
    }

    public static function getSQL(Array $fields, Array $tags)
    {
        //$sql = "SELECT * FROM {edu_lib_webinary} as t where " . implode(' ' ,self::getWhere($fields, $tags)) . " order by t.title";
        $sql = "SELECT t.* FROM {edu_lib_webinary} as t";
        if ($fields) {
            $sql .= self::addTableForSearchAuthor($fields);
        }
        $sql .= "  where " . implode(' ', self::getWhere($fields, $tags));
        if ($fields) {
            foreach ($fields as $field) {
                if ($field->name == 'dateview') {
                    if ($field->value > 0) {
                        if ($field->value == 7) {
                            $week = time() - 3600 * 24 * 7;
                            $sql .= " and t.dateview > $week ";
                        } elseif ($field->value == 30) {
                            $month = time() - 3600 * 24 * 30;
                            $sql .= " and t.dateview > $month ";
                        }
                    }
                } else {

                }
            }
        }
        $sql .= " order by t.dateview DESC";
        //$sql .= "  where " . implode(' ', self::getWhere($fields, $tags)) . " order by t.dateview DESC";
        return $sql;
    }


    public static function addTableForSearchAuthor(Array $fields)
    {
        $sql = '';
        foreach ($fields as $field) {
            if ($field->name == 'author') {
                $sql = ", {user} as u ";
            }
        }
        return $sql;
    }


    public static function render($action = '', $id = 0)
    {
        global $DB;
        $tagid = optional_param("tagid", 0, PARAM_INT);
        if ($action == 'delete' && $id) return self::delete($id);
        if ($action == 'edit') return self::edit($id);
        $html = '';
        $html .= \html_writer::start_div("container-fluid row");

        $html .= \html_writer::start_div("col-sm-4 left");

        $search = \html_writer::tag("h4", "Поиск");


        $html .= $search;

        //$html .= \html_writer::tag("h4", "Поиск");

        $html .= \html_writer::start_div("search-fields");

        $html .= \html_writer::tag("input", "", array("class" => "type", "type" => "hidden", "value" => "webinary"));
        $html .= \html_writer::tag("input", "", array("class" => "type", "type" => "hidden", "value" => "webinary", "id" => "thmemeSearchID", "name" => 'webinars'));

        $html .= \html_writer::tag("label", "Название/Тема", array("for" => "lib-title"));
        $html .= \html_writer::tag("input", "", array("type" => "text", "id" => "lib-title", "name" => "title", "class" => "form-control"));


        //$html .= \html_writer::tag("label", "Автор/ведущий", array("for" => "lib-author"));
        //$teachers = [];//\Edu::getAllTeachers();
//        $mas = array();
//        $options = array('0' => 'Выберите автора/ведущего');
//        foreach ($teachers as $t) $mas[$t->id] = fullname($t);
//        $teachers = $mas;
//        $options = $options + $mas;
//        $html .= \html_writer::select($options, 'userid', '', array(), array("type" => "input", "id" => "lib-author", "name" => "userid"));


        /*$html .= \html_writer::tag("label", "Автор/ведущий", array("for" => "lib-author"));
        $html .= \html_writer::tag("input", "", array("type" => "text", "id" => "lib-author", "name" => "author"));*/

        /* $html .= \html_writer::tag("label", "Описание вебинара", array("for"=>"lib-description"));
         $html .= \html_writer::tag("textarea", "", array("type"=>"text", "id"=>"lib-description", "name"=>"description"));*/

        /*$html .= \html_writer::tag("label", "Отрасль", array("for" => "lib-sector"));
        $html .= \html_writer::tag("input", "", array("type" => "text", "id" => "lib-sector", "name" => "sector"));*/

        /* $html .= \html_writer::tag("label", "Курс", array("for" => "lib-course"));
         $html .= \html_writer::tag("input", "", array("type" => "text", "id" => "lib-course", "name" => "course"));*/

        //$html .= \html_writer::tag("label", "Выбрать период", array("for" => "lib-dateview"));
        //$period = array('0' => 'Любой', '7' => 'Неделя', '30' => 'Месяц');
        //$html .= \html_writer::select($period, 'dateview', '', array(), array("type" => "input", "id" => "lib-period", "name" => "dateview"));

        $html .= "<br/>";
        $html .= \html_writer::tag("button", "Найти", array("class" => "lib-find btn btn-primary"));
        $html .= "<br/>";
        $html .= "<br/>";

        $html .= \html_writer::tag("h5", "Категории");

        $html .= \edu\tag::getListForSearch($tagid);

        $html .= \html_writer::tag("button", "Очистить", array("class" => "lib-reset btn btn-default"));

        $html .= \html_writer::end_div();

        $html .= \html_writer::end_div();

        $html .= \html_writer::start_div("col-sm-8 right");

        if ($id) {
            $web = \edu\webinary::getContent($id);
            $web = json_decode($web);
            $html .= \html_writer::tag("h4", $web->name);
            $html .= \html_writer::div($web->content, 'bookcontent') . \html_writer::div(\edu\webinary::view(), 'booklist', ["style" => "display: none"]);
            $html .= \html_writer::tag("button", "Показать еще 12", array("class" => "lib-showmore btn-success", "style" => "display:none;"));
        } else {
            $html .= \html_writer::tag("h4", "Список вебинаров");
            $html .= \html_writer::div("", 'bookcontent') . \html_writer::div(\edu\webinary::view(), 'booklist');
            if ($DB->count_records('edu_lib_webinary') > 12)
                $html .= \html_writer::tag("button", "Показать еще 12", array("class" => "lib-showmore btn-success"));
        }

        //$html .= \html_writer::tag("button", "Показать еще 12", array("class" => "lib-showmore btn-success"));

        $html .= \html_writer::img("/local/edu/lib/img/loading.gif", "Идет загрузка...", array("class" => "lib-loading-webinary"));

        $html .= \html_writer::end_div();

        $html .= \html_writer::end_div();
        return $html;
    }

    public function checkOtherFiles($mas = array())
    {
        $text = "Файлы";
        if (!empty($mas)) return $text;
        return false;
    }


}


class webinary_form extends \moodleform
{

    private $id = 0;

    function __construct($url, $id)
    {
        $this->id = $id;
        parent::moodleform($url);
    }

    public function definition()
    {
        global $DB, $PAGE;
        $mform = $this->_form; // Don't forget the underscore!
        $subtype = optional_param('subtype', 0, PARAM_INT);

        if ($this->id) {
            $web = $DB->get_record('edu_lib_webinary', ['id' => $this->id]);
            $subtype = $web->subtype;
        }

        $mform->addElement('header', 'serach', "Новый ресурс");

        $id = optional_param("id", 0, PARAM_INT);


        $mform->addElement('hidden', 'id');
        $mform->setDefault('id', $id);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'subtype');
        $mform->setDefault('subtype', $subtype);
        $mform->setType('subtype', PARAM_INT);

        $mform->addElement('hidden', 'type');
        $mform->setDefault('type', "webinary");
        $mform->setType('type', PARAM_TEXT);

        $mform->addElement('text', 'title', 'Название/Тема');
        $mform->setType('title', PARAM_TEXT);
        $mform->addRule('title', null, 'required', null, 'client');

        $teachers = [];//\Edu::getAllTeachers();
        $mas = array();

        foreach ($teachers as $t) $mas[$t->id] = fullname($t);
        $tags = array();
        if ($this->id) {
            $mas = $DB->get_field('edu_lib_webinary', 'tags', array('id' => $this->id));
            if ($mas = explode(',', $mas)) {
                foreach ($mas as $m)
                    if ($m) $tags[] = $m;
            }
        }

        $PAGE->requires->js_call_amd("local_edu/edu-lib", "startJSTreeLib", [tag::getCategoriesASObject(0, $tags)]);
        $mform->addElement('html', \html_writer::tag('h4', 'Категории') . \html_writer::div('', 'jstree-container-edit') . \html_writer::div('Выбрано категорий: ' . \html_writer::span(count($this->tags)), 'jstree-container-info') .
            \html_writer::empty_tag('input', array('type' => 'hidden', 'value' => '', 'name' => 'tags')));


        $filemanager_options = array();
        $filemanager_options['accepted_types'] = array('.jpg', '.png', 'jpeg');
        $filemanager_options['maxbytes'] = 20500240;
        $filemanager_options['maxfiles'] = 1;
        $filemanager_options['mainfile'] = false;

        $mform->addElement('filepicker', 'poster', 'Обложка', null, $filemanager_options);
        $mform->addRule('poster', null, 'required', null, 'client');

        $filemanager_options = array();
        $filemanager_options['accepted_types'] = array('.*');
        $filemanager_options['maxbytes'] = 200500240;
        $filemanager_options['maxfiles'] = 1;
        $filemanager_options['mainfile'] = false;

        if (empty($subtype)) {
            $mform->addElement('filepicker', 'file', 'Ресурс', null, $filemanager_options);
            $mform->addRule('file', null, 'required', null, 'client');
        } else {
            $mform->addElement('text', 'urlvideo', "ID видеоматериала");
            $mform->setType('urlvideo', PARAM_URL);
            $mform->addRule('urlvideo', null, 'required', null, 'client');
        }

        $this->add_action_buttons(false, "Сохранить");
    }

    function validation($data, $files)
    {
        global $DB;
        $error = parent::validation($data, $files);
        return $error;
    }
}

?>