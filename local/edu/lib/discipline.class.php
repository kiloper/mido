<?php

namespace edu;

global $CFG;
require_once("$CFG->libdir/formslib.php");

class discipline{

    private static $LIB_DISCIPLINE = "edu_lib_discipline";
    public static $admin = true;
    public static $uc = 0;
    
    public static function edit($id = 0)
    {
        global $DB;
        $context = \context_system::instance();
        $form = new discipline_form(new \moodle_url('/?action=edit&tab=2&uc='.self::$uc), $id);
        if ($id) {
            $data = $DB->get_record(self::$LIB_DISCIPLINE, array("id" => $id));
            //для зарузки файлов
            $fileoptions = array(
                'subdirs' => 1,
                'maxbytes' => 0,
                'maxfiles' => -1,
                'changeformat' => 0,
                'context' => $context,
                'noclean' => 1,
                'trusttext' => 0
            );
            $draftitemid = file_get_submitted_draft_itemid('card');
            file_prepare_draft_area($draftitemid, $context->id, 'local_edu', 'card', $id, $fileoptions);
            $data->card = $draftitemid;
            //для зарузки файлов
            $draftitemid = file_get_submitted_draft_itemid('contentdiscip');
            file_prepare_draft_area($draftitemid, $context->id, 'local_edu', 'contentdiscip', $id, $fileoptions);
            $data->contentdiscip = $draftitemid;

            $form->set_data($data);
        }

        if ($data = $form->get_data()) {
            $tags = optional_param('tags', '', PARAM_TEXT);
            $data->tags = '';
            if ($tags) $data->tags = ",$tags,";

            if ($data->id == 0) {

                //Добавляем новую запись
                $data->dateinsert = time();
                if ($libid = $DB->insert_record(self::$LIB_DISCIPLINE, $data)) {
                    //для зарузки файлов
                    if ($data->card) {
                        file_save_draft_area_files($data->card, $context->id, 'local_edu', 'card', $libid, array('subdirs' => false));
                    }
                    if ($data->contentdiscip) {
                        file_save_draft_area_files($data->contentdiscip, $context->id, 'local_edu', 'contentdiscip', $libid, array('subdirs' => false));
                    }
                    redirect(new \moodle_url('/?tab=2&uc='.self::$uc));
                }
            } else {
                $data->dateinsert = time();
                // Обновляем запись
                $DB->update_record(self::$LIB_DISCIPLINE, $data);
                //для зарузки файлов
                if ($data->card) {
                    file_save_draft_area_files($data->card, $context->id, 'local_edu', 'card', $data->id, array('subdirs' => false));
                }
                if ($data->contentdiscip) {
                    file_save_draft_area_files($data->contentdiscip, $context->id, 'local_edu', 'contentdiscip', $data->id, array('subdirs' => false));
                }
                redirect(new \moodle_url('/?tab=2&uc='.self::$uc));
            }
        }
        return $form->render();
    }

//$button = html_writer::link(new moodle_url('/course/view.php', array('id' => $c->id, 'pid' => $p->id)), "Войти", array('class' => 'btn-success'));
    public static function getList($lib){
        global $OUTPUT, $DB;

        $html = '';
        $fs = get_file_storage();
        foreach ($lib as $item) {

            $coursename = $DB->get_field("course", "fullname", array("id"=>$item->courseid));
            $courseid = $item->courseid;
            $item->courseid = $coursename;

            $editimg = \html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/settings'), 'title' => 'Редактировать', 'class' => ''));
            $deleteimg = \html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'title' => 'Удалить', 'class' => ''));
            $edit = " " . \html_writer::link(new \moodle_url('/', array('id' => $item->id, 'action' => 'edit', 'tab' => 2, 'uc' => self::$uc)), $editimg).' '.
                \html_writer::link(new \moodle_url('/', array('id' => $item->id, 'action' => 'delete', 'tab' => 2, 'uc' => self::$uc)), $deleteimg, array('class' => 'confirm-delete'));;
            if(!self::$admin) $edit = '';

            $content = '';
            $fields = array(
                'id' => false,
                'title' => "Название",
                'tags' => false,
                'courseid' => "Курс",
                'card' => "Карточка учета",
                'contentdiscip' => "Содержание",
                'dateinsert'=> "Дата загрузки документа",
                'author' => "Автор",
                'years' => "Год"
            );
            $itemmas = (array)$item;

            $files = $fs->get_area_files(1, 'local_edu', 'card', $item->id);
            $cardurl = '';
            $cardname = '';
            foreach ($files as $file) {
                $cardname = $file->get_filename();
                if ($cardname == '.') continue;
                $cardurl = \moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename(), true);
                break;
            }
            $files = $fs->get_area_files(1, 'local_edu', 'contentdiscip', $item->id);
            $card1url = '';
            $card1name = '';
            foreach ($files as $file) {
                $card1name = $file->get_filename();
                if ($card1name == '.') continue;
                $card1url = \moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename(), true);
                break;
            }

            foreach ($itemmas as $k => $v) {
                $name = isset($fields[$k]) ? $fields[$k] : $k;
                if (empty($name)) continue;
                if (empty($v)) continue;
                $content .= \html_writer::tag('dt', $name);
                if($k == 'card') $v = \html_writer::link($cardurl, $cardname);
                if($k == 'contentdiscip') $v = \html_writer::link($card1url, $card1name);
                if($k == 'dateinsert') $v = $v ? date('d.m.Y',$v) : '';
                $content .= \html_writer::tag('dd', $v);
            }
            $content .= \html_writer::empty_tag('br');
            if($courseid != 0) $content .= \html_writer::link(new \moodle_url('/course/view.php', array('id' => $courseid)), "Войти", array('class' => 'btn-success'));
            $content = \html_writer::div(\html_writer::tag('dl', $content), 'span9');


            $html .= \html_writer::div(\EduSlider::render(\html_writer::span($item->title) . $edit, \html_writer::div($content, 'row-fluid')), 'eduslider');

        }
        return $html;
    }

    public static function view()
    {
        global $DB;
        $lib = $DB->get_records(self::$LIB_DISCIPLINE, null, 'title', '*', 0, 20);
        return self::getList($lib);
    }

    public static function getSearchData(Array $fields, Array $tags, $start = 0, $step = 20,$date1 = 0,$date2 = 0){
        global $DB;
        $sql = "SELECT * FROM {edu_lib_discipline}";
        if ($fields || $tags || $date1) {
            $sql .= " WHERE";
        }

        if ($date1 && $date2) {
            $sql .= " card is not null and dateinsert >=$date1 and
           dateinsert < $date2 ";
        }

        if ($fields) {
            $liker = '';
            if ($date1 && $date2) $liker = " and ";
            $fieldssql = array();
            foreach ($fields as $field){
                if ($field->name == 'title_letter_books') {
                    $name = 'title';
                    $fieldssql[] = " $liker LOWER($name) LIKE '$field->value%' ";
                }
                else {
                    if ($date1 && $date2) {
                        $fieldssql[] = " LOWER($field->name) LIKE '%$field->value%'";
                    }
                    else {
                        $fieldssql[] = " $liker LOWER($field->name) LIKE '%$field->value%'";
                    }

                }

            }
            if ($fieldssql){
                $sql.= ' '.implode(" and ", $fieldssql);
            }
        }

        if ($fields && $tags){
            $sql .= " AND ";
        }

        if ($tags) {
            $tagssql = array();
            if($date1 && $date2 || $tags) {
                $sql .= " ( ";
            }

            foreach ($tags as $tag) {
                $tagssql[] = "tags LIKE '%,$tag,%'";
            }
            if($tagssql)
            {
                if($date1 && $date2 || $tags) {
                    $sql.= ' '.implode(" or ", $tagssql) . ' )';
                }
                else {
                    $sql.= ' '.implode(" or ", $tagssql);
                }

            }

        }
//var_dump($sql);die;
        $sql .= " order by title";

        $data = $DB->get_records_sql($sql, array(), $start, $step);
        return $data;
    }
    static function delete($id)
    {
        global $DB;
        $DB->delete_records('edu_lib_discipline', array("id" => $id));
        $fs = get_file_storage();
        $fs->delete_area_files(1, 'local_edu', 'contentdiscip', $id);
        $fs->delete_area_files(1, 'local_edu', 'card', $id);
        redirect(new \moodle_url('/?tab=2&uc='.self::$uc));
    }

    public static function render($action = '', $id = 0){
        global $OUTPUT;
        if ($action == 'delete' && $id) return self::delete($id);
        if ($action == 'edit' && self::$admin) return self::edit($id);
        $html = '';
        $html .= \html_writer::start_div("container-fluid");

        $html .= \html_writer::start_div("span4");
        //$html .= \html_writer::tag("h4", "Поиск");
        $search = \html_writer::div(\html_writer::tag("h4", "Поиск"), 'div-td');

        $search .= \html_writer::div(\html_writer::div(\html_writer::div('Алфавитный указатель', 'help') . \html_writer::span('Все') . \html_writer::tag("input", "", array("type" => "text", "maxlength" => 1, "name" => "title_letter_books","id" => "title_letter_books", 'style' => 'display: none;')), 'search-fields tooltip', ['id' => 'alphabet', 'title' => 'Нажмите для ввода буквы', 'data-placement' => 'bottom']), 'div-td div-td-2');

        $html .= \html_writer::div($search, 'div-table');


        $html .= \html_writer::start_div("search-fields");

        $html .= \html_writer::tag("input", "", array("class" => "type", "type" => "hidden", "value" => "discipline"));

        $html .= \html_writer::tag("label", "Название", array("for" => "lib-title"));
        $html .= \html_writer::tag("input", "", array("type" => "text", "id" => "lib-title", "name" => "title"));
//
        $html .= \html_writer::tag('br','');
        $html .= \html_writer::tag("label", "От:", array("for" => "lib-date1"));
        $date1 = date('Y'.'-'.'m'.'-'.'d',time() - 24*3600*7);
        $html .= \html_writer::tag("input", "", array("type" => "date", "id" => "lib-date1", "name" => "date1","value"=>$date1));

        $html .= \html_writer::tag("label", "До:", array("for" => "lib-date2"));
        $date2 = date('Y'.'-'.'m'.'-'.'d',time());
        $html .= \html_writer::tag("input", "", array("type" => "date", "id" => "lib-date2", "name" => "date2","value"=>$date2));
        $html .= \html_writer::tag('br','');
        $options = array('type' => 'checkbox','class'=>'lib-discipline-check');
        $html .= \html_writer::empty_tag('input', $options) . 'учитывать даты';
        //$html .= \html_writer::checkbox('check',0,false,"учитывать даты",array('class'=>'lib-discipline-check'));
        $html .= \html_writer::tag('br','');
        //
        $html .= \html_writer::tag("button", "Найти", array("class" => "lib-find btn-success"));

        $html .= \html_writer::tag("h5", "Категории");

        $html .= \edu\tag::getListForSearch();

        $html .= \html_writer::tag("button", "Очистить", array("class" => "lib-reset"));
        if(self::$admin)
            $html .= \html_writer::tag("p", \html_writer::link('#', \html_writer::img(("/local/edu/lib/img/excel.png"),'excel').' '.\html_writer::span("Скачать в Excel"), array('class' => 'excel-library')));

        $html .= \html_writer::end_div();

        $html .= \html_writer::end_div();

        $html .= \html_writer::start_div("span8");

        $html .= \html_writer::tag("h4", "Список дисциплин");

        $html .= \html_writer::div(\edu\discipline::view(), 'booklist');
        $html .= \html_writer::img("/local/edu/lib/img/loading.gif", "Идет загрузка...", array("class" => "lib-loading"));

        $html .= \html_writer::end_div();

        $html .= \html_writer::end_div();
        return $html;
    }

}

class discipline_form extends \moodleform{

    private $id = 0;

    function __construct($url, $id)
    {
        $this->id = $id;
        parent::moodleform($url);
    }

    public function definition() {
        global $DB, $PAGE;
        $mform = $this->_form; // Don't forget the underscore!

        $mform->addElement('header', 'serach', "Новая дисциплина");

        $id = optional_param("id", 0, PARAM_INT);
        $mform->addElement('hidden', 'id');
        $mform->setDefault('id', $id);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'type');
        $mform->setDefault('type', "discipline");
        $mform->setType('type', PARAM_TEXT);

        $mform->addElement('text', 'title', 'Название дисциплины');
        $mform->setType('title', PARAM_TEXT);
        $mform->addRule('title', null, 'required', null, 'client');

        //Доп поля автор и год по задаче
        $mform->addElement('text','author','Автор');
        $mform->setType('author', PARAM_TEXT);
        $mform->addElement('text','years','Год');
        $mform->setType('years', PARAM_TEXT);

        $courses = $DB->get_records_sql("select c.id, c.fullname, cat.name from {course} as c, {course_categories} as cat where c.category = cat.id order by fullname");
        $select = array();
        foreach($courses as $course){
            $cname = trim($course->fullname);
            if(empty($cname)) continue;
            if(!isset($select[$course->name]))
            {
                $select[$course->name] = array();
            }
            $select[$course->name][$course->id] = $course->fullname;
        }
        $mform->addElement('selectgroups', 'courseid', 'Курс', $select, array('class' => 'singleSelect'));
        //$mform->getElement('courseid')->setMultiple(false);
        //$mform->addElement('select', 'courseid', 'Курс', $select);

        $tags = array();
        if ($this->id) {
            $mas = $DB->get_field('edu_lib_discipline', 'tags', array('id' => $this->id));
            if($mas = explode(',', $mas))
            {
                foreach ($mas as $m)
                    if($m) $tags[] = $m;
            }
        }

        //$PAGE->requires->js_init_call('startJSTreeLib', array('data' => tag::getCategoriesASObject(0, $tags)));
        $PAGE->requires->js_call_amd("local_edu/edu-lib", "startJSTreeLib", [tag::getCategoriesASObject(0, $tags)]);
        $mform->addElement('html', \html_writer::tag('h4', 'Категории') . \html_writer::div('', 'jstree-container-edit') . \html_writer::div('Выбрано категорий: ' . \html_writer::span(count($this->tags)), 'jstree-container-info') .
            \html_writer::empty_tag('input', array('type' => 'hidden', 'value' => '', 'name' => 'tags')));
        //Добавление двух доп полей
        $filemanager_options = array();
        $filemanager_options['accepted_types'] = array('.jpg', '.png', '.jpeg','.doc','.docx', '.pdf');
       $filemanager_options['maxbytes'] = 2500000;
        $filemanager_options['maxfiles'] = 1;
        $filemanager_options['mainfile'] = false;

        $mform->addElement('filemanager', 'card', 'Карточка учета', null, $filemanager_options);
        //$mform->addRule('card', null, 'required', null, 'client');

        $filemanager_options = array();
        $filemanager_options['accepted_types'] = array('.jpg', '.png', '.jpeg', '.doc','.docx', '.pdf');
        $filemanager_options['maxbytes'] = 2500000;
        $filemanager_options['maxfiles'] = 1;
        $filemanager_options['mainfile'] = false;

        $mform->addElement('filemanager', 'contentdiscip', 'Содержание', null, $filemanager_options);
        //$mform->addRule('contentdiscip', null, 'required', null, 'client');

        $this->add_action_buttons(false, "Сохранить");
    }

    function validation($data, $files) {
        global $DB;
        $error = parent::validation($data, $files);
        return $error;
    }
}