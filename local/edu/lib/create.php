<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
require_once('../../../config.php');
require_once($CFG->dirroot . '/lib/formslib.php');
require_once('lib.class.php');
require_once('tag.class.php');
require_once('webinary.class.php');


$PAGE->set_title('Библиотека');
$PAGE->set_heading();

//$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/edu/lib/js/edu-lib.js'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/local/edu/lib/css/edu-lib.css'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/mgaps/javascript/init-ui.js'));
echo $OUTPUT->header();
echo $OUTPUT->heading('Новая книга');


$id = optional_param("id", 0, PARAM_INT);
$type = optional_param("type", "", PARAM_TEXT);

echo html_writer::start_div('library');

switch($type){
    case "book":
        \edu\lib::edit($id);
        break;

    case "tag":
        \edu\tag::edit($id);
        break;

    case "webinary":
        \edu\webinary::edit($id);
        break;
    default:
        break;
}

echo html_writer::end_div();

echo $OUTPUT->footer();
