<?php

class ActionButton{

    private $title;
    private $data = array();
    private $class;

    function ActionButton($title, $data, $class='success'){
        $this->title = $title;
        $this->data = $data;
        $this->class = $class;
    }


    function render(){
        $html = '';
        $html .= \html_writer::start_div("btn-group edu-lib");{
            $html .= \html_writer::tag(
                "button",
                $this->title . " " . \html_writer::span("", "caret"),
                array(
                    "class"=>"btn btn-" . $this->class . " dropdown-toggle",
                    "data-toggle"=>"dropdown")
            );
            $html .= \html_writer::start_tag("ul", array("class"=>"dropdown-menu"));{
                foreach($this->data as $link=>$name){
                    $html .= \html_writer::tag("li", \html_writer::link($link, $name));
                }
            }$html .= \html_writer::end_tag("ul");

        }$html .= \html_writer::end_div();
        return $html;
    }

}

class EduButton
{

    private $title;
    private $data = array();
    private $class;

    function __construct($title, $data, $class = 'success')
    {
        $this->title = $title;
        $this->data = $data;
        $this->class = $class;
    }


    function render()
    {
        $html = '';
        $html .= \html_writer::start_div("btn-group edu-lib");
        {
            $html .= \html_writer::tag(
                "button",
                $this->title . " " . \html_writer::span("", "caret"),
                array(
                    "class" => "btn btn-" . $this->class . " dropdown-toggle",
                    "data-toggle" => "dropdown")
            );
            $html .= \html_writer::start_tag("ul", array("class" => "dropdown-menu"));
            {
                foreach ($this->data as $link => $name) {
                    $html .= \html_writer::tag("li", \html_writer::link(str_replace("&amp;", "&", $link), $name, ['class' => 'dropdown-item']));
                }
            }
            $html .= \html_writer::end_tag("ul");

        }
        $html .= \html_writer::end_div();
        return $html;
    }

}



?>
