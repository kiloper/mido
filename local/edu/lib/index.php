<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
require_once('../../../config.php');
require_once($CFG->dirroot . '/lib/formslib.php');
require_once($CFG->dirroot . '/local/edu/helpers.php');
require_once('lib.class.php');
require_once('tag.class.php');
require_once('webinary.class.php');
require_once('discipline.class.php');
require_once('classes/ActionButton.class.php');
require_once($CFG->libdir . '/adminlib.php');
//$CFG->debug = 32767;$CFG->debugdisplay = 1;

admin_externalpage_setup('library');

$tabid = optional_param('libtab', 0, PARAM_INT);
$PAGE->set_title('Библиотека');
$PAGE->set_heading();

echo $OUTPUT->header();
echo $OUTPUT->heading('Библиотека');
echo \local_edu\library::render();
echo $OUTPUT->footer();
