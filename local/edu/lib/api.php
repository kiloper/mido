<?php
require_once('../../../config.php');

global $DB,$CFG;
require_once($CFG->libdir . "/filelib.php");
$fs = get_file_storage();
$action = required_param('action', PARAM_CLEAN);
if (optional_param('json', 0, PARAM_INT)) {
    header('Content-Type: application/json');
}
if ($action == 'getWebinaryBooks') {
    $web = array();
    if ($web = $DB->get_records_sql("select * from {edu_lib_webinary}")) {
        foreach ($web as $w)
        {
            $files = $fs->get_area_files(1, 'local_edu', 'poster', $w->id);
            $url = '';
            foreach ($files as $file) {
                $name = $file->get_filename();
                if ($name == '.') continue;
                $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename());
                break;
            }
            $files = $fs->get_area_files(1, 'local_edu', 'video', $w->id);
            $down = '';
            foreach ($files as $file) {
                $name = $file->get_filename();
                if ($name == '.') continue;
                $down = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename());
                break;
            }
            $w->poster = (string) $url;
            $w->video = (string) $down;

        }
    }
    echo json_encode($web);
    die;
}

if($action == 'getCategory') {
    function getLibraryTree($parentid = 0)
    {
        global $DB;
        $cat = array();
        if ($tags = $DB->get_records_sql("select * from {edu_lib_tags} where parent = $parentid order by tag")) {
            foreach ($tags as $t) {
                $ob = new stdClass();
                $ob->id = $t->id;
                $ob->name = $t->tag;
                $ob->childrens = getLibraryTree($t->id);
                $cat[] = $ob;
            }
        }
        return $cat;
    }

echo json_encode(getLibraryTree());
}

?>