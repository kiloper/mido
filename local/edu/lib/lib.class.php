<?php

namespace edu;

global $CFG;
require_once("$CFG->libdir/formslib.php");

/**
 * @class lib
 * Класс для работы электронной биби=лиотекой
 * */
class lib
{
    private static $LIB = "edu_lib";              ///< Таблица с уведомлениями
    private static $LIB_TAGS = "edu_lib_tags";    ///< Таблица с прочитанными уведомлениями
    public static $admin = true;
    public static $uc = 0;

    /**
     * Создание и редактирование записи о книге. Выводит форму для работы с книгой
     * @param int $id - ID книги, если 0 то создание новой книги
     */
    public static function edit($id = 0)
    {
        global $DB;
        $context = \context_system::instance();
        $form = new lib_form(new \moodle_url('/?action=edit&tab=0&uc='.self::$uc), $id);
        if ($id) {
            $data = $DB->get_record(self::$LIB, array("id" => $id));
            // Get TAGS

            $fileoptions = array(
                'subdirs' => 1,
                'maxbytes' => 0,
                'maxfiles' => -1,
                'changeformat' => 0,
                'context' => $context,
                'noclean' => 1,
                'trusttext' => 0
            );
            $draftitemid = file_get_submitted_draft_itemid('book_image');
            file_prepare_draft_area($draftitemid, $context->id, 'local_edu', 'bookimage', $id, $fileoptions);
            $data->book_image = $draftitemid;

            $draftitemid = file_get_submitted_draft_itemid('book_file');
            file_prepare_draft_area($draftitemid, $context->id, 'local_edu', 'bookfile', $id, $fileoptions);
            $data->book_file = $draftitemid;
            $form->set_data($data);
        }

        if ($data = $form->get_data()) {
            $tags = optional_param('tags', '', PARAM_TEXT);
            $data->tags = '';
            if ($tags) $data->tags = ",$tags,";

            if ($data->id == 0) {
                //Добавляем новую запись
                if ($libid = $DB->insert_record(self::$LIB, $data)) {
                    if ($data->book_image) {
                        file_save_draft_area_files($data->book_image, $context->id, 'local_edu', 'bookimage', $libid, array('subdirs' => false));
                    }
                    if ($data->book_file) {
                        file_save_draft_area_files($data->book_file, $context->id, 'local_edu', 'bookfile', $libid, array('subdirs' => false));
                    }
                    redirect(new \moodle_url('/?tab=0&uc='.self::$uc));
                }
            } else {
                // Обновляем запись
                $DB->update_record(self::$LIB, $data);
                if ($data->book_image) {
                    file_save_draft_area_files($data->book_image, $context->id, 'local_edu', 'bookimage', $data->id, array('subdirs' => false));
                }
                if ($data->book_file) {
                    file_save_draft_area_files($data->book_file, $context->id, 'local_edu', 'bookfile', $data->id, array('subdirs' => false));
                }
                redirect(new \moodle_url('/?tab=0&uc='.self::$uc));
            }
        }
        return $form->render();
    }


    /**
     * Вывод книг в виже списка
     * @return String $html - HTML список книг
     */
    public static function getList($lib)
    {
        global $CFG, $OUTPUT;
        require_once($CFG->libdir . "/filelib.php");
        $fs = get_file_storage();
        $html = '';

        foreach ($lib as $item) {
            $editimg = \html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/settings'), 'title' => 'Редактировать', 'class' => ''));
            $edit = " " . \html_writer::link(new \moodle_url('/', array('id' => $item->id, 'action' => 'edit', 'uc' => self::$uc)), $editimg);
            if (!self::$admin) $edit = '';

            $files = $fs->get_area_files(1, 'local_edu', 'bookimage', $item->id);
            $url = '';
            foreach ($files as $file) {
                $name = $file->get_filename();
                if ($name == '.') continue;
                $url = \moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename());
                break;
            }
            $files = $fs->get_area_files(1, 'local_edu', 'bookfile', $item->id);
            $down = '';
            foreach ($files as $file) {
                $name = $file->get_filename();
                if ($name == '.') continue;
                $down = \moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename());
                break;
            }
            $img = \html_writer::div(\html_writer::div(\html_writer::img($url, $item->title), 'img') .
                \html_writer::link($down, "Скачать", array('class' => 'btn btn-info down'))
                , 'span3 bookimage');
            $content = '';
            $fields = array(
                'id' => false,
                'author_collective' => false,
                'title' => false,
                'operator' => false,
                'download' => false,
                'book_image' => false,
                'book_file' => false,
                'class_number' => false,
                'price' => false,
                'tags' => false,
                'city' => false,
                'pages' => "Количество страниц:",
                'illustrations' => false,
                'series' => "Серия:",
                'publish_area' => false,
                'publish_type' => false,
                'publish_year' => "Год публикации:",
                'annotation' => "Аннотация:",
                'publish_house' => "Издательский дом:",
                'isbn' => "Номер ISBN:",
                'authors' => "Автор:"
            );
            $itemmas = (array)$item;
            foreach ($itemmas as $k => $v) {
                $name = isset($fields[$k]) ? $fields[$k] : $k;
                if (empty($name)) continue;
                if (empty($v)) continue;
                $content .= \html_writer::tag('dt', $name);
                $content .= \html_writer::tag('dd', $v);
            }
            $content = \html_writer::div(\html_writer::tag('dl', $content), 'span9');

            $html .= \html_writer::div(\EduSlider::render(\html_writer::span($item->title) . $edit, \html_writer::div($img . $content, 'row-fluid')), 'eduslider');

        }
        return $html;
    }

    public static function view()
    {
        // Вывод книг в виде списка
        global $DB, $OUTPUT;
        $lib = $DB->get_records(self::$LIB, null, 'title');
        return self::getList($lib);
    }

    /**
     * Производит поиск книг по полям и тегам
     * @param Array $fields - Массив объектов (пар: название поля - значение)
     * @param Array $tags - Массив тегов
     * @return Array $data - массив найденных книг
     */
    public static function getSearchData(Array $fields, Array $tags)
    {
        global $DB;
        $sql = "SELECT * FROM {edu_lib}";
        if ($fields || $tags) {
            $sql .= " WHERE";
        }
        if ($fields) {
            $fieldssql = array();
            foreach ($fields as $field) {
                $fieldssql[] = "LOWER($field->name) LIKE '%$field->value%'";
            }
            if ($fieldssql) {
                $sql .= ' ' . implode(" and ", $fieldssql);
            }
        }

        if ($fields && $tags) {
            $sql .= " AND ";
        }

        if ($tags) {
            $tagssql = array();
            foreach ($tags as $tag) {
                $tagssql[] = "tags LIKE '%,$tag,%'";
            }
            if ($tagssql)
                $sql .= ' (' . implode(" or ", $tagssql) . ")";

        }
        $sql .= " order by title";
        $data = $DB->get_records_sql($sql);
        return $data;
    }


    public static function render($action = '', $id = 0)
    {
        if ($action == 'edit' && self::$admin) return self::edit($id);
        $html = '';
        $html .= \html_writer::start_div("container-fluid");

        $html .= \html_writer::start_div("span4");
        $html .= \html_writer::tag("h4", "Поиск");

        $html .= \html_writer::start_div("search-fields");

        $html .= \html_writer::tag("input", "", array("class" => "type", "type" => "hidden", "value" => "book"));

        $html .= \html_writer::tag("label", "Авторы", array("for" => "lib-authors"));
        $html .= \html_writer::tag("input", "", array("type" => "text", "id" => "lib-authors", "name" => "authors"));

        $html .= \html_writer::tag("label", "Заглавие", array("for" => "lib-title"));
        $html .= \html_writer::tag("input", "", array("type" => "text", "id" => "lib-title", "name" => "title"));

        $html .= \html_writer::tag("label", "Вид издания", array("for" => "lib-publish-type"));
        $html .= \html_writer::tag("input", "", array("type" => "text", "id" => "lib-publish-type", "name" => "publish_type"));

        $html .= \html_writer::tag("label", "Место", array("for" => "lib-city"));
        $html .= \html_writer::tag("input", "", array("type" => "text", "id" => "lib-city", "name" => "city"));

        $html .= \html_writer::tag("label", "Издательство", array("for" => "lib-publish-house"));
        $html .= \html_writer::tag("input", "", array("type" => "text", "id" => "lib-publish-house", "name" => "publish_house"));

        $html .= \html_writer::tag("button", "Найти", array("class" => "lib-find btn-success"));

        $html .= \html_writer::tag("h5", "Категории");

        $html .= \edu\tag::getListForSearch();

        $html .= \html_writer::tag("button", "Очистить", array("class" => "lib-reset"));

        $html .= \html_writer::end_div();

        $html .= \html_writer::end_div();

        $html .= \html_writer::start_div("span8");

        $html .= \html_writer::tag("h4", "Список книг");

        $html .= \html_writer::div(\edu\lib::view(), 'booklist');
        $html .= \html_writer::img("/local/edu/lib/img/loading.gif", "Идет загрузка...", array("class" => "lib-loading"));

        $html .= \html_writer::end_div();

        $html .= \html_writer::end_div();
        return $html;
    }

}


class lib_form extends \moodleform
{

    private $id = 0;

    function __construct($url, $id)
    {
        $this->id = $id;
        parent::moodleform($url);
    }


    public function definition()
    {
        global $PAGE, $DB;
        $mform = $this->_form; // Don't forget the underscore!

        $mform->addElement('header', 'serach', "Новая книга");

        $id = optional_param("id", 0, PARAM_INT);
        $mform->addElement('hidden', 'id');
        $mform->setDefault('id', $id);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'type');
        $mform->setDefault('type', "book");
        $mform->setType('type', PARAM_TEXT);

        $mform->addElement('text', 'authors', 'Авторы');
        $mform->setType('authors', PARAM_TEXT);

        $mform->addElement('text', 'author_collective', 'Авторский коллектив');
        $mform->setType('author_collective', PARAM_TEXT);

        $mform->addElement('text', 'title', 'Заглавие');
        $mform->setType('title', PARAM_TEXT);

        $mform->addElement('text', 'publish_type', 'Вид издания');
        $mform->setType('publish_type', PARAM_TEXT);

        $mform->addElement('text', 'publish_area', 'Область издания');
        $mform->setType('publish_area', PARAM_TEXT);

        $mform->addElement('text', 'city', 'Место');
        $mform->setType('city', PARAM_TEXT);

        $mform->addElement('text', 'publish_house', 'Издательство');
        $mform->setType('publish_house', PARAM_TEXT);

        $mform->addElement('text', 'publish_year', 'Год издания');
        $mform->setType('publish_year', PARAM_TEXT);

        $mform->addElement('text', 'pages', 'Количество страниц');
        $mform->setType('pages', PARAM_TEXT);

        $mform->addElement('text', 'illustrations', 'Иллюстрации');
        $mform->setType('illustrations', PARAM_TEXT);

        $mform->addElement('text', 'series', 'Серия');
        $mform->setType('series', PARAM_TEXT);

        $mform->addElement('text', 'isbn', 'ISBN');
        $mform->setType('isbn', PARAM_TEXT);

        $mform->addElement('text', 'price', 'Цена');
        $mform->setType('price', PARAM_TEXT);

        $mform->addElement('text', 'class_number', 'Классификационный индекс');
        $mform->setType('class_number', PARAM_TEXT);


        $filemanager_options = array();
        $filemanager_options['accepted_types'] = array('.jpg', '.png', 'jpeg');
        $filemanager_options['maxbytes'] = 500240;
        $filemanager_options['maxfiles'] = 1;
        $filemanager_options['mainfile'] = false;

        $mform->addElement('filemanager', 'book_image', 'Обложка книги', null, $filemanager_options);
        $mform->addRule('book_image', null, 'required', null, 'client');


        $filemanager_options = array();
        $filemanager_options['accepted_types'] = array('.pdf', '.docx', 'doc', '.djvu', '.epub', '.rtf');
        $filemanager_options['maxfiles'] = 1;
        $filemanager_options['mainfile'] = false;

        $mform->addElement('filemanager', 'book_file', 'Файл книги', null, $filemanager_options);
        $mform->addRule('book_file', null, 'required', null, 'client');


        $mform->addElement('text', 'annotation', 'Аннотация');
        $mform->setType('annotation', PARAM_TEXT);

        $tags = array();
        if ($this->id) {
            $mas = $DB->get_field('edu_lib', 'tags', array('id' => $this->id));
            if($mas = explode(',', $mas))
            {
                foreach ($mas as $m)
                    if($m) $tags[] = $m;
            }
        }

        //$PAGE->requires->js_init_call('startJSTreeLib', array('data' => tag::getCategoriesASObject(0, $tags)));
        $PAGE->requires->js_call_amd("local_edu/edu-lib", "startJSTreeLib", [tag::getCategoriesASObject(0, $tags)]);
        $mform->addElement('html', \html_writer::tag('h4', 'Категории') . \html_writer::div('', 'jstree-container-edit') . \html_writer::div('Выбрано категорий: ' . \html_writer::span(count($this->tags)), 'jstree-container-info') .
            \html_writer::empty_tag('input', array('type' => 'hidden', 'value' => '', 'name' => 'tags')));

        /*$mform->addElement('text', 'tags', 'Ключевые слова', 'size="50"');
        $mform->setType('tags', PARAM_TEXT);*/

        $mform->addElement('select', 'download', 'Скачать', array(1 => 'Да', 0 => 'Нет'));

        $mform->addElement('text', 'operator', 'Оператор');
        $mform->setType('operator', PARAM_TEXT);

        $this->add_action_buttons(false, "Сохранить");

    }

    function validation($data, $files)
    {
        global $DB;
        $error = parent::validation($data, $files);
        return $error;
    }

}

?>