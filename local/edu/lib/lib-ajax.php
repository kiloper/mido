<?php

require_once('../../../config.php');
//require_once($CFG->dirroot.'/local/edu/programs/helpers/helper.php');
require_once('lib.class.php');
require_once('tag.class.php');
require_once('webinary.class.php');
require_once('discipline.class.php');
require_once($CFG->libdir . '/excellib.class.php');
require_once($CFG->dirroot . '/local/edu/helpers.php');

$action = optional_param('action', "", PARAM_TEXT);
$type = optional_param('type', "book", PARAM_TEXT);
$start = optional_param('start', 0, PARAM_INT);
$step = optional_param('step', 100000, PARAM_INT);
$id = optional_param('id', 0, PARAM_INT);
$uc = optional_param('uc', 0, PARAM_INT);
$webid = optional_param('webid',0,PARAM_INT);
$mark = optional_param('mark',0,PARAM_INT);
$term = optional_param('term','',PARAM_TEXT);
$check = optional_param('check',0,PARAM_INT);
$date1 = optional_param('date1',0,PARAM_INT);
$date2 = optional_param('date2',0,PARAM_INT);
if (optional_param('json', 0, PARAM_INT)) {
    header('Content-Type: application/json');
}


\edu\webinary::$uc = $uc;
if(isset($SESSION->library_admin))
    \edu\webinary::$admin = $SESSION->library_admin;
if ( $action == "get_content" ){
    echo \edu\webinary::getContent($id);
    die;
}

if ( $action == "get_tags" ){
    echo json_encode(\edu\tag::getTagsAjax());
}

if ($action == 'excel') {
    if ($check) {
        $data = \edu\discipline::getSearchData(array(), array(), 0, 0, $date1, $date2);
    } else {
        $data = \edu\discipline::getSearchData(array(), array(), 0, 0);
    }
    $result = [];
    foreach ($data as $d) {
        $ob->title = $d->title;
        $ob->href = 'http://lib.lomonosov.online/course/view.php?id='. $d->courseid;
        $ob->author = $d->author ? $d->author : '';
        $ob->years = $d->years ? $d->years : '';
        $result[] = array($ob->title,$ob->href,$ob->author,$ob->years);
    }
    $SESSION->excel_data  = $result;
    echo json_encode($CFG->wwwroot . '/local/edu/lib/lib-ajax.php?action=getExcelFile');
}

if ($action == 'getExcelFile') {
    $downloadfilename = "Каталог электронных учебников.xls";
    $workbook = new \MoodleExcelWorkbook("-");
    $workbook->send($downloadfilename);
    $myxls = $workbook->add_worksheet("passed");
    $format = $workbook->add_format(array("bold" => 1, "h_align" => "center"));
    $myxls->set_column(0, 9, 60);
    $myxls->write_string(0, 0, "Название учебника", $format);
    $myxls->write_string(0, 1, "Ссылка", $format);
    $myxls->write_string(0, 2, "Автор", $format);
    $myxls->write_string(0, 3, "Год", $format);
    $i = 0;
    foreach ($SESSION->excel_data  as $data)
    {
        $i++;
        $myxls->write_string($i, 0, $data[0]);
        $myxls->write_string($i, 1, $data[1]);
        $myxls->write_string($i, 2, $data[2]);
        $myxls->write_string($i, 3, $data[3]);
    }
    $workbook->close();
    die;
}



if ( $action == "search" ){
    $fields = optional_param("fields", "", PARAM_RAW);
    $tags = optional_param("tags", "", PARAM_RAW);

    $fields = json_decode($fields);
    $tags = json_decode($tags);

    switch($type){
        case 'book':
            $data = \edu\lib::getSearchData($fields, $tags);
            echo json_encode(\edu\lib::getList($data));
            break;
        case 'webinary':
            $data = \edu\webinary::getSearchData($fields, $tags, $start, $step);
            echo json_encode(\edu\webinary::getList($data));
            break;
        case 'discipline':
            if ($check) {
                $data = \edu\discipline::getSearchData($fields, $tags, $start, $step,$date1,$date2);
            } else {
                $data = \edu\discipline::getSearchData($fields, $tags, $start, $step);
            }
            echo json_encode(\edu\discipline::getList($data));
            break;
    }

}

if ($action == 'viewWebinar' && $webid) {
    global $DB,$USER;
    $today = time();
    $ob = new stdClass();
    $ob->webid = $webid;
    $ob->userid = $USER->id;
    $ob->dateinsert = $today;
    $DB->insert_record('edu_lib_webinary_raiting',$ob);
}

if($action =='setLikeWebinar' && $mark && $webid)
{
    global $DB,$USER;
    $ob = new stdClass();
    $ob->webid = $webid;
    $ob->userid = $USER->id;
    $ob->dateinsert = time();
    $ob->mark = $mark;
    $DB->insert_record('edu_lib_webinary_liker',$ob);
    $rating = $DB->get_field_sql("select round(avg(mark),1) from {edu_lib_webinary_liker} where webid = $webid");
    if($rating)
    {
        $DB->set_field('edu_lib_webinary','rating',$rating,array('id'=>$webid));
    }
    $webinar = $DB->get_record('edu_lib_webinary', ['id' => $webid]);
    echo \edu\webinary::printRating($webinar, true);
}


if($action == 'getGroups' && $term)
{
    global $DB;
    $sql = "select * from {edu_lib_webinary_groups} where name like '%$term' or name like '%$term%' or name like '$term%'";
    if($groups = $DB->get_records_sql($sql))
    { $result = array();
        foreach ($groups as $g)
        {
            $gr = new stdClass();
            $gr->id = $g->id;
            $gr->value = $g->name;
            $result[] = $gr;
        }
        echo json_encode($result);
    }
}

if($action =='getThemes' && $term)
{
    global $DB;
    $sql = "select * from {edu_lib_webinary} 
      where  (title like '%$term' or title like '%$term%' or title like '$term%')";
    if($themes = $DB->get_records_sql($sql))
    {   $result = array();
        foreach ($themes as $w)
        {
            $web = new stdClass();
            $web->id = $w->id;
            $web->label = $w->title;
            $web->value = $w->title;
            $result[] = $web;
        }
        echo json_encode($result);
    }
}


?>