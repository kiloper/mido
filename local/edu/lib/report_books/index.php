<?php
require_once('../../../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot . '/local/edu/lib/report_books/locallib.php');

require_capability('local/edu:manage', context_system::instance());
admin_externalpage_setup('trainee_teacher');
//\teachers_report\teachersSDO::addPage($PAGE);

echo $OUTPUT->header();
echo $OUTPUT->heading("Отчет по Эл. учебникам без карточки учета ");


$report = new \local_lib_books_report\lib_books_report("local/edu/lib/report_books/index.php");

echo $report->view();

echo $OUTPUT->footer();


?>