<?php

namespace edu;

global $CFG; 
require_once("$CFG->libdir/formslib.php");


class tag
{
    private static $LIB_TAGS = "edu_lib_tags";
    public static $admin = true;
    public static $tabname = '';
    public static $tabvalue = '';

    public static function edit($id = 0)
    {
        global $DB, $PAGE;
        $form = new tag_form(new \moodle_url($PAGE->url));
        if ($id && $data = $DB->get_record(self::$LIB_TAGS, array("id" => $id))) {
            $form->set_data($data);
        }
        if ($data = $form->get_data()) {
            if ($data->id == 0) {
                //Добавляем новую запись
                if ($tagid = $DB->insert_record(self::$LIB_TAGS, $data)) {
                }
            } else {
                // Обновляем запись
                $DB->update_record(self::$LIB_TAGS, $data);
            }
            redirect(new \moodle_url($PAGE->url, ['action' => '']));
        }
        return $form->render();
    }

    /**
     * Для Autocomolete с категориями
     * @return Array $data - массив объектов БВ
     */
    public static function getTagsAjax()
    {
        global $DB;
        $mas = array();
        $data = $DB->get_records_sql("SELECT id,tag,parent FROM {edu_lib_tags} WHERE parent <> 0 ORDER BY parent, tag");
        foreach($data as $d){
            $parent = $DB->get_record("edu_lib_tags", array("id"=>$d->parent), 'tag');
            $ob = new \stdClass();
            $ob->label = $d->tag;
            $ob->category = $parent->tag;
            $mas[] = $ob;
        }
        return $mas;
    }

    public static function getCategoriesASObject($parent = 0, $selected = array())
    {
        global $DB;
        $mas = array();
        if($cats = $DB->get_records('edu_lib_tags', array('parent' => $parent), 'tag'))
        {
            foreach ($cats as $cat)
            {
                $ob = new \stdClass();
                $ob->text = $cat->tag;
                $ob->id = $cat->id;
                $ob->icon = false;
                $ob->state = new \stdClass();
                $ob->state->selected = in_array($cat->id, $selected);
                $ob->children = array();
                $childrens = self::getCategoriesASObject($cat->id, $selected);
                if($childrens) $ob->children = $childrens;
                $mas[] = $ob;
            }
        }
        return $mas;
    }

    public static function getCategoriesASMas($parent = 0, $level = 0)
    {
        global $DB;
        $mas = array();
        if($cats = $DB->get_records('edu_lib_tags', array('parent' => $parent), 'tag'))
        {
            foreach ($cats as $cat)
            {
                $spaces = '';
                for($i = 0; $i < $level; $i++)
                    $spaces .= '&#160;&#160;&#160;';
                $mas[$cat->id] = $spaces.$cat->tag;
                $childrens = self::getCategoriesASMas($cat->id, $level+1);
                if($childrens) $mas = $mas + $childrens;

            }
        }
        return $mas;
    }

    public static function getListForSearch($tagid = 0){
        global $DB, $OUTPUT, $PAGE;
        //$PAGE->requires->js_init_call('startJSTreeLib', array('data' => self::getCategoriesASObject(0, [$tagid])));
        $PAGE->requires->js_call_amd("local_edu/edu-lib", "startJSTreeLib", [self::getCategoriesASObject(0, [$tagid])]);
        $html = \html_writer::div('', 'jstree-container');
        /*$tags = $DB->get_records(self::$LIB_TAGS, array("parent"=>0), 'tag');
        foreach ($tags as $item) {
            $content = '';
            $childtags = $DB->get_records(self::$LIB_TAGS, array("parent" => $item->id), 'tag');
            foreach($childtags as $child){
                $content .=  \html_writer::start_tag("label");
                $content .=  \html_writer::checkbox("checkbox".$child->id, $child->id, false, '', array("class" => "childtag"));
                $content .=  $child->tag;
                $content .=  \html_writer::end_tag("label");
            }
            $content = \html_writer::div($content, 'span9');
            $title = \html_writer::checkbox("checkbox".$item->id, $item->id, false, '', array("class" => "parenttag")) . $item->tag;;
            $html .= \html_writer::div(\EduSlider::render($title, \html_writer::div($content, 'row-fluid')), 'eduslider');
        }*/
        return $html;
    }

    /**
     * Вывод книг в виже списка
     * @return String $html - HTML список книг
     */
    public static function getList($url = '/')
    {
        global $DB, $OUTPUT;
        $editimg = \html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/settings'), 'title' => 'Редактировать', 'class' => ''));
        $html = '';
        $tags = $DB->get_records(self::$LIB_TAGS, array("parent"=>0), 'tag');
        foreach ($tags as $item) {
            $content = '';
            $childtags = $DB->get_records(self::$LIB_TAGS, array("parent" => $item->id), 'tag');
            foreach($childtags as $child){
                $content .=  \html_writer::start_tag("label");
                $content .=  $child->tag;
                $edit = " " . \html_writer::link(new \moodle_url($url, array('uc' => self::$uc, 'tab' => 3, 'action' => 'edit', 'id' => $child->id)), $editimg);
                if(!self::$admin) $edit = '';
                $content .=  $edit;
                $content .=  \html_writer::end_tag("label");
            }
            $edit = " " . \html_writer::link(new \moodle_url($url, array('uc' => self::$uc, 'tab' => 3, 'action' => 'edit', 'id' => $item->id)), $editimg);
            if(!self::$admin) $edit = '';
            $content = \html_writer::div($content, 'span9');
            $html .= \html_writer::div(\EduSlider::render($item->tag . $edit, \html_writer::div($content, 'row-fluid')), 'eduslider');
        }
        return $html;
    }

    public static function view(){
        global $DB;
        //$tags = $DB->get_records(self::$LIB_TAGS);
        return self::getList();
    }

    public static function render($action = '', $id = 0){
        global $PAGE, $OUTPUT;
        if($action == 'edit') return self::edit($id);
        //$PAGE->requires->js_init_call('startJSTreeLib', array('data' => self::getCategoriesASObject(0)));
        $PAGE->requires->js_call_amd("local_edu/edu-lib", "startJSTreeLib", [self::getCategoriesASObject(0)]);
        $html = '<br/>';//\html_writer::div(\html_writer::link('/local/edu/webinary_list/', \html_writer::img($OUTPUT->pix_url('t/download'), 'download_list') . ' Скачать каталог'));
        $html .= \html_writer::tag('button', 'Редактировать', array('id' => 'editJsTree', 'disabled' => 'disabled', 'class' => 'btn btn-default'));
        $html .= \html_writer::div('', 'jstree-container-noncheck');
        return $html;

    }

    public static function get_fullpath_tagname($tid){
        global $DB;
        $tag = $DB->get_record("edu_lib_tags", ["id"=>$tid]);
        $html = $tag->tag;
        if ($tag->parent){
            $html = self::get_fullpath_tagname($tag->parent) . "/" . $html;
        }
        return $html;
    }

}



class tag_form extends \moodleform {

    function __construct($url)
    {
        parent::moodleform($url);
    }

    public function definition() {
        global $DB;
        $mform = $this->_form; // Don't forget the underscore!

        $mform->addElement('header', 'serach', "Новая категория");

        $id = optional_param("id", 0, PARAM_INT);
        $mform->addElement('hidden', 'id');
        $mform->setDefault('id', $id);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'type');
        $mform->setDefault('type', "tag");
        $mform->setType('type', PARAM_TEXT);

        $mform->addElement('text', 'tag', 'Название категории');
        $mform->setType('tag', PARAM_TEXT);
        $mform->addRule('tag', null, 'required', null, 'client');



        $tags_arr = array('0' => 'Главная категория') +  tag::getCategoriesASMas();

        $mform->addElement('select', 'parent', 'Родительская категория', $tags_arr);

        if ($id > 0){
            $url = new \moodle_url("/", ["lib" => 1, "tagid" => $id]);
            $link = \html_writer::link($url, $url, ["target" => "_blank"]);
            $mform->addElement('static', 'link', "Ссылка вебинары категории", $link);
        }

        $this->add_action_buttons(false, "Сохранить");
    }


    //Custom validation should be added here
    function validation($data, $files) {
        global $DB;
        $error = parent::validation($data, $files);
        /*if(empty($data['id']))
            if($DB->get_field('edu_orgs', 'id', array('shortname' => trim($data['shortname'])))) $error['shortname'] = 'Организация с таким именем уже есть';
        if(empty($data['id']))
            if($DB->get_field('edu_orgs', 'id', array('name' => trim($data['name'])))) $error['name'] = 'Организация с таким именем уже есть';*/
        return $error;
    }

}


?>