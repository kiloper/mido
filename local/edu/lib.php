<?php
function local_edu_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $CFG, $DB;
    require_once("$CFG->libdir/resourcelib.php");
    if ($context->contextlevel != CONTEXT_SYSTEM) {
        return false;
    }
    $itemid = array_shift($args);
    $filename = array_pop($args);

    $path = implode('/', $args);
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'local_edu', $filearea, $itemid, '/'.$path.'/', $filename);
    if (!$file) {
        return false; // The file does not exist.
    }
    send_stored_file($file, null, 0, $forcedownload, $options);
}


?>