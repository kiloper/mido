<?php
$capabilities = array(
    // Право управления деканатом
    'local/edu:manage' => array(
        'riskbitmask' => RISK_SPAM | RISK_XSS,

        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM
    )
    );