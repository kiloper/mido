<?php
require_once('../../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once('./edit_form.php');
$id = optional_param("id", 0, PARAM_INT);
$systemcontext = context_system::instance();
$isManager = false;
require_capability('local/edu:manage', $systemcontext);
admin_externalpage_setup('main_menu_list');
$delete = optional_param("delete", 0, PARAM_INT);
$id = optional_param("id", 0, PARAM_INT);



echo $OUTPUT->header();
echo $OUTPUT->heading("Меню");
$form = new edit_form();
if (($delete) && ($id)) {
    $DB->delete_records('edu_menu', array('id' => $id));
    notice("Модуль удален", new moodle_url("/local/edu/main_menu/index.php"));
}
if($form->is_cancelled()) redirect(new moodle_url("/local/edu/main_menu/index.php"));
if ($data = $form->get_data()) {
    if ($data->id) {
        $DB->update_record('edu_menu', $data);
        if($isManager)
            notice("Параметры модуля сохранены", new moodle_url("/"));
        else
            notice("Параметры модуля сохранены", new moodle_url("/local/edu/main_menu/index.php"));
    } else {
        $DB->insert_record('edu_menu', $data);
        notice("Модуль создан", new moodle_url("/local/edu/main_menu/index.php"));
    }
}
{
    if($id)
    {
            $form->set_data($data);
    }
}
$form->display();

echo $OUTPUT->footer();

?>