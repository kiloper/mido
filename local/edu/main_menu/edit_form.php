<?php
require_once("$CFG->libdir/formslib.php");
class edit_form extends moodleform {

    public function definition() {
        global $DB,$CFG;
        $mform = $this->_form; // Don't forget the underscore!
        $mform->addElement('header', 'serach', "Модуль основного меню");
        $id = optional_param("id", 0, PARAM_INT);
        $mform->addElement('hidden', 'id');
        $mform->setDefault('id', $id);
        $mform->setType('id', PARAM_INT);


        $mform->addElement('text', 'name', 'Название');
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required');

        $mform->addElement('text', 'url', 'Cссылка');
        $mform->setType('url', PARAM_TEXT);
        $mform->addRule('url', null, 'required');


        $mform->addElement('static', 'mmm', 'Список иконок', html_writer::link("https://fontawesome.com/v4.7.0/icons/", "https://fontawesome.com/v4.7.0/icons/", ['target' => '_blank']));
        $mform->addElement('text', 'icon', 'Иконка');
        $mform->setType('icon', PARAM_TEXT);
        $mform->addRule('icon', null, 'required');



        if($id && $tag = $DB->get_record('edu_menu', array('id' => $id)))
        {
            $mform->setDefault('name', $tag->name);
            $mform->setDefault('url', $tag->url);
            $mform->setDefault('icon', $tag->icon);
            $this->add_action_buttons(false, "Сохранить");
        } else
            $this->add_action_buttons(false, "Создать");
    }

}

?>