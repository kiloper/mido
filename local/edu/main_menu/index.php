<?php
require_once('../../../config.php');
require_once($CFG->libdir . '/adminlib.php');

admin_externalpage_setup('main_menu_list');

echo $OUTPUT->header();
echo $OUTPUT->heading("Меню");

$orgs = $DB->get_records_sql("select * from {edu_menu} order by name");

$table = new html_table();
$table->head = array("Название модуля", "Управление",'');
$num = array();


$table->data = array();
foreach($orgs as $tag) {
    $data = array($tag->name);
    $delimg = html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'title'=>'Удалить модуль', 'class'=>'iconsmall'));
    $delete = $OUTPUT->action_link($CFG->wwwroot.'/local/edu/main_menu/create.php?id='.$tag->id."&delete=1", $delimg, new confirm_action("Удалить модуль?", null));
    $data[] = /*$courses.$news.*/html_writer::link(new moodle_url('/local/edu/main_menu/create.php', array('id'=>$tag->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'title'=>'Редактировать модуль', 'class'=>'iconsmall'))).$delete;;
    $table->data[] = $data;
}

echo $OUTPUT->single_button('/local/edu/main_menu/create.php', 'Создать');

echo html_writer::table($table);

echo $OUTPUT->footer();

?>