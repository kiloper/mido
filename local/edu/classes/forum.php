<?php

namespace local_edu;


class forum extends edu
{

    function getForumByCourse($cid)
    {
        global $DB;
        return $DB->get_records_sql("select f.*, cm.id as cmid from {course_modules} as cm, {modules} as m, {forum} as f where cm.course = ? and cm.visible = 1 and cm.deletioninprogress = 0 and cm.module = m.id and m.name = 'forum' and cm.instance = f.id", [$cid]);
    }

    function getForumsByCourses($cs)
    {
        $mas = [];
        $num = 0;
        foreach ($cs as $c) {
            if ($fs = $this->getForumByCourse($c->id)) {
                $mas[$c->id] = [];
                foreach ($fs as $f) {
                    $f->num = self::getNoReadForums($f->id);
                    $num += $f->num;
                    $mas[$c->id][$f->id] = $f;

                }
            }
        }
        return [$mas, $num];
    }

    function getForums($pid)
    {
        global $DB;
        $mas = [];
        $p = $this->getProgram($pid);
        if ($forum = self::getForumForProgram($pid)) {
            if ($fs = $this->getForumByCourse($forum->id)) {
                $mas['p' . $p->id] = [];
                foreach ($fs as $f) {
                    $mas['p' . $p->id][$f->id] = $f;
                }
            }

        }
        $courses = $this->getCourses($pid);
        list($colors, $comments, $info, $completed, $access) = $this->getInfoByCourses($pid);
        $i = 0;
        $isManager = $this->isProgramsForManager();
        foreach ($courses as $c) {
            $i++;
            if ($access[$i - 1] || $isManager) {
                if ($fs = $this->getForumByCourse($c->id)) {
                    $mas[$c->id] = [];
                    foreach ($fs as $f) {
                        $mas[$c->id][$f->id] = $f;
                    }
                }
            }
        }
        return $mas;
    }

    function getNoReadForums($fid)
    {
        global $DB;
        $lastaccess = $DB->get_field('edu_forums_views', 'timeinsert', ['userid' => $this->userid, 'forumid' => $fid]);
        $lastaccess = $lastaccess ? $lastaccess : 0;
        return $DB->get_field_sql("select count(p.id) from {forum_posts} as p, {forum_discussions} as d where p.discussion = d.id and d.forum = ? and p.modified > $lastaccess", [$fid, $lastaccess]);
    }

    function getAllNoReadForums($pid)
    {
        $summ = 0;
        if ($forums = $this->getForums($pid)) {
            foreach ($forums as $k => $fs) {
                foreach ($fs as $f) {
                    $summ += $this->getNoReadForums($f->id);
                }
            }
        }
        return $summ;
    }


    static function getForumForProgram($pid)
    {
        global $DB;
        return $DB->get_record('course', array('idnumber' => "pforum-" . $pid));
    }

    static function exportForum($idnumber)
    {
        $userid = 2;
        global $DB, $CFG;
        $course = $DB->get_record("course", array('idnumber' => $idnumber));
        if (empty($course)) return false;
        $id = $course->id;
        @ini_set('memory_limit', '128M');
        $CFG->keeptempdirectoriesonbackup = true;
        require_once($CFG->dirroot . '/backup/util/includes/backup_includes.php');
        set_time_limit(600);
        $bc = new \backup_controller(\backup::TYPE_1COURSE, $id, \backup::FORMAT_MOODLE,
            \backup::INTERACTIVE_NO, \backup::MODE_IMPORT, $userid);
        $bc->execute_plan();
        return $bc->get_backupid();
    }

    static function importForum($backupid, $categoryid = 15)
    {
        @ini_set('memory_limit', '512M');
        global $CFG, $USER;
        $CFG->enableavailability = true;
        require_once($CFG->dirroot . '/backup/util/includes/restore_includes.php');
        set_time_limit(600);
        $courseid = \restore_dbops::create_new_course('', '', $categoryid);
        $rc = new \restore_controller($backupid, $courseid, \backup::INTERACTIVE_NO, \backup::MODE_IMPORT, 2, \backup::TARGET_NEW_COURSE);
        $rc->execute_precheck();
        $rc->execute_plan();
        return $courseid;

    }

    static function updateForum($shortname, $fullname, $courseid, $idnumber = '', $summary = '', $visible = 1)
    {
        global $DB;
        $course = $DB->get_record('course', array('id' => $courseid));
        if (empty($course)) return null;
        $course->shortname = $shortname;
        $course->fullname = $fullname;
        $course->startdate = time();
        $course->visible = $visible;
        if ($course->summary)
            $course->summary = $summary;
        if ($idnumber)
            $course->idnumber = $idnumber;
        $DB->update_record('course', $course);
        if ($forum = $DB->get_record("forum", array('course' => $courseid))) {
            $forum->name = $fullname;
            $forum->intro = $fullname;
            $DB->update_record('forum', $forum);
        }
        return $course;
    }

    static function forumForProgram($p)
    {
        global $DB;
        $courseforforum = self::getForumForProgram($p->id);
        if (empty($courseforforum)) {
            if ($bid = self::exportForum("pforum")) {
                if ($courseid = self::importForum($bid)) {
                    $name = "Форум \"{$p->name}\"";
                    self::updateForum($name, $name, $courseid, "pforum-" . $p->id);
                }
            }
        } else {
            $name = "Форум \"{$p->name}\"";
            if ($name != $courseforforum->fullname) {
                self::updateForum($name, $name, $courseforforum->id, "pforum-" . $p->id);
            }
        }
    }
}

?>