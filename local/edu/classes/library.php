<?php


namespace local_edu;

require_once($CFG->dirroot . '/lib/formslib.php');
require_once($CFG->dirroot . '/local/edu/helpers.php');
require_once($CFG->dirroot . '/local/edu/lib/lib.class.php');
require_once($CFG->dirroot . '/local/edu/lib/tag.class.php');
require_once($CFG->dirroot . '/local/edu/lib/webinary.class.php');
require_once($CFG->dirroot . '/local/edu/lib/discipline.class.php');
require_once($CFG->dirroot . '/local/edu/lib/classes/ActionButton.class.php');

use html_writer;
use BootsrapTabs;
use moodle_url;
use EduButton;


class library
{
    static function render()
    {
//        return html_writer::tag('iframe', '', ['src' => "https://sdo.gaps.edu.ru/library.php", 'width' => "100%", 'height' => "100%", 'frameborder' => "0", 'allowfullscreen' => 'allowfullscreen', 'style' => 'min-height: 1000px;']);


        global $PAGE, $SESSION;
        $PAGE->set_url(new moodle_url($PAGE->url, ['to' => optional_param('to', '', PARAM_TEXT)]));
        $libhtml = '';
        $admin = is_siteadmin();
        $book_link = (string)new moodle_url($PAGE->url, ['action' => 'edit', 'libtab' => 0]);
        $webinary_link = (string)new moodle_url($PAGE->url, ['action' => 'edit', 'libtab' => 1]);
        $webinary_link1 = (string)new moodle_url($PAGE->url, ['action' => 'edit', 'libtab' => 1, 'subtype' => 1]);
        $webinary_link2 = (string)new moodle_url($PAGE->url, ['action' => 'edit', 'libtab' => 1, 'subtype' => 2]);
        $discipline_link = (string)new moodle_url($PAGE->url, ['action' => 'edit', 'libtab' => 2]);
        $tag_link = (string)new moodle_url($PAGE->url, ['action' => 'edit', 'libtab' => 3]);
        $html = '';
        $action_btn = new EduButton(
            "Создать",
            array(
                //$book_link => "Электронную книгу",
                $webinary_link => "Файл",
                $webinary_link1 => "Видео",
                $webinary_link2 => "YouTube видео",
                //$discipline_link => "Электронный учебник",
                $tag_link => "Категорию"
            ),
            "success"
        );
        $info = '';
        $html = $admin ? $action_btn->render() : '';

        $html .= "<br><br>";
//if (!$admin) $html = '';
        $tabid = optional_param('libtab', 0, PARAM_INT);
        $action = optional_param('action', '', PARAM_TEXT);
        $PAGE->set_url(new moodle_url($PAGE->url, ['libtab' => $tabid, 'action' => $action]));
        $id = optional_param('id', 0, PARAM_INT);
        $lib = optional_param('lib', 0, PARAM_INT);
        $tabid = empty($lib) ? $tabid : 1;
        $SESSION->library_admin = $admin;
        \edu\lib::$admin = $admin;
        \edu\webinary::$admin = $admin;
        \edu\tag::$admin = $admin;
        \edu\discipline::$admin = $admin;

        \edu\tag::$tabname = 'libtab';
        \edu\tag::$tabvalue = 3;
        $tabUc = html_writer::empty_tag('input', array('type' => 'hidden', 'id' => 'tabUc', 'value' => 0));

        $libhtml .= $info . $html . $tabUc;

        $tabs = new BootsrapTabs();
        $tabs->add('webinars', 'Ресурсы', \edu\webinary::render($action, $id), $tabid == 1, new moodle_url($PAGE->url, ['action' => '', 'libtab' => 1]));
        if ($admin)
            $tabs->add('tags', 'Категории', \edu\tag::render($action, $id), $tabid == 3, new moodle_url($PAGE->url, ['action' => '', 'libtab' => 3]));
        $libhtml .= html_writer::div($tabs->render(), 'library');


        $PAGE->requires->js_call_amd("local_edu/edu-lib", "init", [str_replace("&amp;", "&", (string)$PAGE->url)]);
        return $libhtml;
    }
}

?>