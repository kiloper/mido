<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Event observers used in forum.
 *
 * @package    mod_forum
 * @copyright  2013 Rajesh Taneja <rajesh@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();



class local_edu_observer
{
    static function forum_viewed($forum)
    {
        global $DB, $USER;
        if(!$DB->record_exists('edu_forums_views', ['userid' => $USER->id, 'forumid' => $forum->objectid]))
        {
            $ob = new stdClass();
            $ob->timeinsert = time();
            $ob->userid = $USER->id;
            $ob->forumid = $forum->objectid;
            $DB->insert_record('edu_forums_views', $ob);
        } else
            $DB->set_field('edu_forums_views', 'timeinsert' , time(), ['userid' => $USER->id, 'forumid' => $forum->objectid]);
    }

}