<?php

namespace local_edu;


class edu
{
    public $programs;
    public $userid;
    public $isedu = false;
    public $pid = 0;

    function __construct($userid)
    {
        global $SESSION;
        $this->userid = $userid;
        $this->programs = $this->_getPrograms();
        $this->isedu = $this->isEdu();
        if (empty($pid) && $this->isedu) {
            $this->pid = reset($this->programs)->id;
        }
        $this->pid = optional_param('pid', isset($SESSION->pid) ? $SESSION->pid : 0, PARAM_INT);
        if (empty($this->pid))
            $this->pid = reset($this->programs)->id;
        $SESSION->pid = $this->pid;
    }

    function isProgramsForManager()
    {
        global $DB;
        return $DB->get_records_sql("select * from {edu_programs} where managerid = {$this->userid}");
    }

    function isTeacher()
    {
        global $DB;
        $sql = "(
        select u.*
        from mdl_user_enrolments as ue, 
        mdl_enrol as e, 
        mdl_user as u, 
        mdl_context as c, 
        mdl_role_assignments as a
        where
        ue.enrolid = e.id and
        ue.userid = u.id and
        u.id = {$this->userid} and
        e.status = 0 and
        e.enrol = 'manual' and
        c.instanceid = e.courseid and
        c.contextlevel = 50 and
        a.contextid = c.id and 
        a.roleid = 3 and 
        a.itemid = 0 and 
        a.userid = ue.userid
        )";
        return $DB->record_exists_sql($sql);
    }


    function getCoursesForRole($roleid)
    {
        global $DB;
        $sql = "(
        select co.*
        from mdl_user_enrolments as ue, 
        mdl_enrol as e, 
        mdl_user as u, 
        mdl_context as c, 
        mdl_role_assignments as a,
        mdl_course as co
        where
        ue.enrolid = e.id and
        ue.userid = u.id and
        u.id = {$this->userid} and
        e.status = 0 and
        e.enrol = 'manual' and
        e.courseid = co.id and
        c.instanceid = e.courseid and
        c.contextlevel = 50 and
        a.contextid = c.id and 
        a.roleid = $roleid and 
        a.itemid = 0 and 
        a.userid = ue.userid
        )";
        return $DB->get_records_sql($sql);
    }

    function isEdu()
    {
        return $this->userid && (!empty($this->programs) || $this->isTeacher() || $this->isProgramsForManager());
    }

    function getIdent()
    {
        global $DB;
        return $DB->get_record('edu_ident', ['userid' => $this->userid]);
    }

    static function getNews($pid)
    {
        global $DB;
        return $DB->get_records_sql("select * from {edu_programs_info} where (pid = 0 or pid = $pid)");
    }

    static function getFiles($pid)
    {
        $mas = array();
        $systemcontext = \context_system::instance();
        $fs = get_file_storage();
        $files = $fs->get_area_files(1, 'local_edu', 'programs', $pid);
        $i = 0;
        foreach ($files as $file) {
            $i++;
            $name = $file->get_filename();
            if ($name == '.') continue;
            if ($name == '..') continue;
            $mas[] = (string)\moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename());

        }
        return $mas;
    }

    private function _getPrograms()
    {
        global $DB;
        return $DB->get_records_sql("select p.*, g.timestart from {edu_programs} as p, {edu_programs_subs} as s, {edu_groups} as g, {edu_groups_members} as m
        where p.id = s.pid and s.groupid = g.id and g.id = m.groupid and m.userid = ?", [$this->userid]);
    }

    public function isCompleted($courseid)
    {
        global $DB;
        return $DB->record_exists_sql("select id from {course_completions} where userid = {$this->userid} and course = {$courseid} and timecompleted > 0");
    }

    public function getProgram($pid = null)
    {
        if ($pid && isset($this->programs[$pid])) return $this->programs[$pid];
        if ($this->pid && isset($this->programs[$this->pid])) return $this->programs[$this->pid];
        return reset($this->programs);
    }

    public function getCourses($pid)
    {
        global $DB;
        $start = $this->programs[$pid]->timestart;
        $courses = $DB->get_records_sql("select c.id, c.fullname, pc.name, pc.open_course, pc.close_course, pc.days, pc.hours, pc.relativecourses, pc.control, pc.ident from {course} as c, {edu_programs_courses} as pc where pc.pid = ? and pc.courseid = c.id order by pc.id", [$pid]);
        $cs = [];
        foreach ($courses as $c) {
            $c->start = $start + $c->open_course * 3600 * 24;
            $c->end = $c->close_course ? $start + $c->close_course * 3600 * 24 : 0;
            $c->mustlearn = $c->days ? $start + $c->days * 3600 * 24 : 0;
            $cs[$c->id] = $c;
        }
        return $cs;
    }

    public function getInfoByCourses($pid)
    {
        $now = time();
        $courses = $this->getCourses($pid);
        $colors = $comments = $info = $completed = $access = [];
        $i = 0;
        $numcourses = [];
        foreach ($courses as $c) {
            $numcourses[] = $c;
        }
        $ident = $this->getIdent();

        foreach ($courses as $c) {
            $info[$i] = [];
            $colors[$i] = '';
            $comments[$i] = '';
            $access[$i] = false;
            $in = new \stdClass();
            $in->text = '';
            $in->status = 'no';
            if ($now < $c->start) {
                $colors[$i] = 'lgrey';
                $comments[$i] = 'Дисциплина пока недоступна';
                $in->text = 'Дисциплина будет доступна с ' . date('d.m.Y', $c->start);
            }
            if ($now > $c->end && $c->end) {
                $colors[$i] = 'lgrey';
                $comments[$i] = 'Дисциплина завершена';
                $in->text = 'Дисциплина завершена';
            }
            if ($now > $c->start && (($now < $c->end) || empty($c->end))) {
                $colors[$i] = 'lgreen';
                $access[$i] = true;
                $comments[$i] = 'Дисциплина на этапе изучения';
                $in->status = 'yes';
                if ($c->end)
                    $in->text = 'Дисциплина будет доступна до ' . date('d.m.Y', $c->end);
                else
                    $in->text = 'Дисциплина будет доступна до ' . date('d.m.Y', $this->programs[$pid]->duration * 3600 * 24 + $this->programs[$pid]->timestart);
            }

            if ($now > $c->start && $c->mustlearn && $now > $c->mustlearn) {
                $colors[$i] = 'lred';
                $access[$i] = true;
                $comments[$i] = 'Вы опаздываете с обучением по данной дисциплине';
                if ($c->end)
                    $in->text = 'Дисциплина будет доступна до ' . date('d.m.Y', $c->end);
                else
                    $in->text = 'Дисциплина будет доступна до ' . date('d.m.Y', $this->programs[$pid]->duration * 3600 * 24 + $this->programs[$pid]->timestart);
                if ($c->end && $now > $c->end) {
                    $in->text = 'Дисциплина завершена';
                    $access[$i] = false;
                }
            }
            if ($now > $c->start && (($now < $c->end) || empty($c->end)) && $c->mustlearn && $now > $c->mustlearn - 3600 * 24 * 3) {
                $colors[$i] = 'lyellow';
                $access[$i] = true;
                $in->status = 'yes';
                $comments[$i] = 'Вы опаздываете с обучением по данной дисциплине';
                if ($c->end)
                    $in->text = 'Дисциплина будет доступна до ' . date('d.m.Y', $c->end);
                else
                    $in->text = 'Дисциплина будет доступна до ' . date('d.m.Y', $this->programs[$pid]->duration * 3600 * 24 + $this->programs[$pid]->timestart);
            }
            if ($now > $this->programs[$pid]->duration * 3600 * 24 + $this->programs[$pid]->timestart) {
                $colors[$i] = 'lred';
                $access[$i] = false;
                $comments[$i] = 'Вы опаздываете с обучением по данной дисциплине';
                if ($c->end)
                    $in->text = 'Дисциплина будет доступна до ' . date('d.m.Y', $c->end);
                else
                    $in->text = 'Дисциплина будет доступна до ' . date('d.m.Y', $this->programs[$pid]->duration * 3600 * 24 + $this->programs[$pid]->timestart);
            }
            $in1 = [];
            if ($c->ident) {
                if (empty($ident)) {
                    $in->status = 'no';
                    $in->text = "Необходимо " . \html_writer::link("/?to=ident&pid=$pid&courseid=" . $c->id, "подтвердить личность");
                    $colors[$i] = 'lred';
                    $access[$i] = false;
                    $comments[$i] = 'Не пройдена идентификация личности';
                }
                if ($ident && $ident->status == -1) {
                    $in->status = 'no';
                    $in->text = \html_writer::link("/?to=ident&pid=$pid&courseid=" . $c->id, "Идентификация отклоненна");
                    $colors[$i] = 'lred';
                    $access[$i] = false;
                    $comments[$i] = 'Не пройдена идентификация личности';
                }
            }
            if ($c->relativecourses) {
                $rc = explode(',', $c->relativecourses);
                $mustc = [];
                foreach ($rc as $num) {
                    if (isset($numcourses[$num - 1]) && !$this->isCompleted($numcourses[$num - 1]->id)) {
                        $access[$i] = false;
                        $ob = new \stdClass();
                        $ob->status = "no";
                        $ob->text = "Не изучена дисциплина \"" . ($numcourses[$num - 1]->name ? $numcourses[$num - 1]->name : $numcourses[$num - 1]->fullname) . "\"";
                        $in1[] = $ob;
                    }
                }

            }

            $completed[$i] = $this->isCompleted($c->id);
            if ($completed[$i]) {
                $colors[$i] = 'lgreen';
                $comments[$i] = 'Дисциплина пройдена';
                $in->text = 'Дисциплина пройдена';
                $in->status = 'yes';
            }
            $info[$i][] = $in;
            $info[$i] = array_merge($info[$i], $in1);
            $i++;
        }
        return [$colors, $comments, $info, $completed, $access];
    }

    public function getTeachers($cid)
    {
        global $DB;
        $sql = "(
        select u.*
        from mdl_user_enrolments as ue, 
        mdl_enrol as e, 
        mdl_user as u, 
        mdl_context as c, 
        mdl_role_assignments as a
        where
        ue.enrolid = e.id and
        ue.userid = u.id and
        e.courseid = $cid and
        e.status = 0 and
        c.instanceid = e.courseid and
        c.contextlevel = 50 and
        a.contextid = c.id and 
        a.roleid = 3 and 
        a.itemid = 0 and 
        a.userid = ue.userid
        )";
        return $DB->get_records_sql($sql);
    }

    public function getGrade($courseid)
    {
        //var_dump($context = context_course::instance(3)->id);die;
        global $DB, $CFG;
        require_once($CFG->libdir . "/gradelib.php");
        $result = array();
        $course = null;
        if ($items = $DB->get_records('grade_items', array('courseid' => $courseid, 'hidden' => 0), 'sortorder')) {
            foreach ($items as $i) {
                if ($i->itemtype != 'course') continue;
                if ($grades = grade_get_grades($courseid, $i->itemtype, $i->itemmodule, $i->iteminstance, $this->userid)) {

                    $it = (reset($grades->items));
                    return [$it->grades[$this->userid]->grade, $it->grades[$this->userid]->str_grade];

                }
            }
        }
        return array(0, '-');
    }

    static function getCourseImage($cid)
    {
        global $CFG;
        require_once($CFG->libdir . '/filestorage/file_storage.php');
        $fs = get_file_storage();
        $context = \context_course::instance($cid);
        $files = $fs->get_area_files($context->id, 'course', 'overviewfiles', false, 'filename', false);
        foreach ($files as $file) {
            $isimage = $file->is_valid_image();
            $url = file_encode_url("$CFG->wwwroot/pluginfile.php",
                '/' . $file->get_contextid() . '/' . $file->get_component() . '/' .
                $file->get_filearea() . $file->get_filepath() . $file->get_filename(), !$isimage);
            if ($isimage) {
                return $url;
            }
        }
        return '';
    }

    function getCoursesIfTeacher()
    {
        global $DB;
        $sql = "select course.*

                from 
                {user_enrolments} as ue, 
                {enrol} as e, 
                {user} as u, 
                {context} as c, 
                {role_assignments} as a, 
                {course} as course
                
                where
                ue.userid = {$this->userid} and
                ue.enrolid = e.id and 
                ue.userid = u.id and
                e.status = 0 and
                e.courseid = course.id and
                e.enrol = 'manual' and 
                c.instanceid = e.courseid and 
                c.contextlevel = 50 and
                a.contextid = c.id and 
                a.roleid = 3 and 
                a.userid = ue.userid";

        return $DB->get_records_sql($sql);
    }

    function getNoGradesAssignData()
    {
        $assigns = [];
        $users = [];
        $courseids = [];
        if ($courses = $this->getCoursesIfTeacher()) {
            foreach ($courses as $c) {
                $courseids[] = $c->id;
                $users_list = self::getUsers($c->id, 5);
                foreach ($users_list as $u) {
                    if (!in_array($u->id, $users)) {
                        $users[] = $u->id;
                    }
                }
            }
            $assigns_t = $this->getNoGradesAssigns($courseids, $users);
            foreach ($assigns_t as $a) {
                $assigns[] = $a;
            }
        }

        $this->sortNoGradesAssigns($assigns);

        return $assigns;
    }

    static function getGroupsByPrograms($pid)
    {
        $users = self::getUsersByPrograms($pid);
        $groups = [];
        foreach ($users as $u) {
            $groups[$u->gid] = $u->gname;
        }
        return $groups;
    }

    static function getUsersByProgramsIdent($pid, $status = null)
    {
        global $DB;
        if (empty($status))
            $status = [-1, 1, 0];
        if (!is_array($pid))
            $pid = [$pid];
        if (!is_array($status))
            $status = [$status];
        $pid = implode(',', $pid);
        $status = implode(',', $status);
        return $DB->get_records_sql("select concat(u.id,'-',g.id) as id, u.id as userid, u.firstname, u.lastname, i.status as istatus, g.name as gname, p.name as pname, p.id as pid, g.id as gid, g.timestart from 
              {user} as u, {edu_groups_members} as m, {edu_groups} as g, {edu_programs_subs} as s, {edu_programs} as p, {edu_ident} as i
              where u.deleted = 0 and u.id = m.userid and m.groupid = g.id and g.id = s.groupid and s.pid = p.id and p.id  in ($pid) and i.userid = u.id and i.status in ($status) order by u.firstname");
    }

    static function getUsersByPrograms($pid)
    {
        global $DB;
        if (!is_array($pid))
            $pid = [$pid];
        $pid = implode(',', $pid);
        return $DB->get_records_sql("select concat(u.id,'-',g.id) as id, u.id as userid, u.firstname, u.lastname, i.status as istatus, g.name as gname, p.name as pname, p.id as pid, g.id as gid, g.timestart from 
              {user} as u left join {edu_ident} as i on (u.id = i.userid), {edu_groups_members} as m, {edu_groups} as g, {edu_programs_subs} as s, {edu_programs} as p
              where u.deleted = 0 and u.id = m.userid and m.groupid = g.id and g.id = s.groupid and s.pid = p.id and p.id  in ($pid) order by u.firstname");
    }


    static function getUsers($courseid, $roleid)
    {
        global $DB;
        $context = \context_course::instance($courseid);
        list($gradebookroles_sql, $params) = $DB->get_in_or_equal($roleid, SQL_PARAMS_NAMED, 'grbr');
        list($enrolledsql, $enrolledparams) = get_enrolled_sql($context);
        $relatedcontexts = "IN (" . implode(',', $context->get_parent_context_ids(true)) . ")";
        $params = array_merge($params, $enrolledparams);
        $fields = "u.id, u.username, u.firstname, u.lastname";
        $users_sql = "SELECT $fields
                        FROM {user} u
                        JOIN ($enrolledsql) je ON je.id = u.id

                        JOIN (
                                  SELECT DISTINCT ra.userid
                                    FROM {role_assignments} ra
                                   WHERE ra.roleid $gradebookroles_sql
                                     AND ra.contextid $relatedcontexts
                             ) rainner ON rainner.userid = u.id
                         WHERE u.deleted = 0 and u.suspended = 0

                    ORDER BY lastname";
        $result = array();
        if ($users = $DB->get_records_sql($users_sql, $params)) {
            foreach ($users as $user) {
                if ($user->userduration) $user->duration = $user->userduration;
            }
            return $users;
        }
        return $result;
    }

    function getNoGradesAssigns($cid, $usersids = null)
    {
        global $DB;
        $userssql = '';
        if ($usersids) $userssql = "and asub.userid in (" . implode(',', $usersids) . ")";
        if (empty($usersids)) return array();
        $courselike = '';
        if (is_array($cid))
            $courselike = "a.course in (" . implode(',', $cid) . ")";
        else
            $courselike = "a.course = $cid";

        $sql = "select asub.*, a.id as assignid, cm.course as courseid, cm.id as cmid, c.fullname as coursename
            from {assign_submission} as asub, 
                 {assign_grades} as agrade, 
                 {assign} as a, 
                 {course_modules} as cm, 
                 {course} as c
            where 
                1 $userssql and 
                asub.assignment = agrade.assignment and 
                agrade.userid = asub.userid and 
                agrade.attemptnumber = asub.attemptnumber and 
                agrade.timemodified <= asub.timemodified and 
                asub.assignment = a.id and 
                $courselike and 
                asub.status = 'submitted' and 
                asub.latest = 1 and 
                cm.instance = a.id and 
                cm.module = 1 and
                c.id = cm.course
            union
            select asub.*, a.id as assignid, cm.course as courseid, cm.id as cmid, c.fullname as coursename
            from {assign_submission} asub 
                 left outer join {assign_grades} agrade 
                 on (asub.userid = agrade.userid and 
                     asub.attemptnumber = agrade.attemptnumber and 
                     asub.assignment = agrade.assignment), 
                {assign} as a, 
                {course_modules} as cm,
                {course} as c
            where 
                1 $userssql and 
                agrade.id is NULL and 
                asub.assignment = a.id and 
                $courselike and 
                asub.status = 'submitted' and 
                asub.latest = 1 and 
                cm.instance = a.id and 
                cm.module = 1 and
                c.id = cm.course
            order by timemodified";
        return $DB->get_records_sql($sql);
    }

    function sortNoGradesAssigns(&$assigns)
    {
        usort($assigns, 'sortCallback');
    }

}


?>