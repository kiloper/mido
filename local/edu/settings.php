<?php


defined('MOODLE_INTERNAL') || die();

/*$previewnode = $PAGE->navigation->add(get_string('preview'), new moodle_url('/a/link/if/you/want/one.php'), navigation_node::TYPE_CONTAINER);
$thingnode = $previewnode->add(get_string('name of thing'), new moodle_url('/a/link/if/you/want/one.php'));
$thingnode->make_active();*/

if (true) {
    $ADMIN->add('root', new admin_category('edu', 'Электронный деканат'));
    //$ADMIN->add('edu', new admin_category('programs', 'Образовательные программы'));

    $ADMIN->add('edu', new admin_externalpage('programs_list', 'Образовательные программы', $CFG->wwwroot . '/local/edu/programs/index.php', 'local/edu:manage'));
    $ADMIN->add('edu', new admin_externalpage('programs_create', 'Создать программу', $CFG->wwwroot . '/local/edu/programs/create.php', 'local/edu:manage'));
    $ADMIN->add('edu', new admin_externalpage('programs_groups', 'Группы', $CFG->wwwroot . '/local/edu/programs/groups.php', 'local/edu:manage'));
    $ADMIN->add('edu', new admin_externalpage('main_menu_list', 'Меню', $CFG->wwwroot . '/local/edu/main_menu/index.php', 'local/edu:manage'));
    $ADMIN->add('edu', new admin_externalpage('library', 'Электронная библиотека', $CFG->wwwroot . '/local/edu/lib/', 'local/edu:manage'));
    //$ADMIN->add('programs', new admin_externalpage('programs_create', 'Создать', $CFG->wwwroot . '/local/edu/programs/create.php', 'local/edu:admin'));
    //$ADMIN->add('programs', new admin_externalpage('programs_groups', 'Группы', $CFG->wwwroot . '/local/edu/programs/groups.php', 'local/edu:admin'));
    //$ADMIN->add('programs', new admin_externalpage('programs_samples', 'Шаблоны уведомлений', $CFG->wwwroot . '/local/edu/programs/samples.php', 'local/edu:admin'));
}
if ($hassiteconfig || has_capability('local/edu:manage', context_system::instance())) {
    $settings = new admin_settingpage('local_edu', 'Электронный деканат');
    $ADMIN->add('localplugins', $settings);
    $settings->add(new admin_setting_heading('local_edu_settings', 'Электронный деканат', 'Электронный деканат'));
    $settings->add(new admin_setting_configtextarea('local_edu/youtubecode', "Шаблон вставки кода видео YouTube", "", ''));
    $settings->add(new admin_setting_configtextarea('local_edu/videocode', "Шаблон вставки кода видео", "", ''));
    $settings->add(new admin_setting_configcheckbox('local_edu/showlib', "Включить электронную библиотеку", "", ''));

}