<?php

class EDropDown2
{
    private $elements = array();
    private $active = array();
    private $name = '';
    private $id = 0;

    function isEmpty()
    {
        return count($this->elements) == 0;
    }

    function __construct($name = '')
    {
        $this->name = $name;
        $this->id = rand(0, 999999);
    }

    function render($isdiv = true)
    {
        $html = '';
        foreach ($this->elements as $k => $el) {
            $html .= $el;
        }
        return html_writer::div(html_writer::link("#", $this->name, array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'id' => 'id' . $this->id, 'aria-haspopup' => 'true', 'aria-expanded' => 'false')) . html_writer::tag('div', $html, array('class' => 'dropdown-menu', 'aria-labelledby' => 'id' . $this->id)), 'dropdown');
    }

    function display()
    {
        echo $this->render();
    }


    function addDivider()
    {
        $this->elements[] = '<div class="dropdown-divider"></div>';
    }

    function addElement($link, $name, $active = false)
    {
        $this->elements[] = html_writer::link($link, $name, ['class' => 'dropdown-item']);
        $this->active[] = $active ? 'active' : '';
    }
}

class EDropDown
{
    private $elements = array();
    private $active = array();
    private $name = '';

    function isEmpty()
    {
        return count($this->elements) == 0;
    }

    function __construct($name = '')
    {
        $this->name = $name;
    }

    function render($isdiv = true)
    {
        $html = '';
        foreach ($this->elements as $k => $el) {
            if (empty($el))
                continue;
            elseif (is_object($el)) {
                $html .= html_writer::tag('li', $el->render(false), array('class' => 'dropdown-submenu'));

            } elseif ($el == 'divider') {
                $html .= html_writer::tag('li', '', array('class' => 'divider'));
            } else
                $html .= html_writer::tag('li', $el, array('class' => $this->active[$k]));
        }
        if ($isdiv)
            return html_writer::div(html_writer::link("#", $this->name, array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown')) . html_writer::tag('ul', $html, array('class' => 'dropdown-menu')), 'dropdown');
        else
            return html_writer::link("#", $this->name, array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown')) . html_writer::tag('ul', $html, array('class' => 'dropdown-menu'));
    }

    function display()
    {
        echo $this->render();
    }


    function addDivider()
    {
        $this->elements[] = 'divider';
    }

    function addSubMenu($menu, $active = false)
    {
        $this->elements[] = $menu;
        $this->active[] = $active ? 'active' : '';
    }

    function addElement($link, $name, $active = false)
    {
        $this->elements[] = html_writer::link($link, $name);
        $this->active[] = $active ? 'active' : '';
    }
}

class HTable
{
    private $class = '';
    private $tables = array();
    private $i = -1;


    function __construct($class = '')
    {
        $this->class = $class;
    }

    function addTR()
    {
        if ($this->i > -1)
            $this->tables[$this->i] .= html_writer::end_div();
        $this->i++;
        $this->tables[$this->i] = html_writer::start_div('div-table ' . $this->class);
    }

    function addTD($text, $class = '')
    {
        $this->tables[$this->i] .= html_writer::div($text, 'div-td ' . $class);
    }

    function render()
    {
        $this->tables[$this->i] .= html_writer::end_div();
        return implode('', $this->tables);
    }

    function display()
    {
        echo $this->render();
    }


}

class EduDialog
{
    private $class = '';
    private $name = '';
    private $title = '';
    private $content = '';
    private $id = '';
    private $openajax = '';
    public $width = "80%";
    private $confirm = '';

    static function addPage()
    {
        global $PAGE;
        $PAGE->requires->css(new moodle_url('/local/edu/helpers/dialog.css'));
        $PAGE->requires->js(new moodle_url('/local/edu/helpers/dialog.js'));
    }

    function setConfirm($url)
    {
        $this->confirm = $url;
    }

    function setAjax($url)
    {
        $this->openajax = $url;
    }

    function __construct($name, $title, $content, $class = '')
    {
        $this->name = $name;
        $this->content = $content;
        $this->class = $class;
        $this->title = $title;
        $this->id = 'dm' . rand(0, 5899888);
    }

    function render()
    {
        $html = html_writer::link("#", $this->name, array('class' => 'moodle-dialog-open', 'alt' => $this->id));
        $html .= html_writer::div($this->content, 'ui-dialog-moodle ' . $this->class, array('id' => $this->id, 'title' => $this->title,
            'data-open-ajax' => $this->openajax,
            'data-open-width' => $this->width,
            'data-url' => $this->confirm
        ));
        return $html;
    }

    function display()
    {
        echo $this->render();
    }

}

class EduAccordion
{
    private $class = '';
    private $active = 'active';
    private $lis = array();
    private $contents = array();

    function __construct($class = '')
    {
        $this->class = $class;
        $this->id = 'acc-' . rand(0, 10000);
    }

    static function addPage()
    {
        global $PAGE;
        //$PAGE->requires->css(new moodle_url('/local/edu/helpers/accordion.css'));
        //$PAGE->requires->js(new moodle_url('/local/edu/helpers/accordion.js'));
    }


    function add($header, $content)
    {
        $this->lis[] = $header;
        $this->contents[] = $content;
    }

    function renderold()
    {
        $html = '';
        foreach ($this->lis as $k => $li) {
            $html .= html_writer::tag('h3', $li);
            $html .= html_writer::div($this->contents[$k], 'active in');
        }
        return html_writer::div($html, 'ui-accordion-moodle ' . $this->class, array('data-active' => $this->active));
    }

    function display()
    {
        echo $this->render();
    }

    function render()
    {
        $html = '<div class="accordion ' . $this->class . '" id="' . $this->id . '">';
        foreach ($this->lis as $k => $li) {
            $html .= '<div class="card">
    <div class="card-header" id="headingOne">
      <h3 class="mb-0" data-toggle="collapse" data-target="#collapse' . $k . '" aria-expanded="true" aria-controls="collapse' . $k . '">
          ' . $li . '
      </h3>
    </div>';
            $html .= '<div id="collapse' . $k . '" class="collapse show in" aria-labelledby="headingOne" data-parent="#' . $this->id . '">
      <div class="card-body">' . $this->contents[$k] . '</div>
    </div>
  </div>';
        }
        $html .= '</div>';

        return $html;
    }
}

class BootsrapTabs
{
    private $id = 0;
    private $open = '';
    private $title = [];
    private $content = [];
    private $url = [];

    public function __construct()
    {
        $this->id = 'tab-' . rand(0, 10000);
    }

    function add($name, $title, $content, $active = false, $url = false)
    {
        $this->title[$name] = $title;
        $this->content[$name] = $content;
        $this->url[$name] = $url;
        if (count($this->title) == 1)
            $this->setOpen($name);
        if ($active)
            $this->setOpen($name);
    }

    function setOpen($name)
    {
        $this->open = $name;
    }

    function render()
    {

        $html = '<ul class="nav nav-tabs" id="' . $this->id . '" role="tablist">';
        foreach ($this->title as $name => $title) {
            $selected = $name == $this->open ? 'true' : 'false';
            $active = $name == $this->open ? 'active' : '';
            $html .= '<li class="nav-item">';
            if (empty($this->url[$name]))
                $html .= '<a class="nav-link ' . $active . '" id="' . $name . '-tab" data-toggle="tab" href="#' . $name . '" role="tab" aria-controls="' . $name . '" aria-selected="' . $selected . '">' . $title . '</a>';
            else
                $html .= '<a class="nav-link ' . $active . '" href="' . $this->url[$name] . '" role="tab" aria-controls="' . $name . '" aria-selected="' . $selected . '">' . $title . '</a>';
            $html .= '</li>';
        }
        $html .= '</ul><div class="tab-content" id="myTabContent' . $this->id . '">';
        foreach ($this->content as $name => $title) {
            $selected = $name == $this->open ? 'show active in' : '';
            $html .= '<div class="tab-pane fade ' . $selected . '" id="' . $name . '" role="tabpanel" aria-labelledby="' . $name . '-tab">' . $title . '</div>';
        }
        $html .= '</div>';
        return $html;
    }

}


class EduTab
{
    private $vertical = false;
    private $class = '';

    private $lis = array();
    private $contents = array();
    private $preid = '';

    function __construct($class = '', $preid = '')
    {
        $this->class = $class;
        $this->preid = $preid;
    }

    static function addPage()
    {
        global $PAGE;
        $PAGE->requires->css(new moodle_url('/local/edu/helpers/tabs.css'));
        $PAGE->requires->js(new moodle_url('/local/edu/helpers/tabs.js'));
    }

    function setVertical($value)
    {
        $this->vertical = $value;
    }

    function add($header, $content)
    {
        $count = count($this->lis);
        $this->lis[] = html_writer::tag('li', html_writer::link("#tab{$this->preid}-" . $count, $header));
        $this->contents[] = html_writer::div($content, '', array('id' => "tab{$this->preid}-" . $count));
    }

    function render()
    {
        $html = '';
        foreach ($this->lis as $li) $html .= $li;
        $html = html_writer::tag('ul', $html) . html_writer::start_div('contents-tabs');
        foreach ($this->contents as $div) $html .= $div;
        $html .= html_writer::end_div();
        if ($this->vertical) $this->class .= ' ui-tabs-vertical';
        return html_writer::div($html, 'ui-tab-moodle ' . $this->class);
    }

    function display()
    {
        echo $this->render();
    }
}


class EduDL
{
    private $dt = array();
    private $dd = array();
    private $class = '';

    function __construct($class = '')
    {
        $this->class = 'edu-dl ' . $class;
    }

    function add($dt, $dd)
    {
        $this->dt[] = $dt;
        $this->dd[] = $dd;
    }

    function render()
    {
        $html = '';
        foreach ($this->dt as $k => $dt) {
            $html .= html_writer::tag('dt', $dt);
            $html .= html_writer::tag('dd', $this->dd[$k]);
        }
        return html_writer::tag('dl', $html, array('class' => $this->class));
    }

    function display()
    {
        echo $this->render();
    }
}

class EduHTML
{
    static $pid;

    static function getTextChangeCourse($c)
    {
        if ($c->cmin && $c->cmax) {
            if ($c->cmin == $c->cmax) return "Вам необходимо выбрать {$c->cmin} дисциплин(у,ы) для изучения";
            return "Вам необходимо выбрать от {$c->cmin} до {$c->cmax} дисциплин(у,ы) для изучения";
        }
        if ($c->сmin)
            return "Вам необходимо выбрать хотя бы {$c->cmin} дисциплин(у,ы) для изучения";
        if ($c->cmax)
            return "Вы можете выбрать не более {$c->cmax} дисциплин(у,ы) для изучения";
        return "Вы можете выбрать дисциплины на выбор для изучения";
    }

    static function multiselect($name, $options, $noselect = false, $select = null, $attributes = array())
    {
        $attributes['name'] = $name;
        $html = html_writer::start_tag('select', $attributes);
        if ($noselect) $html .= html_writer::tag('option', $noselect, array('value' => 0));
        foreach ($options as $label => $selects) {
            $mas = '';
            foreach ($selects as $id => $name) {
                $options = array('value' => $id);
                if ($select && $select == $id)
                    $options['selected'] = 'selected';
                $mas .= html_writer::tag('option', $name, $options);
            }
            $html .= html_writer::tag('optgroup', $mas, array('label' => $label));
        }
        $html .= html_writer::end_tag('select');
        return $html;
    }

    static function printCourseEdit($id = null, $courseid = null, $type = 0, $moduleid = 0, $groupid = 0, $typemodule = 'module')
    {
        $link = $typemodule == 'module' ? $typemodule . "s" : $typemodule;
        global $CFG, $OUTPUT, $DB;
        $hours = '';
        $kz = '';
        $name = '';
        $cmin = 0;
        $cmax = 0;
        $itemid = 0;
        $itemtype = 0;
        $courseObject = null;
        if ($id) {
            $courseObject = $DB->get_record('edu_courses', array('id' => $id));
            $hours = $courseObject->hours;
            $kz = $courseObject->kz;
            $name = $courseObject->name;
            $cmin = $courseObject->cmin;
            $itemid = $courseObject->itemid;
            $cmax = $courseObject->cmax;
            $itemtype = $courseObject->itemtype;
        }

        $options = Edu::getCategoriesCoursesForSelect();
        $delimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'title' => 'Удалить дисциплину?', 'class' => 'iconsmall'));
        $moveimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/move'), 'title' => 'Перемещайте курсы, нажатем левой клавиши мыши', 'class' => 'iconsmall'));
        $editimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => 'Редактировать', 'class' => 'iconsmall'));
        $html = '';
        if (empty($type)) {
            $dl = new EduDL();
            $dl->add("Выберите курс", EduHTML::multiselect('courseid', $options, "Выберите курс...", $courseid));
            $dl->add("Количество часов", html_writer::empty_tag("input", array('type' => 'text', 'name' => 'hours', 'value' => $hours)));
            $dl->add("Название", html_writer::empty_tag("input", array('type' => 'text', 'name' => 'name', 'value' => $name)));
            $dl->add("Количество зачетных единиц", html_writer::empty_tag("input", array('type' => 'text', 'name' => 'kz', 'value' => $kz)));
            $dl->add("Тип аттестации", html_writer::select(itemGrade::getItemsAsMas(true), 'itemid', $itemid, false));
            $dl->add("Тип курса", html_writer::select(itemGrade::getItemsTypes(), 'itemtype', $itemtype, false));
            $html = html_writer::div(
                html_writer::div($dl->render(), 'div-td') .
                html_writer::div(
                    html_writer::span($moveimg, 'move-course') .
                    html_writer::link($CFG->wwwroot . '/local/edu/' . $link . '/index.php', $delimg, array('class' => 'del-course'))
                    , 'div-td managecourse', array('data-del' => 'course[:id]')
                )
                , 'div-table object-element', array('data-element' => 'course[:id]', 'data-id' => '[:id]'));
            if ($id) $html = str_replace("[:id]", $id, $html);
        } else {
            $courses_list = '';
            $dl = new EduDL();
            $dl->add('Мин. возможный выбор (0 - нет ограничений)', html_writer::empty_tag("input", array('type' => 'text', 'name' => 'cmin', 'value' => $cmin, 'size' => 2)));
            $dl->add('Макс. возможный выбор (0 - нет ограничений)', html_writer::empty_tag("input", array('type' => 'text', 'name' => 'cmax', 'value' => $cmax, 'size' => 2)));
            $courses_list .= $dl->render();
            if ($groupid && $moduleid) {

                if ($courses = $DB->get_records('edu_courses', array($typemodule . 'id' => $moduleid, 'groupid' => $groupid), "sortorder")) {
                    $clist = '';
                    $table = new html_table();
                    $table->head = array("Название", "Кол-во часов", "Аттестация");
                    $table->data = array();
                    foreach ($courses as $course) {
                        if (empty($course->courseid)) continue;
                        $hours = $course->hours ? $course->hours . " ч." : "";
                        $data = array();
                        $data[] = $DB->get_field('course', 'fullname', array('id' => $course->courseid));
                        $data[] = $hours;
                        $data[] = itemGrade::getName($course->itemid, '-');
                        $table->data[] = $data;
                        //$clist .= html_writer::tag('li',  . $hours);
                    }
                    $courses_list .= $table->data ? html_writer::table($table) : "";
                }
            }
            $edit = html_writer::link($CFG->wwwroot . '/local/edu/' . $link . '/courses.php?id=' . $moduleid . "&groupid=[:groupid]", $editimg);
            $html = html_writer::div(
                html_writer::div(html_writer::tag('p', "Дисциплины на выбор") . $courses_list, 'div-td') .
                html_writer::div(
                    html_writer::span($moveimg, 'move-course') .
                    $edit .
                    html_writer::link($CFG->wwwroot . '/local/edu/' . $link . 's/index.php', $delimg, array('class' => 'del-course')),
                    'div-td managecourse', array('data-del' => 'course[:id]')
                )
                , 'div-table object-element', array('data-element' => 'course[:id]', 'data-id' => '[:id]'));
            if ($id) $html = str_replace("[:id]", $id, $html);
            if ($groupid) $html = str_replace("[:groupid]", $groupid, $html);
        }
        return $html;
    }

    static function printElementEdit($id = null, $courseid = 0, $type = "Т", $moduleid = 0, $groupid = 0, $typemodule = 'eop')
    {
        global $CFG, $OUTPUT, $DB;
        $options = Edu::getCategoriesEopForSingleSelect();
        $delimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'title' => 'Удалить модуль?', 'class' => 'iconsmall'));
        $moveimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/move'), 'title' => 'Перемещайте курсы, нажатем левой клавиши мыши', 'class' => 'iconsmall'));
        $viewimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => 'Редактировать', 'class' => 'iconsmall'));
        //$viewimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/preview'), 'alt' => 'Просмотр', 'class' => 'iconsmall'));
        $html = '';

        $dl = new EduDL();

        $types = $DB->get_records_sql_menu("select id, name from {edu_types}");
        $dl->add(
            html_writer::select($types, 'type', $type, false),
            //EduHTML::multiselect('eopid', $options, "Выберите элемент образовательной программы...", $courseid)
            html_writer::empty_tag('input', array('eop-id' => isset($options[$courseid]) ? $courseid : '[:eopid]', 'class' => 'updateEopName', 'type' => 'text', 'placeholder' => 'Введите название элемента...', 'value' => isset($options[$courseid]) ? trim($options[$courseid]) : '[:eopname]'))
        //EduHTML::multiselect('eopid', $options, "Выберите элемент образовательной программы...", $courseid)
        );
        //$dl->add("Количество часов", html_writer::empty_tag("input", array('type' => 'text', 'name' => 'hours', 'value' => $hours)));
        $html = html_writer::div(
            html_writer::div($dl->render(), 'div-td') .
            html_writer::div(
                html_writer::span($moveimg, 'move-course') .
                html_writer::link($CFG->wwwroot . '/local/edu/' . $typemodule . 's/index.php', $delimg, array('class' => 'del-course')) .
                (html_writer::link("/local/edu/eop/courses.php?id=" . ($courseid ? $courseid : '[:eopid]'), $viewimg))
                , 'div-td managecourse', array('data-del' => 'course[:id]')
            )
            , 'div-table object-element', array('data-element' => 'course[:id]', 'data-id' => '[:id]'));
        if ($id) $html = str_replace("[:id]", $id, $html);
        return $html;
    }


    static function printModuleEdit($id = null, $courseid = null, $type = 0, $moduleid = 0, $groupid = 0, $typemodule = 'eop')
    {
        global $CFG, $OUTPUT, $DB;
        $hours = '';
        $cmin = 0;
        $cmax = 0;
        $itemid = 0;
        $courseObject = null;
        if ($id) {
            $courseObject = $DB->get_record('edu_courses', array('id' => $id));
            $hours = $courseObject->hours;
            $cmin = $courseObject->cmin;
            $cmax = $courseObject->cmax;
            $itemid = $courseObject->itemid;
        }

        $options = Edu::getCategoriesModulesForSelect();
        $delimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'title' => 'Удалить модуль?', 'class' => 'iconsmall'));
        $moveimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/move'), 'title' => 'Перемещайте курсы, нажатем левой клавиши мыши', 'class' => 'iconsmall'));
        $editimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => 'Редактировать', 'class' => 'iconsmall'));
        $viewimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/preview'), 'alt' => 'Просмотр', 'class' => 'iconsmall'));
        $html = '';
        if (empty($type)) {
            $dl = new EduDL();
            $dl->add("Выберите модуль", EduHTML::multiselect('courseid', $options, "Выберите модуль...", $courseid));
            $dl->add("Тип аттестации", html_writer::select(itemGrade::getItemsAsMas(true), 'itemid', $itemid, false));
            //$dl->add("Количество часов", html_writer::empty_tag("input", array('type' => 'text', 'name' => 'hours', 'value' => $hours)));
            $html = html_writer::div(
                html_writer::div($dl->render(), 'div-td') .
                html_writer::div(
                    html_writer::span($moveimg, 'move-course') .
                    html_writer::link($CFG->wwwroot . '/local/edu/' . $typemodule . 's/index.php', $delimg, array('class' => 'del-course'))
                    . ($courseid ? html_writer::link("/local/edu/modules/courses.php?id=" . $courseid, $viewimg) : '')
                    , 'div-td managecourse', array('data-del' => 'course[:id]')

                )
                , 'div-table object-element', array('data-element' => 'course[:id]', 'data-id' => '[:id]'));
            if ($id) $html = str_replace("[:id]", $id, $html);
        } else {
            $courses_list = '';
            $dl = new EduDL();
            $dl->add('Мин. возможный выбор (0 - нет ограничений)', html_writer::empty_tag("input", array('type' => 'text', 'name' => 'cmin', 'value' => $cmin, 'size' => 2)));
            $dl->add('Макс. возможный выбор (0 - нет ограничений)', html_writer::empty_tag("input", array('type' => 'text', 'name' => 'cmax', 'value' => $cmax, 'size' => 2)));
            $courses_list .= $dl->render();
            if ($groupid && $moduleid) {

                if ($courses = $DB->get_records('edu_courses', array($typemodule . 'id' => $moduleid, 'groupid' => $groupid), "sortorder")) {
                    $table = new html_table();
                    $table->head = array("Название", "Аттестация");
                    $table->data = array();
                    foreach ($courses as $course) {
                        if (empty($course->courseid)) continue;
                        $data = array();
                        $data[] = $DB->get_field('edu_modules', 'name', array('id' => $course->courseid));
                        $data[] = itemGrade::getName($course->itemid, '-');
                        $table->data[] = $data;
                    }
                    $courses_list .= $table->data ? html_writer::table($table) : "";
                }
            }
            $edit = html_writer::link($CFG->wwwroot . '/local/edu/' . $typemodule . '/courses.php?type=M&id=' . $moduleid . "&groupid=[:groupid]", $editimg);
            $html = html_writer::div(
                html_writer::div(html_writer::tag('p', "Модули на выбор") . $courses_list, 'div-td') .
                html_writer::div(
                    html_writer::span($moveimg, 'move-course') .
                    $edit .
                    html_writer::link($CFG->wwwroot . '/local/edu/' . $typemodule . 's/index.php', $delimg, array('class' => 'del-course')),
                    'div-td managecourse', array('data-del' => 'course[:id]')
                )
                , 'div-table object-element', array('data-element' => 'course[:id]', 'data-id' => '[:id]'));
            if ($id) $html = str_replace("[:id]", $id, $html);
            if ($groupid) $html = str_replace("[:groupid]", $groupid, $html);
        }
        return $html;
    }

    static function getColor($interval)
    {
        global $DB;
        $summ = 0;
        $num = 0;
        if ($ids = explode('/', $interval->type)) {
            $mas = array();
            foreach ($ids as $id) {
                if ($id == 0) $id = 3;
                if ($color = $DB->get_field('edu_types', 'color', array('id' => $id))) {
                    $summ += hexdec($color);
                    $num++;
                }
            }
        }
        $summ = round($summ / $num);
        return substr("000000" . dechex($summ), -6);
    }

    static function getNames($interval)
    {
        global $DB;
        $summ = 0;
        $num = 0;
        if ($ids = explode('/', $interval->type)) {
            $mas = array();
            foreach ($ids as $id) {
                if ($id == 0) $id = 3;
                if ($name = $DB->get_field('edu_types', 'name', array('id' => $id))) {
                    $mas[] = $name;
                }
            }
        }
        return $mas;
    }

    static function viewElementsASModule($modules, $courses)
    {
        $html = '';
        if (!$modules) return '';
        if (count($modules) < 2) {
            $accordion = new EduAccordion();
            $module = reset($modules);
            $module_name = $module->name;
            $module_content = '';
            if ($courses[$module->id]) {
                foreach ($courses[$module->id] as $cs) {
                    $module_content .= self::viewElementsASCourse($cs);

                }
            }
            $accordion->add($module_name, $module_content);
            $html = $accordion->render();

        } else {
            $c = reset($modules);
            $configtext = '';
            if ($c->cmin) $configtext .= " от " . $c->cmin;
            if ($c->cmax) $configtext .= " до " . $c->cmax;
            $configtext = $configtext ? " (" . $configtext . " модулей)" : $configtext;
            $html = html_writer::tag('h2', "Модули на выбор" . $configtext);
            $accordion = new EduAccordion();
            foreach ($modules as $module) {

                $module_name = $module->name;
                $module_content = '';
                if ($courses[$module->id]) {
                    foreach ($courses[$module->id] as $cs) {
                        $module_content .= self::viewElementsASCourse($cs);

                    }
                }
                $accordion->add($module_name, $module_content);
            }
            $html .= $accordion->render();
            $html = html_writer::div($html, 'asmodule');
        }
        return $html;
    }

    static function printCheckTeachers($pid, $lid, $teachers = '')
    {
        $html = '';
        $mas = explode(',', $teachers);
        if ($teachers = Edu::getTeachers($pid)) {
            $list = array();
            foreach ($teachers as $tid => $t) {
                $options = array('type' => 'checkbox', 'name' => $tid, 'data-lid' => $lid, 'id' => "t" . $lid . $tid);
                if (in_array($tid, $mas)) $options['checked'] = 'checked';
                $list[] = html_writer::empty_tag('input', $options) . html_writer::label($t, "t" . $lid . $tid);
            }
            $html = html_writer::alist($list, array('class' => 'teachers'));


        }
        return $html;
    }

    static function viewElementsASCourse($courses)
    {
        $html = '';
        if (!$courses) return $html;
        if (count($courses) < 2) {
            $course = reset($courses);
            $table = new html_table();
            $table->head = array("Название", "Кол-во часов", "Аттестация", "Преподаватели");
            $table->data = array(array(html_writer::link(new moodle_url("/course/view.php?id=" . $course->id), $course->fullname)
            , $course->hours, itemGrade::getName($course->itemid, '-'), self::printCheckTeachers(self::$pid, $course->lid, $course->teachers)));
            $html = html_writer::table($table);
        } else {
            $c = reset($courses);
            $configtext = '';
            if ($c->cmin) $configtext .= " от " . $c->cmin;
            if ($c->cmax) $configtext .= " до " . $c->cmax;
            $configtext = $configtext ? " (" . $configtext . " курсов)" : $configtext;
            $html = html_writer::tag('h4', "Дисциплины на выбор" . $configtext);
            $table = new html_table();
            $table->head = array("Название", "Кол-во часов", "Аттестация");
            $table->data = array();
            foreach ($courses as $course) {
                $data = array();
                $data[] = html_writer::link(new moodle_url("/course/view.php?id=" . $course->id), $course->fullname);
                $data[] = $course->hours;
                $data[] = itemGrade::getName($course->itemid, '-');
                $table->data[] = $data;
            }
            $html .= html_writer::table($table);
        }
        return html_writer::div($html, 'ascourse');
    }

    static function viewElements($elements)
    {
        $html = '';
        if ($elements) {
            foreach ($elements as $e) {
                if ($e->type == 'C') $html .= self::viewElementsASCourse($e->courses);
                if ($e->type == 'M') $html .= self::viewElementsASModule($e->modules, $e->courses);
            }
        }
        return $html;
    }

}




class BootsrapModal
{
    private $id = '';
    private $title = '';
    private $content = '';
    private $ajax = '';
    private $autoOpen = false;
    private $style = 'max-width: 800px;';
    private $showHeader = true;

    function __construct($id, $title, $content, $autoOpen = false)
    {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->autoOpen = $autoOpen;
    }

    function setStyle($style)
    {
        $this->style = $style;
    }
    static function renderOnlyTitle($id, $tag, $text, $options = [], $ajax = '', $open = false, $afterAjax = '')
    {
        $options['data-toggle'] = 'modal';
        $options['data-target'] = '#modalID' . $id;
        $options['data-ajax'] = $ajax;
        $options['data-afterajax'] = $afterAjax;
        $options['data-onlytitle'] = 'ok';
        if ($open)
            $options['data-open'] = 1;
        return html_writer::tag($tag, $text, $options);
    }

    function renderTitle($tag, $text, $options = [], $ajax = '', $open = false, $afterAjax = '')
    {
        $options['data-toggle'] = 'modal';
        $options['data-target'] = '#modalID' . $this->id;
        $options['data-ajax'] = $ajax;
        $options['data-afterajax'] = $afterAjax;
        if ($open)
            $options['data-open'] = 1;
        return html_writer::tag($tag, $text, $options);
    }

    function setHeader($f)
    {
        $this->showHeader = $f;
    }

    function renderWindow()
    {

        $style = $this->style ? 'style="' . $this->style . '"' : '';
        $html = '<div class="modal fade' . ($this->autoOpen ? ' autoOpen' : '') . '" id="modalID' . $this->id . '" tabindex="-1" role="dialog" aria-labelledby="helperModalLongTitle' . $this->id . '" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                    <div ' . $style . ' class="modal-dialog" role="document">
                      <div class="modal-content">';
        if($this->showHeader)

            $html .= '<div class="modal-header">
                          <h5 class="modal-title" id="helperModalLongTitle' . $this->id . '">' . $this->title . '</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>';
        $html .= '<div class="modal-body">
                          ' . $this->content . '
                        </div>
                        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
      </div>
                      </div>
                    </div>
                  </div>';
        return $html;
    }
}

?>