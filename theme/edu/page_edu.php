<?php


class page_edu extends moodle_page
{
    function __construct()
    {
        global $EDU, $USER, $SESSION, $CFG;

        //if(!isset($SESSION->EDU) || empty($SESSION->EDU))
        {
            $SESSION->EDU = new \local_edu\edu($USER->id);
        }
        $EDU = $SESSION->EDU;
        if($EDU->isedu) {
            $CFG->theme = 'edu';
            //$CFG->cachejs = false;
            //$CFG->themedesignermode = true;
        }
    }
}