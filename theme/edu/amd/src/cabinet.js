define(['jquery', 'theme_edu/scroll', 'theme_edu/collapsible', 'theme_edu/waves', 'theme_edu/tooltip'], function ($) {


    return {
        modal: function () {
            var url = '';
            $('[data-toggle="modal"][data-ajax]').click(function () {
                url = $(this).attr('data-ajax');
                var target = $(this).attr('data-target');
                $(".modal-body", $(target)).html('');
                Y.io(url, {
                    method: 'POST',
                    data: null,
                    on: {
                        success: function (tid, response) {
                            $(".modal-body", $(target)).html(response.responseText);
                        }
                    }
                });
            });
            $('.modal [data-ajax]').click(function () {
                var datastr = $('.modal form').serialize();

                var data = {field: $(this).attr('name')};
                Y.io(url + "&" + datastr, {
                    method: 'POST',
                    data: data
                });
            });
        },
        cabinet: function () {
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').bstooltip();
                if ($("#nav-drawer").length) {
                    var ps = new PerfectScrollbar('#nav-drawer');
                    $(".block_orgs").click(function () {
                        setTimeout(function () {
                            ps.update();
                        }, 501);

                    })
                }

                $('.program-menu-container h4').click(function () {
                    var parent = $(this).parent();
                    if($(parent).hasClass('open'))
                    {
                        $("> div", $(parent)).slideUp(300, function () {
                            $(parent).removeClass('open');
                        });
                    } else
                        $("> div", $(parent)).slideDown(300, function () {
                            $(parent).addClass('open');
                        });
                })
            })
        }

    };
});