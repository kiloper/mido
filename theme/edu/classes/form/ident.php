<?php

namespace theme_edu\form;
require_once("$CFG->libdir/formslib.php");
class ident extends \moodleform
{
    private $edu;
    function __construct(\local_edu\edu &$edu)
    {
        $this->edu = $edu;
        parent::__construct();
    }

    function definition()
    {
        global $DB, $USER;

        $mform = $this->_form; // Don't forget the underscore!
        $filemanager_options['accepted_types'] = array('*.jpg', '*.png', '*.jpeg');
        $filemanager_options['subdirs'] = false;
        $filemanager_options['maxbytes'] = 10 * 1024 * 1024;
        $ident = $this->edu->getIdent();
        $identtext = '';
        if($ident && $ident->status == -1)
        {
            $comment = '';
            if($ident->comment)
                $comment = '<p><b>Комментарий: </b>'.$ident->comment."</p>";
            $send = '';
            if($ident->adminid)
                $send = \html_writer::tag('p', \html_writer::link(new \moodle_url('/message/index.php?id=' . $ident->adminid), '<button type="button" class="btn btn-primary"><i class="fa fa-comment-o" aria-hidden="true"></i> Связаться с менеджером</button>'));
            $identtext = "<br/>".\html_writer::div("Ваша фотография отклонена. Попробуйте еще раз.", 'alert alert-danger').$comment.$send;
        }
        $mform->addElement('html',
            \html_writer::div(
                \html_writer::div('<img width="100%" src="/theme/edu/pix/ident.png"/>', 'col-5') .
                \html_writer::div(
                    \html_writer::div(fullname($USER) . ", Вам необходимо пройти идентификацию личности. Для этого приложите вашу фотографию с разворотом паспорта рядом с Вашим лицом", 'alert alert-info').$identtext
                    , 'col-7')
                , 'row')
        );

            $mform->addElement('hidden', 'id', 0);
            $mform->addElement('hidden', 'pid', required_param('pid', PARAM_INT));
            $mform->addElement('hidden', 'courseid', required_param('pid', PARAM_INT));
            $mform->addElement('hidden', 'to', 'ident');
        if(empty($ident) || $ident->status == -1) {
            $mform->addElement('filepicker', 'file', "Файл", null, $filemanager_options);
            $mform->addRule('file', null, 'required');
            $this->add_action_buttons();
        } else
        {
            if($ident->status == 0)
                $mform->addElement('html', \html_writer::div("Ваша фотография находится на обработке у менеджера", 'alert alert-warning'));
        }
    }
}

?>