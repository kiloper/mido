<?php

namespace theme_edu;


class sidenav
{
    private $headers = [];
    private $childrens = [];
    private $icons = [];
    private $counter;
    private $links = [];
    private $actives = [];
    private $active = '';
    private $contents = [];
    private $lines = [];
    private $counts = [];
    private $activedefault = false;
    private $infos = [];

    function __construct($active)
    {
        $this->active = $active;
        $this->counter = -1;
    }

    function setActive($num = false)
    {
        if($num === false) {
            $num = $this->counter;
            $this->activedefault = $num;
        }
        foreach ($this->actives as $k => $v)
            $this->actives[$k] = false;
        $this->actives[$num] = true;
    }

    function addHeader($name, $icon, $url = false, $active = false)
    {
        $this->counter++;
        $this->headers[$this->counter] = $name;
        $this->links[$this->counter] = $url;
        $this->actives[$this->counter] = $active == $this->active;
        $this->icons[$this->counter] = $icon;
        $this->childrens[$this->counter] = [];
        $this->contents[$this->counter] = '';
        $this->lines[$this->counter] = false;
        $this->counts[$this->counter] = 0;
        $this->infos[$this->counter] = false;
    }

    function addInfo($type = 'warning')
    {
        $this->infos[$this->counter] = \html_writer::span('<i class="fa fa-exclamation-circle" aria-hidden="true"></i>', $type);
    }

    function addLine()
    {
        $this->counter++;
        $this->headers[$this->counter] = '';
        $this->links[$this->counter] = '';
        $this->actives[$this->counter] = false;
        $this->icons[$this->counter] = '';
        $this->childrens[$this->counter] = [];
        $this->contents[$this->counter] = '';
        $this->lines[$this->counter] = true;
    }

    function addCount($num)
    {
        $this->counts[$this->counter] = $num;
    }

    function addContent($name, $icon = '')
    {
        $this->counter++;
        $this->headers[$this->counter] = '';
        $this->links[$this->counter] = '';
        $this->actives[$this->counter] = false;
        $this->icons[$this->counter] = $icon;
        $this->childrens[$this->counter] = [];
        $this->contents[$this->counter] = $name;
        $this->lines[$this->counter] = false;
    }
    function addLink($name, $link, $active)
    {
        $this->childrens[$this->counter][] = '<a href="'.$link.'" class="'.($active == $this->active ? 'active' : '').' waves-effect"><span>'.$name.'</span></a>';
        $this->actives[$this->counter] = $this->actives[$this->counter] || ($this->active == $active);
    }

    function render()
    {
        if($this->activedefault !== false)
            $this->setActive($this->activedefault);
        $html = '<div class="navy-blue-skin"><div class="side-nav"><ul class="collapsible collapsible-accordion">';
        foreach ($this->headers as $k => $header)
        {
            if(!empty($this->lines[$k]))
            {
                $html .= \html_writer::tag('li', '&nbsp;', ['class' => 'line']);
            }
            if(!empty($this->contents[$k]))
            {
                $icon = $this->icons[$k];
                $icon = $icon ? '<i class="fa fa-'.$this->icons[$k].'"></i>' : '';
                $html .= \html_writer::tag('li', $icon.$this->contents[$k], ['class' => 'content']);
                continue;
            }
            if(empty($this->links[$k]) && empty($this->childrens[$k])) continue;
            $counts = $this->counts[$k] ? \html_writer::span($this->counts[$k], 'count') : '';
            if($this->infos[$k])
                $counts = $this->infos[$k];
            $html .= '<li class="'.($this->actives[$k] ? 'active' : '').'"><a class="collapsible-header waves-effect arrow-r '.
                ($this->actives[$k] ? 'active' : '').'" '.
                ($this->links[$k] ? 'href="'.$this->links[$k].'"' : '').'><i class="fa fa-'.$this->icons[$k].
                '"></i>'.$header.($this->childrens[$k] ? '<i class="fa fa-angle-down rotate-icon"></i>' : '').($counts).'</a>';
            if($this->childrens[$k])
            {
                $d = $this->actives[$k] ? 'block' : 'none';
                $html .= '<div class="collapsible-body" style="display: '.$d.';">';
                $html .= \html_writer::alist($this->childrens[$k]);
                $html .= '</div>';
            }
            $html .= '</li>';
        }
        $html .= '</ul></div></div>';
        return $html;
    }
}

?>