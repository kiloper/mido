<?php

namespace theme_edu;
class flextable
{
    public $class;
    public $tds = [];
    public $num = 0;
    private $header = false;
    private $trclass = [];

    function __construct($num, $class = '', $header = false, $v = 't')
    {
        $this->num = $num;
        $this->class = $class." ".$v;
        $this->header = $header;
    }

    function add($text)
    {
        $this->tds[] = $text;
    }

    function setTrClass($class)
    {
        $this->trclass[] = $class;
    }


    function render()
    {
        $trclass = array_shift($this->trclass);
        $html = \html_writer::start_div('div-tr '.$trclass.' '.($this->header ? 'header' : ''));
        foreach ($this->tds as $k => $v)
        {
            $html .= \html_writer::div($v, 'div-td c'.(($k % $this->num)));
            if(($k + 1) % $this->num == 0)
            {
                $html .= \html_writer::end_div();
                $trclass = array_shift($this->trclass);
                $html .= \html_writer::start_div('div-tr '.$trclass);
            }
        }
        $html .= \html_writer::end_div();
        return \html_writer::div($html, 'div-table '.$this->class);
    }
}

?>