<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course renderer.
 *
 * @package    theme_noanme
 * @copyright  2016 Frédéric Massart - FMCorz.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace theme_edu\output\core;
defined('MOODLE_INTERNAL') || die();

use local_edu\edu;
use local_edu\forum;
use moodle_url;
use html_writer;
use lang_string;
use theme_edu\flextable;
use theme_edu\form\ident;
use theme_edu\modal;
use tool_httpsreplace\form;

require_once($CFG->dirroot . '/course/renderer.php');

/**
 * Course renderer clasautos.
 *
 * @package    theme_noanme
 * @copyright  2016 Frédéric Massart - FMCorz.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class course_renderer extends \core_course_renderer
{

    /**
     * Renders html to display a course search form.
     *
     * @param string $value default value to populate the search field
     * @param string $format display format - 'plain' (default), 'short' or 'navbar'
     * @return string
     */
    function frontpage_available_courses()
    {
        return parent::frontpage_available_courses();
    }

    function printProgramHeader($program)
    {
        $tableP = new flextable(2, 'p-info', false, 'm');
        $tableP->add(html_writer::tag('h4', $program->name));
        $tableP->add(html_writer::span(
            'с ' . date('d.m.Y', $program->timestart) . ' до ' . date('d.m.Y', $program->timestart + $program->duration * 3600 * 24) . ' ', 'p-dates',
            ['title' => 'Срок обучения', 'data-toggle' => 'tooltip']));
        return $tableP->render();
    }

    function printIdent(edu &$edu)
    {
        global $USER, $DB;
        $ident = $edu->getIdent();
        if ($ident->status == 1) {
            return html_writer::div("Идентификация личности пройдена", 'alert alert-success');
        }
        $form = new ident($edu);
        if ($ident) {

            $draftitemid = file_get_submitted_draft_itemid('file');
            file_prepare_draft_area($draftitemid, 1, 'local_edu', 'ident', $ident->id, []);
            if ($ident->status != -1)
                $ident->file = $draftitemid;
            $form->set_data($ident);
        }
        if ($data = $form->get_data()) {
            $data->status = 0;
            if (empty($data->id)) {
                $data->userid = $USER->id;
                $data->timecreated = time();
                $data->id = $DB->insert_record('edu_ident', $data);
            } else {
                $data->timecreated = time();
                $DB->update_record('edu_ident', $data);
            }
            if ($data->file) {
                file_save_draft_area_files($data->file, 1, 'local_edu', 'ident', $data->id, array('subdirs' => false));
            }
            redirect('/');

        }
        if ($form->is_cancelled())
            redirect('/');
        return $form->render();
    }

    function printLib(edu &$edu)
    {
        return \local_edu\library::render();
    }

    function printNews(edu &$edu)
    {
        global $OUTPUT;
        $news = edu::getNews($edu->pid);
        $id = optional_param('id', 0, PARAM_INT);
        $html = '';
        if (empty($id)) {
            foreach ($news as $n) {
                $html .= html_writer::div(
                    html_writer::tag('h4', $n->name) .
                    html_writer::div(shorten_text(strip_tags($n->content), 500, false, html_writer::tag('p', html_writer::link("/?pid={$edu->pid}&to=news&id={$n->id}", "Читать..."))))
                    , 'program-news');
            }
        } else {
            $n = $news[$id];
            $html = html_writer::div(
                html_writer::tag('h4', $n->name) .
                file_rewrite_pluginfile_urls($n->content, 'pluginfile.php', 1, 'local_edu', 'info', $n->id),
                'program-new');
        }
        return $html;
    }

    function printFiles(edu &$edu)
    {
        global $OUTPUT;
        $systemcontext = \context_system::instance();
        $fs = get_file_storage();
        $files = $fs->get_area_files(1, 'local_edu', 'programs', $edu->pid);
        $i = 0;
        $table = new \html_table();
        $table->caption = "Файлы образовательной программы";
        $table->data = [];
        $p = $edu->getProgram();
        $desc = explode("\n", $p->filedesc);
        foreach ($files as $file) {

            $name = $file->get_filename();

            if ($name == '.') continue;
            if ($name == '..') continue;

            $url = (string)\moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename(), true);
            $table->data[] = [
                html_writer::link($url,
                    html_writer::img($OUTPUT->image_url(file_extension_icon($name, 24))->out(false), $name) . " " . $name) . html_writer::span("(" . round($file->get_filesize() / 1024) . " Кбайт)", 'file-size')
                , $desc[$i]];
            $i++;
        }
        return html_writer::table($table);
    }


    function printGrades(edu &$edu)
    {
        $program = $edu->getProgram();
        $table = new flextable(6, 'grades', true, 'm');
        $table->add('№');
        $table->add('Дисциплина');
        $table->add('Кол-во часов');
        $table->add('Форма аттестации');
        $table->add('Оценка');
        $table->add('Результат');
        $courses = $edu->getCourses($program->id);
        $i = 0;
        foreach ($courses as $c) {
            $i++;
            list($grade, $strgrade) = $edu->getGrade($c->id);
            $table->add($i);
            $table->add($c->name ? $c->name : $c->fullname);
            $table->add($c->hours ? $c->hours : '-');
            $table->add($c->control);
            $table->add($grade);
            $table->add($strgrade);
        }
        return $this->printProgramHeader($program) . $table->render();
    }

    function getIdentsForAdmin(edu &$edu)
    {
        global $OUTPUT;
        $myprograms = $edu->isProgramsForManager();
        $users = edu::getUsersByProgramsIdent(array_keys($myprograms), [0, -1]);
        $groups = [0 => 'Выберите группу...'];
        $groupid = optional_param('groupid', 0, PARAM_INT);
        $spid = optional_param('spid', 0, PARAM_INT);
        $table = new \html_table();
        $table->head = ["ФИО", 'Программа', 'Группа', "Начало обучения", ''];
        $table->data = [];
        $programs = [0 => 'Выберите программу...'];

        $modal = new modal("Статус идентификации", '', 'identStatus');

        foreach ($users as $u) {
            $groups[$u->gid] = $u->gname;
            $programs[$u->pid] = $u->pname;
            if ($groupid && $u->gid != $groupid) continue;
            if ($spid && $u->pid != $spid) continue;
            $data = [];
            $data[] = html_writer::link('/user/profile.php?id=' . $u->userid, fullname($u));
            $data[] = $u->pname;
            $data[] = $u->gname;
            $modal->setAjax('/local/edu/ajax.php?action=ident&userid=' . $u->userid . "&sesskey=" . sesskey());
            $data[] = date('d.m.Y', $u->timestart);
            $status = html_writer::span("Ожидает подтверждения");
            if ($u->istatus == -1)
                $status = html_writer::span("Отклонено", '', ['style' => 'color: red']);
            $data[] = $modal->link($status);
            $table->data[] = $data;
        }
        $modal->addButton('ok', 'Одобрить', true, true, 'success');
        $modal->addButton('cancel', 'Отклонить', true, true, 'danger');
        $modal->addButton('close', 'Закрыть', true, false);
        $selectG = $OUTPUT->single_select('/?to=users&spid=' . $spid, 'groupid', $groups, $groupid, null);
        $selectP = $OUTPUT->single_select('/?to=users&groupid=' . $groupid, 'spid', $programs, $spid, null);
        return html_writer::tag('h4', "Ожидающие идентификацию") .
            $selectG . "&nbsp;" .
            $selectP . "<br/><br/>" .
            html_writer::table($table) . $modal->render();

    }

    function getUserForAdmin(edu &$edu)
    {
        global $OUTPUT;
        $myprograms = $edu->isProgramsForManager();
        $users = edu::getUsersByPrograms(array_keys($myprograms));
        $groups = [0 => 'Выберите группу...'];
        $groupid = optional_param('groupid', 0, PARAM_INT);
        $spid = optional_param('spid', 0, PARAM_INT);
        $table = new \html_table();
        $table->head = ["ФИО", 'Программа', 'Группа', "Начало обучения", 'Идентификация'];
        $table->data = [];
        $programs = [0 => 'Выберите программу...'];
        $modal = new modal("Статус идентификации", '', 'identStatus');
        foreach ($users as $u) {
            $modal->setAjax('/local/edu/ajax.php?read=1&action=ident&userid=' . $u->userid . "&sesskey=" . sesskey());
            $groups[$u->gid] = $u->gname;
            $programs[$u->pid] = $u->pname;
            if ($groupid && $u->gid != $groupid) continue;
            if ($spid && $u->pid != $spid) continue;
            $data = [];
            $data[] = html_writer::link('/user/profile.php?id=' . $u->userid, fullname($u));
            $data[] = $u->pname;
            $data[] = $u->gname;
            $data[] = date('d.m.Y', $u->timestart);
            $ident = '';
            if ($u->istatus !== null) {
                $status = html_writer::span("Ожидает подтверждения");
                if ($u->istatus == -1)
                    $status = html_writer::span("Отклонено", '', ['style' => 'color: red']);
                if ($u->istatus == 1)
                    $status = html_writer::span("Одобрено", '', ['style' => 'color: green']);
                $ident = $modal->link($status);
            } else
                $ident = 'Не отправлено';
            $data[] = $ident;
            $table->data[] = $data;
        }
        $modal->addButton('close', 'Закрыть', true, false);
        $selectG = $OUTPUT->single_select('/?to=users&spid=' . $spid, 'groupid', $groups, $groupid, null);
        $selectP = $OUTPUT->single_select('/?to=users&groupid=' . $groupid, 'spid', $programs, $spid, null);
        return html_writer::tag('h4', "Слушатели") .
            $selectG . "&nbsp;" .
            $selectP . "<br/><br/>" .
            html_writer::table($table) . $modal->render();
    }

    function printForums(edu &$edu)
    {
        global $DB;
        $subs = optional_param('subs', 0, PARAM_INT);
        $unsubs = optional_param('unsubs', 0, PARAM_INT);
        if ($subs) {
            if (!$DB->record_exists('forum_subscriptions', ['userid' => $edu->userid, 'forum' => $subs])) {
                $ob = new \stdClass();
                $ob->userid = $edu->userid;
                $ob->forum = $subs;
                $DB->insert_record('forum_subscriptions', $ob);
            }
        }
        if ($unsubs) {
            $DB->delete_records('forum_subscriptions', ['userid' => $edu->userid, 'forum' => $unsubs]);
        }
        $table = new flextable(3, 'plan forums', true);
        $table->setTrClass('');
        $table->add('Форумы');
        $table->add('Кол-во новых сообщений');
        $table->add('Уведомлять по e-mail?');
        $forum = new forum($edu->userid);
        if($ps = $edu->isProgramsForManager())
        {
            $forums = [];
            foreach ($ps as $p) {
                $forums = $forums + $forum->getForums($p->id);
            }
        }
        elseif ($edu->isTeacher()) {
            list($forums, $num) = $forum->getForumsByCourses($edu->getCoursesForRole(3));
        } else
            $forums = $forum->getForums($edu->pid);
        foreach ($forums as $el => $fs) {
            $mas = [];

            if (substr($el, 0, 1) == 'p')
                $name = "Форум по программе";
            else
                $name = "Форум по дисциплиние - \"" . $DB->get_field('course', 'fullname', ['id' => $el]) . "\"";
            $table->setTrClass('forum-head');
            $table->add($name);
            $table->add('');
            $table->add('');
            foreach ($fs as $f) {

                $table->setTrClass('forum-el');
                $table->add(html_writer::link('/mod/forum/view.php?id=' . $f->cmid, $f->name));
                $table->add(html_writer::span($forum->getNoReadForums($f->id), 'label label-info'));
                if ($f->forcesubscribe == 0) {
                    if ($DB->record_exists('forum_subscriptions', ['userid' => $edu->userid, 'forum' => $f->id]))
                        $table->add(html_writer::link("/?pid={$edu->pid}&to=forums&unsubs=" . $f->id, "Отписаться") . html_writer::tag('p', 'Вы записаны на данный форум', ['class' => 'subs-info']));
                    else
                        $table->add(html_writer::link("/?pid={$edu->pid}&to=forums&subs=" . $f->id, "Подписаться") . html_writer::tag('p', 'Сейчас Вы не получаете уведомлений с данного форума', ['class' => 'subs-info']));
                } else
                    $table->add('');
            }


        }
        return $table->render();
    }

    function printProgram(edu &$edu)
    {
        global $OUTPUT, $PAGE;
        $program = $edu->getProgram();


        $table = new flextable(7, 'plan', true);
        $i = 0;
        $courses = $edu->getCourses($program->id);
        list($colors, $comments, $info, $completed, $access) = $edu->getInfoByCourses($program->id);

        $table->setTrClass('');
        $table->add('');
        $table->add('Наименование дисциплины');
        $table->add('');
        $table->add('Преподаватель');
        $table->add('Кол-во часов');
        $table->add('Форма аттестации');
        $table->add('Оценка');
        foreach ($courses as $c) {
            list($grade, $strgrade) = $edu->getGrade($c->id);
            $teachers = $edu->getTeachers($c->id);
            $teacher = '';
            foreach ($teachers as $t) {
                $tt = new flextable(2, 'teacher-table', false, 'm');
                $pic = new \user_picture($t);
                $pic->size = 100;
                $modal = new \theme_edu\modal(fullname($t),
                    html_writer::div(
                        html_writer::img($pic->get_url($PAGE), fullname($t)) .
                        html_writer::link(new moodle_url('/message/index.php?id=' . $t->id), html_writer::tag('i', '', ['class' => 'fa fa-comments']) . " Написать преподавателю") .
                        html_writer::div($t->description, 'description'), 'teacher-info'
                    )
                    , 'teacher-' . $c->id . '-' . $t->id);
                $pic->size = 35;
                $modal->setOk(false);
                //$tt->add($OUTPUT->user_picture($t, array('size' => 30)));
                $tt->add($modal->link(html_writer::img($pic->get_url($PAGE), fullname($t))) . $modal->render());
                $tt->add($modal->link(fullname($t)));
                $teacher = $tt->render();
                break;
            }
            $i++;
            $class = '';
            if ($i < count($courses)) {
                $class = $colors[$i - 1] . "-" . $colors[$i];
                $table->add(html_writer::div('', 'liner') .
                    html_writer::div('', 'point', [
                        'data-toggle' => 'tooltip',
                        'title' => $comments[$i - 1],
                        'data-placement' => 'right'
                    ]));
            } else {
                $class = $colors[$i - 1];
                $table->add(html_writer::div('', 'point', [
                    'data-toggle' => 'tooltip',
                    'title' => $comments[$i - 1],
                    'data-placement' => 'right'
                ]));
            }
            if ($completed[$i - 1]) $class .= ' iscompleted';
            if (!$access[$i - 1]) $class .= ' noaccess';
            $table->setTrClass($class);
            $content = html_writer::div($c->name ? $c->name : $c->fullname, 'course-name');
            $info1 = '';
            foreach ($info[$i - 1] as $in) {
                $info1 .= html_writer::tag('li', html_writer::tag('i', '', ['class' => 'fa fa-hand-o-right']) . $in->text, ['class' => $in->status]);
            }
            $content .= html_writer::div(html_writer::tag('ul', $info1), 'info');
            $table->add($content);
            $link = html_writer::link(new moodle_url('/course/view.php?id=' . $c->id), 'Войти', ['class' => 'btn btn-success']);
            if (!$access[$i - 1])
                $link = html_writer::tag('button', 'Войти', ['class' => 'btn btn-secondary', 'type' => 'button']);;
            $table->add($link);

            $table->add($teacher);
            $table->add($c->hours ? $c->hours : '-');
            $table->add($c->control);
            $table->add($strgrade);
        }
        return $this->printProgramHeader($program) . $table->render();
    }

    function cabinet(edu &$edu)
    {
        $to = optional_param('to', '', PARAM_TEXT);
        if ($edu->isEdu() && $edu->programs) {
            $html = '';
            switch ($to) {
                case 'ident':
                    $html = $this->printIdent($edu);
                    break;
                case 'lib':
                    $html = $this->printLib($edu);
                    break;
                case 'news':
                    $html = $this->printNews($edu);
                    break;
                case 'files':
                    $html = $this->printFiles($edu);
                    break;
                case 'forums':
                    $html = $this->printForums($edu);
                    break;
                case 'grades':
                    $html = $this->printGrades($edu);
                    break;
                default:
                    $html = $this->printProgram($edu);
                    break;
            }
            return $html;
        } elseif ($edu->isTeacher() || $edu->isProgramsForManager()) {
            switch ($to) {
                case 'users':
                    return self::getUserForAdmin($edu);
                    break;
                case 'identadmin':
                    return self::getIdentsForAdmin($edu);
                    break;
                case 'forums':
                    return $this->printForums($edu);
                    break;
                case 'tasks':
                    $assigns = $edu->getNoGradesAssignData();
                    return self::noGradeAssignTable($assigns);
                    break;

                default:
                    return parent::frontpage_my_courses();
                    break;
            }
        }

        return parent::frontpage_my_courses();
    }

    static function noGradeAssignTable($assigns)
    {
        global $DB, $OUTPUT, $CFG;
        $html = '';
        $table = new \html_table();
        $table->head = array('Название', 'Автор', 'Время ответа', 'Программа', 'Дисциплина', '', '');
        $table->data = array();
        foreach ($assigns as $a) {

            $row = [];
            $row[] = html_writer::link("/mod/assign/view.php?id=" . $a->cmid, $DB->get_field('assign', 'name', array('id' => $a->assignment)));
            $row[] = fullname($DB->get_record('user', array('id' => $a->userid)));
            $row[] = userdate($a->timemodified);
            $row[] = !empty($a->pname) ? $a->pname : "";
            $row[] = $a->coursename;

            $groupincourse = null;
            if (!empty($a->pid)) {
                $groupincourse = groups_get_group_by_idnumber($a->courseid, $a->pid);
            }
            $gradeurl = new moodle_url('/mod/assign/view.php');
            $gradeurl->param('id', $a->cmid);
            $gradeurl->param('action', 'grading');
            if ($groupincourse)
                $gradeurl->param('group', $groupincourse->id);

            $row[] = html_writer::link($gradeurl, "Оценить");

            $del = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'title' => 'Удалить работу студента', 'class' => 'iconsmall'));
            $delete = html_writer::link($CFG->wwwroot . '/theme/sdo/ajax.php?action=deleteStudentsWork&cmid=' . $a->cmid . '&id=' . $a->id, $del, array('class' => 'urlAjax', 'confirm' => 'Удалить работу?', 'redirect' => '/?uc=2'));


            $row[] = html_writer::link('#', $delete);

            $row = new \html_table_row($row);

            $delta = time() - $a->timemodified;
            $class = "nogradeassign-";
            if ($delta >= 36 * 60 * 60) {
                $class .= "red";
            }
            if (($delta >= 24 * 60 * 60) && ($delta < 36 * 60 * 60)) {
                $class .= "yellow";
            }
            if ($delta < 24 * 60 * 60) {
                $class .= "green";
            }

            $row->attributes = ['class' => $class];
            $table->data[] = $row;
        }
        $html .= html_writer::table($table);
        return $html;
    }

    function frontpage_my_courses()
    {
        global $EDU;
        return $this->cabinet($EDU);
    }


}
