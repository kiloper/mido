<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace theme_edu\output;
defined('MOODLE_INTERNAL') || die;

use html_writer;
use core_user;
use local_edu\edu;
use local_edu\forum;
use moodle_url;
use theme_edu\sidenav;

/**
 * Renderers to align Moodle's HTML with that expected by Bootstrap
 *
 * @package    theme_boost
 * @copyright  2012 Bas Brands, www.basbrands.nl
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class core_renderer extends \theme_boost\output\core_renderer
{

    function mainmenu(edu &$edu)
    {
        global $DB, $PAGE;
        $to = optional_param('to', '', PARAM_TEXT);
        $isTeacher = $edu->isTeacher();
        $programsManager = $edu->isProgramsForManager();
        if ($edu->isEdu() && $edu->programs) {
            $actlink = $to . '-' . $edu->pid;
            if (isset($PAGE->cm) && $PAGE->cm->id)
                $actlink = $PAGE->cm->id;
            $sidenav = new sidenav($actlink);
            $sidenav->addHeader("Подтверждение личности", 'user-circle-o', '/?to=ident&pid=' . $edu->pid . "&courseid=0", 'ident-' . $edu->pid);
            $ident = $edu->getIdent();
            if ($ident && ($ident->status == 1))
                $sidenav->addInfo('success');
            if ($ident && ($ident->status == -1))
                $sidenav->addInfo('danger');
            if ($ident && ($ident->status == 0))
                $sidenav->addInfo();
            $sidenav->addContent("Мои программы", 'leanpub');
            $forums = new forum($edu->userid);
            foreach ($edu->programs as $p) {
                $isCurrent = $edu->getProgram()->id == $p->id;
                $sidenav->addHeader($p->name, 'user');
                $sidenav->addLink("Учебный план", new moodle_url('/', ['pid' => $p->id]), '-' . $p->id);
                $sidenav->addLink("Зачетная книжка", new moodle_url('/', ['to' => 'grades', 'pid' => $p->id]), 'grades-' . $p->id);
                if (edu::getFiles($p->id))
                    $sidenav->addLink("Файлы", new moodle_url('/', ['to' => 'files', 'pid' => $p->id]), 'files-' . $p->id);
                if (edu::getNews($p->id))
                    $sidenav->addLink("Новости", new moodle_url('/', ['to' => 'news', 'pid' => $p->id]), 'news-' . $p->id);
                if ($forums->getForums($p->id)) {
                    $count = $forums->getAllNoReadForums($p->id);
                    $count = $count ? \html_writer::div($count, 'counter') : '';
                    $sidenav->addLink("Форумы" . $count, new moodle_url('/', ['to' => 'forums', 'pid' => $p->id]), 'forums-' . $p->id);

                }


            }
            $menu = $DB->get_records('edu_menu');
            foreach ($menu as $m) {
                $sidenav->addHeader($m->name, $m->icon, $m->url, $PAGE->cm->id ? $PAGE->cm->id : false);
            }
            if(get_config('local_edu', 'showlib'))
            {
                $sidenav->addHeader("Электронная библиотека", 'book', '/?to=lib', 'lib-' . $edu->pid);
            }
            return $sidenav->render();
        } elseif ($isTeacher || $programsManager) {

            $sidenav = new sidenav($to . '-' . $edu->pid);
            $forum = new forum($edu->userid);
            if ($isTeacher) {
                list($courses, $nume) = $forum->getForumsByCourses($edu->getCoursesForRole(3));
                $sidenav->addHeader("Мои курсы", 'leanpub', '/', 't-' . $edu->pid);
                $assigns = $edu->getNoGradesAssignData();
                $sidenav->addHeader("Непроверенные работы", 'tasks', '/?to=tasks', 'taskst' . '-' . $edu->pid);

                if ($assigns)
                    $sidenav->addCount(count($assigns));
                $sidenav->addHeader("Форумы", 'question', '/?to=forums', 'forumst' . '-' . $edu->pid);
                $sidenav->addCount($nume);

            }
            if ($programsManager) {
                $sidenav->addHeader("Идентификация слушателей", 'tasks', '/?to=identadmin', 'identadmin' . '-' . $edu->pid);
                $sidenav->addCount(count(edu::getUsersByProgramsIdent(array_keys($programsManager), [0, -1])));
                $sidenav->addHeader("Слушатели", 'users', '/?to=users', 'users' . '-' . $edu->pid);
                $forums = [];
                $num = 0;
                foreach ($programsManager as $p) {
                    $forums = $forums + $forum->getForums($p->id);
                    $num += $forum->getAllNoReadForums($p->id);
                }
                if($forums) {
                    $sidenav->addHeader("Форумы", 'question', '/?to=forums', 'forumst' . '-' . $edu->pid);
                    $sidenav->addCount($num);
                }
            }
            return $sidenav->render();
        }

        return '';
    }

    function coursesidenav()
    {
        global $COURSE, $PAGE, $DB, $USER, $EDU;
        if ($COURSE->id > 1) {
            $fm = get_fast_modinfo($COURSE->id);
            $sections = $fm->get_section_info_all();
            $coursenav = new \theme_edu\sidenav(isset($PAGE->cm) ? $PAGE->cm->id : 0);
            $coursenav->addHeader("Вернуться в личный кабинет", 'angle-left', '/', 10000);
            $coursenav->addLine();
            $coursenav->addHeader(\html_writer::tag('b', "Оценки курса"), 'bar-chart', new moodle_url('/grade/report/user/index.php?id=' . $COURSE->id), -90);
            if ($PAGE->bodyid == 'page-grade-report-user-index')
                $coursenav->setActive();
            $coursenav->addHeader(\html_writer::tag('b', "Содержание курса"), 'list', new moodle_url('/course/view.php?id=' . $COURSE->id), -90);
            foreach ($sections as $section) {
                if ($section->uservisible) {
                    $coursenav->addHeader($section->name ? $section->name : $COURSE->shortname, 'arrow-circle-right', false, $section->section == 0 ? 0 : -1);
                    $cmsids = explode(',', $section->sequence);
                    foreach ($cmsids as $cmid) {
                        if ($cmid) {
                            $cm = $fm->get_cm($cmid);
                            if ($cm->uservisible) {
                                $coursenav->addLink($cm->name, $cm->url, $cm->id);
                            }
                        }
                    }
                }
            }
            return $coursenav->render();
        } elseif ($USER->id > 1) {
            if ($c = $this->block_cabinet()) {
                $html = html_writer::div($c, 'block block_cabinet', ['id' => 'block_cabinet']);
                $html .= $this->mainmenu($EDU);
                return $html;
            }
        }
        return '';
    }

    function block_cabinet()
    {
        /**/
        global $OUTPUT, $USER;
        if ($USER->id > 1) {
            $viewuser = optional_param('viewuser', $USER->id, PARAM_INT);
            //if($viewuser != $USER->id) require_capability('local/edu:viewothercabinet', context_system::instance());
            $viewuser = core_user::get_user($viewuser);
            $block = new \block_contents();
            $block->attributes['id'] = 'block_cabinet';
            $block->title = "Кабинет";
            $html = $OUTPUT->user_picture($viewuser, array('size' => '150', 'class' => 'userfoto'));
            $html .= html_writer::tag('h2', fullname($USER));
            $links = array();
            $links[] = html_writer::link(new moodle_url('/user/profile.php?id=' . $USER->id), html_writer::img($OUTPUT->pix_url("profile", 'theme'), 'profile'), ['title' => 'Профиль']);
            $links[] = html_writer::link(new moodle_url('/message/'), html_writer::img($OUTPUT->pix_url("message", 'theme'), 'message'), ['title' => 'Сообщения']);
            $links[] = html_writer::link(new moodle_url("/login/logout.php?sesskey=" . sesskey()), html_writer::img($OUTPUT->pix_url("exit", 'theme'), 'exit'), ['title' => 'Выход']);


            $html .= html_writer::alist($links, array('class' => 'user-icons'));
            return $html;
        }
        return '';
        /**/
    }

    public function full_header()
    {
        global $PAGE, $COURSE, $OUTPUT, $EDU;
        if ($COURSE->id > 1) {
            $header = new \stdClass();
            $header->coursename = $COURSE->fullname;
            $header->summary = \html_writer::div(shorten_text($COURSE->summary, 2500, false, '... '), 'more');

            $ts = $EDU->getTeachers($COURSE->id);
            if ($forum = forum::getForumForProgram($EDU->pid)) {
                if ($forum->id == $COURSE->id) return parent::full_header();
            }
            $header->teachers = [];
            $header->teacherhtml = '';
            $header->teachermodal = '';
            foreach ($ts as $t) {
                $t->fullname = fullname($t);
                $pic = new \user_picture($t);
                $t->foto = $pic->get_url($PAGE);
                $header->teachers[] = $t;

                $pic = new \user_picture($t);
                $pic->size = 100;
                $modal = new \theme_edu\modal(fullname($t),
                    html_writer::div(
                        html_writer::img($pic->get_url($PAGE), fullname($t)) .
                        html_writer::link(new moodle_url('/message/index.php?id=' . $t->id), html_writer::tag('i', '', ['class' => 'fa fa-comments']) . " Написать преподавателю") .
                        html_writer::div($t->description, 'description'), 'teacher-info'
                    )
                    , 'teacher-' . $COURSE->id . '-' . $t->id);
                $pic->size = 35;
                $modal->setOk(false);
                //$tt->add($OUTPUT->user_picture($t, array('size' => 30)));
                $header->teacherhtml .= '<div class="teacher">
                            ' . $modal->link('<div class="foto" style="background-image: url(' . $pic->get_url($PAGE) . ');"></div>') . '
                            <div class="name">' . $modal->link(fullname($t)) . '</div></div>';
                $header->teachermodal .= $modal->render();
//                $tt->add($modal->link(html_writer::img($pic->get_url($PAGE), fullname($t))).$modal->render());
//                $tt->add($modal->link(fullname($t)));


            }
            $pc = $EDU->getCourses($EDU->pid);
            $header->control = isset($pc[$COURSE->id]) ? $pc[$COURSE->id]->control : '';
            $header->courseimage = edu::getCourseImage($COURSE->id);
            $header->settingsmenu = $this->context_header_settings_menu();
            list($header->grade, $header->str_grade) = $EDU->getGrade($COURSE->id);
            $header->grade = $header->grade ? $header->grade : 0;
            $header->str_grade = html_writer::link('/grade/report/user/index.php?id=' . $COURSE->id, $header->str_grade);
            return $this->render_from_template('theme_edu/courseheader', $header);
        }

        return parent::full_header();
    }

}
