<?php

namespace theme_edu;
class modal
{
    private $title;
    private $content;
    private $id;
    private $okButton = 'Ок';
    private $buttons = [];
    private $ajax = '';
    private $width = '600px';

    function __construct($title, $content, $id = '')
    {
        $this->title = $title;
        $this->content = $content;
        $this->id = $id ? $id : 'modal-' . rand(0, 999999);
    }

    function addButton($name, $title, $close = false, $ajax = false, $class = 'primary')
    {
        if ($this->ajax && $ajax) {
            $ajax = 'data-ajax="1" ';
        }
        $this->buttons[] = '<button '.$ajax.'name="' . $name . '" type="button" class="btn btn-' . $class . '" ' . ($close ? 'data-dismiss="modal"' : '') . '>' . $title . '</button>';
    }

    function setOk($name)
    {
        $this->okButton = $name;
    }

    function setAjax($ajax)
    {
        $this->ajax = $ajax;
    }

    function linkTag($tag, $content, $opt = [])
    {
        //$opt['type'] = 'button';
        $opt['data-toggle'] = 'modal';
        $opt['data-target'] = '#' . $this->id;
        if ($this->ajax) {
            $opt['data-ajax'] = $this->ajax;
        }
        return \html_writer::tag($tag, $content, $opt);

    }

    function link($name)
    {
        return $this->linkTag('a', $name, ['href' => '#']);
    }

    function setWidth($width)
    {
        $this->width = $width;
    }

    function render()
    {
        $html = '<div class="modal fade" id="' . $this->id . '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="max-width: '.$this->width.'">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="' . $this->id . 'Label">' . $this->title . '</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ' . $this->content . '
      </div>
      <div class="modal-footer">';
        if (empty($this->buttons)) {
            $html .= '<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button> ';
            if ($this->okButton)
                $html .= '<button type="button" class="btn btn-primary" data-dismiss="modal">' . $this->okButton . '</button>';
        } else {
            $html .= implode(' ', $this->buttons);
        }
        $html .= '</div>
    </div>
  </div>
</div>';
        return $html;
    }
}

?>